package com.example.deeptrancer.denemep;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.content.Intent;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {



    private EditText editText;
    private TextView boyt, kilogoster_sk, idealk_tv, durum_tv;
    private RadioButton radioBayan, radioBay;

    private boolean kadinmi = true;
    private double boy = 0.0;
    private int kilo = 50 ;
    private SeekBar kilo_sk ;
    private SeekBar.OnSeekBarChangeListener editSeekOlaylar = new SeekBar.OnSeekBarChangeListener() {
        // seekbarı hareket ettirince değerin göretrilmesini sağlamak istediğim için burda listener oluşturup güncellemeye gittim.
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            kilo = 30+progress ;
            guncelle();
        }
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    } ; // unutma!

    private TextWatcher editTextOlaylar = new TextWatcher() {  // aşağıda kullanmak üzere TextWatcher türünde bir nesne oluşturdum.
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            try{

                boy= Double.parseDouble(s.toString())/100.0 ;  //değerler char olarak geliyor onları double a çevirmek istedim. önce stringe sonra double a.
                // mesela değer 175 geliyorsa 1.75 olarak ele alması içinde 100.0 a böldüm.


            }catch(NumberFormatException e)  {  // parse işleminde hatalar olmasın diye bunu ekledim.
                boy = 0.0;    // trt kısmında bir hata oluşursa başlangıç değerini tekrar burada koydum.

                guncelle();
            }

        }
        @Override
        public void afterTextChanged(Editable s) {

        }
    } ;  // buradaki ";" ı unutma !

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = (EditText) findViewById(R.id.editText);
        kilogoster_sk = (TextView) findViewById(R.id.kilogoster_sk);
        idealk_tv = (TextView) findViewById(R.id.idealk_tv);
        boyt = (TextView) findViewById(R.id.boyt);
        durum_tv = (TextView) findViewById(R.id.durum_tv);
        radioBayan = (RadioButton) findViewById(R.id.radioBayan);
        radioBay = (RadioButton) findViewById(R.id.radioBay);
        kilo_sk = (SeekBar) findViewById(R.id.kilo_sk);

        editText.addTextChangedListener(editTextOlaylar); // bir yerdeki değerler değişince diğer yerlerdeki değerlerinde deişmesi için listener ekliyoruz. Onclick listener yönteminin alternatifi.
        // bu yüzden editTextOlaylar adında bir değişken buraya ekledik. Bu değişken üzerinden yapılacak değişikleri güncelle.
        kilo_sk.setOnSeekBarChangeListener(editSeekOlaylar);



        guncelle();
    }

    private void guncelle() {



        kilogoster_sk.setText(String.valueOf(kilo));  // default olarak 50 den başlattık. seek bar hareket ettikçe bu değişsin
        boyt.setText(String.valueOf(boy));


    }



}
