package com.example.deeptrancer.register;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity {
    TextView kayitsayfasinagit ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        TextView girisyap = (TextView) findViewById(R.id.text1);
        EditText kullanici_adi = (EditText) findViewById(R.id.username);
        EditText sifre  = (EditText) findViewById(R.id.password);
        Button kayitbutonu = (Button) findViewById(R.id.loginbutton);
        kayitsayfasinagit = (TextView) findViewById(R.id.register);

      //  kayitsayfasinagit.setOnClickListener(this);
    }

    /*@Override
    public void onClick(View v) {

        if(v.getId()== kayitsayfasinagit.getId()) {

            Intent kaydagit = new Intent(getApplicationContext(), RegisterActivity.class);   // kaydagit değişkeni , bu klasstan, registerActivity clasına gidiyor.
            startActivity(kaydagit);   // kaydagit değişkeniyle bu metodu çağırarak , registera tıklanması halinde kayit sayfasına gitmesi sağlandı
        }
        else
            System.out.println("deneme");
    }
*/

    public void git(View view) {

        Intent kaydagit = new Intent(getApplicationContext(), RegisterActivity.class);   // kaydagit değişkeni , bu klasstan, registerActivity clasına gidiyor.
        startActivity(kaydagit);   // kaydagit değişkeniyle bu metodu çağırarak , registera tıklanması halinde kayit sayfasına gitmesi sağlandı
    }
}
