package com.example.deeptrancer.diyetuygulamam496;

/**
 * Created by deeptrancer on 2.03.2018.
 */

        import android.content.Context;
        import android.database.Cursor;
        import android.graphics.Color;
        import android.net.Uri;
        import android.os.Bundle;
        import android.support.v4.app.Fragment;
        import android.support.v4.app.FragmentManager;
        import android.text.Editable;
        import android.text.TextWatcher;
        import android.view.Gravity;
        import android.view.LayoutInflater;
        import android.view.MenuInflater;
        import android.view.MenuItem;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ImageView;
        import android.widget.TableLayout;
        import android.widget.TableRow;
        import android.widget.TextView;
        import android.widget.Toast;

        import java.util.Calendar;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Anasayfa.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Anasayfa#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Anasayfa extends Fragment {

    private View anahat;
    private Cursor listC;


    private MenuItem besinekleToolbar;


    private String yiltut = "";
    private String aytut = "";
    private String gunTut = "";

    private String hangibesin;
    private String besinIsmi;
    private String besinID;

    private boolean porsiyonboyutu;
    private boolean porsiyonBoyutuGram;


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    public Anasayfa() {

    }


   


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((Menu)getActivity()).getSupportActionBar().setTitle("Anasayfa");

        anasayfaOlusturma();

        setHasOptionsMenu(true);
    }

	
	
	
	 public static Anasayfa newInstance(String param1, String param2) {
        Anasayfa fragment = new Anasayfa();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    } // otomatk oluşturdu
	
	

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        anahat = inflater.inflate(R.layout.anasayfa, container, false);
        return anahat;
    }


    

    public void onCreateOptionsMenu(android.view.Menu menu, MenuInflater inflater) {

        MenuInflater menuInflater = ((Menu)getActivity()).getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);

        besinekleToolbar = menu.findItem(R.id.besinEkleme);

        besinekleToolbar.setVisible(true);
    }
	
	
	private void setAnahat(int id){
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        anahat = inflater.inflate(id, null);
        ViewGroup rootView = (ViewGroup) getView();
        rootView.removeAllViews();
        rootView.addView(anahat);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        int i_id = menuItem.getItemId();
        if (i_id == R.id.besinEkleme) {
            besiniEkle();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void anasayfaOlusturma(){                                                 //******************

        if(yiltut.equals("") || aytut.equals("") || gunTut.equals("")) {
            Calendar zaman = Calendar.getInstance();
            int yil = zaman.get(Calendar.YEAR);
            int ay = zaman.get(Calendar.MONTH);
            int gun = zaman.get(Calendar.DAY_OF_MONTH);


            yiltut = "" + yil;

            ay = ay+1; // Month starts with 0
            if(ay < 10){
                aytut = "0" + ay;
            }
            else{
                aytut = "" + ay;
            }

            if(gun < 10){
                gunTut = "0" + gun;
            }
            else{
                gunTut = "" + gun;
            }
        }
        String stringFdDate = yiltut + "-" + aytut + "-" + gunTut;


        guncelle(stringFdDate, "0");
        guncelle(stringFdDate, "1");
        guncelle(stringFdDate, "2");
        guncelle(stringFdDate, "3");
        guncelle(stringFdDate, "4");
        guncelle(stringFdDate, "5");
        guncelle(stringFdDate, "6");

        //gunluk kalori hesaplama
        alinanKaloriyihesapla(stringFdDate);                                                           //*********************


        ImageView kahfaltiyaEkle = (ImageView)getActivity().findViewById(R.id.kahfalti);
        kahfaltiyaEkle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                besinEkleme(0);
            }
        });
        ImageView OgleYemegineEkle = (ImageView)getActivity().findViewById(R.id.ogleyemegi);
        OgleYemegineEkle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                besinEkleme(1);
            }
        });
        ImageView egzersizoncesiEkle = (ImageView)getActivity().findViewById(R.id.egzersizoncesi);
        egzersizoncesiEkle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                besinEkleme(2);
            }
        });
        ImageView imageViewAddAfterTraining = (ImageView)getActivity().findViewById(R.id.egzersizSonrasi);
        imageViewAddAfterTraining.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                besinEkleme(3);
            }
        });
        ImageView aksamYemegiEkle = (ImageView)getActivity().findViewById(R.id.aksamYemegi);
        aksamYemegiEkle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                besinEkleme(4);
            }
        });
        ImageView AtistirmalikEkle = (ImageView)getActivity().findViewById(R.id.atistirmalik);
        AtistirmalikEkle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                besinEkleme(5);
            }
        });

        ImageView ekolarakEkle = (ImageView)getActivity().findViewById(R.id.ekoarak);
        ekolarakEkle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                besinEkleme(6);
            }
        });

    } // anasayfaOlusturma



    private void guncelle(String stringDate, String OgunnO){

        Verit veriler = new Verit(getActivity());
        veriler.open();

        String menuSQL = veriler.aktarma(OgunnO);
        String s_tarihSQL = veriler.aktarma(stringDate);

        String fdFields[] = new String[] {
                "_id",
                "gbesin_besinID",
                "gbesin_servisGram",
                "gbesin_servisGramOlc",
                "gbesin_servisAadet",
                "gbesin_servisAadetOlcmbr",
                "gbesin_enerji",
                "gbesin_protein",
                "gbesin_karbonhidrat",
                "gbesin_yag"
        };
        String fdWhereClause[] = new String[]{
                "gbesin_tarih",
                "gbesin_ogun"
        };
        String fdWhereCondition[] = new String[]{
                s_tarihSQL,
                menuSQL
        };
        String fdWhereAndOr[] = new String[]{
                "AND"
        };
        Cursor scurs = veriler.sec("gunlukBesin", fdFields, fdWhereClause, fdWhereCondition, fdWhereAndOr);

        String besinkoy[] = new String[] {
                "_id",
                "gida_isim",
                "gida_Marka"
        };
        Cursor besinCursor;

        Cursor fcursor;
        String fieldsFdce[] = new String[] {
                "_id",
                "besin_alinan_cal_ID",
                "besin_alinan_cal_tarih",
                "besin_alinan_cal_ogunNo",
                "besin_alinan_enerji",
                "besin_alinan_protein",
                "besin_alinan_karbon",
                "besin_alinan_yag"
        };
        String nerede[] = new String[]{
                "besin_alinan_cal_tarih",
                "besin_alinan_cal_ogunNo"
        };
        String k_Nerede[] = new String[]{
                s_tarihSQL,
                menuSQL
        };
        String neredrc[] = new String[]{
                "AND"
        };

        fcursor = veriler.sec("besinlerdenAlinanKalori", fieldsFdce, nerede, k_Nerede, neredrc);
        int sayacCursor = fcursor.getCount();

        int hata = 0;
        if(sayacCursor == 0){

            String insFields = "_id, besin_alinan_cal_tarih, besin_alinan_cal_ogunNo, besin_alinan_enerji, besin_alinan_protein, besin_alinan_karbon, besin_alinan_yag";
            String insValues = "NULL, " + s_tarihSQL + ", " + menuSQL + ", '0', '0', '0', '0'";
            veriler.insert("besinlerdenAlinanKalori", insFields, insValues);

            fcursor = veriler.sec("besinlerdenAlinanKalori", fieldsFdce, nerede, k_Nerede, neredrc);
        }
        String stringID = fcursor.getString(0);
        long longStringID = Long.parseLong(stringID);

        int alinanEnerji = 0;
        int alinanprotein = 0;
        int alinankarbonhidrat = 0;
        int alinanYag = 0;


        int sayacCursorr = scurs.getCount();
        for(int x=0;x<sayacCursorr;x++){
            String stringIDno = scurs.getString(0);

            String besinID = scurs.getString(1);
            String besinIDSQL = veriler.aktarma(besinID);

            String besinServisGram = scurs.getString(2);
            String besinServisolcumgram = scurs.getString(3);
            String besinolcumadet = scurs.getString(4);
            String besinOlcumbirimi = scurs.getString(5);
            String besinhesaplananEnerji = scurs.getString(6);
            String besinHesaplananPRorein = scurs.getString(7);
            String besinhesaplananKArbonhidrat = scurs.getString(8);
            String besinHesaplannaYag = scurs.getString(9);
            int hesaplananEnerji = Integer.parseInt(besinhesaplananEnerji);
            int hesaplananProrein = Integer.parseInt(besinHesaplananPRorein);
            int hesaplannaKArbonhidrat = Integer.parseInt(besinhesaplananKArbonhidrat);
            int hesaplananYag = Integer.parseInt(besinHesaplannaYag);


            besinCursor = veriler.sec("gidalar", besinkoy, "_id", besinIDSQL);

            //tabloya eklersek
            String besinlerID = besinCursor.getString(0);
            String besin_Ismi = besinCursor.getString(1);
            String besinUreticiISmi  = besinCursor.getString(2);

            String subLine = besinUreticiISmi + ", " +
                    besinServisGram + " " +
                    besinServisolcumgram + ", " +
                    besinolcumadet + " " +
                    besinOlcumbirimi;


            // tabloya eklersek
            TableLayout tab1 = null;
            if(OgunnO.equals("0")) {
                tab1 = (TableLayout) getActivity().findViewById(R.id.tlKahfaltiT);
            }
            else if(OgunnO.equals("1")) {
                tab1 = (TableLayout) getActivity().findViewById(R.id.tl_Ogleitem);
            }
            else if(OgunnO.equals("2")) {
                tab1 = (TableLayout) getActivity().findViewById(R.id.tl_egzersizoncesiBes);
            }
            else if(OgunnO.equals("3")) {
                tab1 = (TableLayout) getActivity().findViewById(R.id.tl_egzersizSonrasi);
            }
            else if(OgunnO.equals("4")) {
                tab1 = (TableLayout) getActivity().findViewById(R.id.tl_tabloAksam);
            }
            else if(OgunnO.equals("5")) {
                tab1 = (TableLayout) getActivity().findViewById(R.id.tv_atistirmaliklar);
            }
            else {
                tab1 = (TableLayout) getActivity().findViewById(R.id.tl_ekolarakBesin);
            }
            TableRow tablerow1 = new TableRow(getActivity());
            tablerow1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));

            TableRow tablerow2 = new TableRow(getActivity());
            tablerow2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));


            TextView tv_isim = new TextView(getActivity());
            tv_isim.setText(besin_Ismi);
            tv_isim.setTextSize(18);
            tv_isim.setTextColor(Color.DKGRAY);
            tv_isim.setGravity(Gravity.CENTER_VERTICAL);
            TableRow.LayoutParams paramsName = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);
            paramsName.setMargins(4, 8, 0, 0);
            tablerow1.addView(tv_isim, paramsName);

            TextView tv_Enerji = new TextView(getActivity());
            tv_Enerji.setText(besinhesaplananEnerji);
            tv_Enerji.setTextSize(18);
            tv_Enerji.setTextColor(Color.DKGRAY);
            tv_Enerji.setGravity(Gravity.CENTER_VERTICAL);
            TableRow.LayoutParams enerji = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
            enerji.setMargins(0, 8, 10, 0);
            tablerow1.addView(tv_Enerji, enerji);


            TableRow.LayoutParams paramsSubLine = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);
            paramsSubLine.setMargins(4, 0, 0, 12);

            TextView tvs = new TextView(getActivity());
            tvs.setText(subLine);
            tablerow2.addView(tvs, paramsSubLine);

            // ekle
            tab1.addView(tablerow1, new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
            tab1.addView(tablerow2, new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));

            // listener
            tablerow1.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    v.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_light));

                    Context icerik = getContext();
                    TableRow row = (TableRow)v;
                    TextView textView = (TextView)row.getChildAt(0);

                    String tvText ="" + textView.getText();
                    eklesil(tvText);
                }
            });

            // listener
            tablerow2.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    v.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_light));

                    Context icerik = getContext();
                    TableRow dzi = (TableRow)v;
                    TextView textView = (TextView)dzi.getChildAt(0);

                    String tvText ="" + textView.getText();
                    eklesil(tvText);
                }
            });

            alinanEnerji = alinanEnerji+hesaplananEnerji;
            alinanprotein = alinanprotein+hesaplananProrein;
            alinankarbonhidrat = alinankarbonhidrat+hesaplannaKArbonhidrat;
            alinanYag = alinanYag+hesaplananYag;


            scurs.moveToNext();
        }


        if(OgunnO.equals("0")) {
            TextView tv_enerji = (TextView)getActivity().findViewById(R.id.tvKahfaltiEnerji);
            tv_enerji.setText(""+ alinanEnerji);
        }
        else if(OgunnO.equals("1")) {
            TextView tv_enerji = (TextView)getActivity().findViewById(R.id.tv_enerjiOgleyemegi);
            tv_enerji.setText(""+ alinanEnerji);
        }
        else if(OgunnO.equals("2")) {
            TextView tv_enerji = (TextView)getActivity().findViewById(R.id.tv_egzersizOncesiC);
            tv_enerji.setText(""+ alinanEnerji);
        }
        else if(OgunnO.equals("3")) {
            TextView tv_enerji = (TextView)getActivity().findViewById(R.id.tv_egzersizSonrasi);
            tv_enerji.setText(""+ alinanEnerji);
        }
        else if(OgunnO.equals("4")) {
            TextView tv_enerji = (TextView)getActivity().findViewById(R.id.tv_enerjiAksam);
            tv_enerji.setText(""+ alinanEnerji);
        }
        else if(OgunnO.equals("5")) {
            TextView tv_enerji = (TextView)getActivity().findViewById(R.id.tv_atistirmalikEnerji);
            tv_enerji.setText(""+ alinanEnerji);
        }
        else {
            TextView tv_enerji = (TextView)getActivity().findViewById(R.id.tv_ekOlarakEnerji);
            tv_enerji.setText(""+ alinanEnerji);
        }

        String updateFields[] = new String[] {
                "besin_alinan_enerji",
                "besin_alinan_protein",
                "besin_alinan_karbon",
                "besin_alinan_yag"
        };
        String updateValues[] = new String[] {
                "'" + alinanEnerji + "'",
                "'" + alinanprotein + "'",
                "'" + alinankarbonhidrat + "'",
                "'" + alinanYag + "'"
        };

        veriler.guncelle("besinlerdenAlinanKalori", "_id", longStringID, updateFields, updateValues);


        veriler.close();
    } // guncelle


    private void besiniEkle() {
        int idtut = R.layout.anasayfaogun;
        setAnahat(idtut);

        TextView kahfalti = (TextView)getActivity().findViewById(R.id.tv_kahfalti);
        kahfalti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                besinEkleme(0);
            }
        });

        TextView ogleYemegi = (TextView)getActivity().findViewById(R.id.tv_oglen);
        ogleYemegi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                besinEkleme(1);
            }
        });

        TextView egzersizoncesi = (TextView)getActivity().findViewById(R.id.tv_aktiviteoncesi);
        egzersizoncesi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                besinEkleme(2);
            }
        });

        TextView egzersizSonrasi = (TextView)getActivity().findViewById(R.id.tv_aktSonrasi);
        egzersizSonrasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                besinEkleme(3);
            }
        });

        TextView aksamYemegi = (TextView)getActivity().findViewById(R.id.tv_aksm);
        aksamYemegi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                besinEkleme(4);
            }
        });

        TextView atistirmalik = (TextView)getActivity().findViewById(R.id.tv_atistirma);
        atistirmalik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                besinEkleme(5);
            }
        });

        TextView ekolarak = (TextView)getActivity().findViewById(R.id.textViewExt);
        ekolarak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                besinEkleme(6);
            }
        });



    } // ogunebesinekle


    private void besinEkleme(int mealNumber){

        Fragment fragment = null;
        Class fragmentClass = null;
        fragmentClass = OguneEkle.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Bundle bundle = new Bundle();
        bundle.putString("mealNumber", ""+mealNumber);                                      //************bukez burayı yaptıgımız gibi biraktik.
        bundle.putString("hangibesin", "");
        bundle.putString("action", "");
        fragment.setArguments(bundle);

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.doldur, fragment).commit();


    } // anasayfaOlusturma

    private void eklesil(String tabloIsim){

        Verit veriler = new Verit(getActivity());
        veriler.open();

        int idtut = R.layout.anasayfaduzenleme;
        setAnahat(idtut);

        String fields[] = new String[] {
                "_id",
                "gbesin_besinID",
                "gbesin_servisGram",
                "gbesin_servisGramOlc",
                "gbesin_servisAadet",
                "gbesin_servisAadetOlcmbr",
                "gbesin_enerji",
                "gbesin_protein",
                "gbesin_karbonhidrat",
                "gbesin_yag"
        };
        String stringTarihtut = yiltut + "-" + aytut + "-" + gunTut;
        String stringTarihTutSQL = veriler.aktarma(stringTarihtut);
        Cursor fdcursor = veriler.sec("gunlukBesin", fields, "gbesin_tarih", stringTarihTutSQL);
        String s_id = "0";

        String fieldsFood[] = new String[] {
                "_id",
                "gida_isim",
                "gida_Marka"
        };
        Cursor gidaCurs;

        String s_besinIDno = "";
        String stringBesinSQL = "";

        String s_servisboyutuGram = "";
        String s_servisBoyutuGramolcuB = "";
        String s_ServisBoyutuAdet = "";
        String s_servisBoyutuAdetolcuBr = "";
        String s_hesaplananEnerji = "";

        String s_besinID = "";
        String s_besinIsmi = "";
        String s_marka  = "";

        int cursorSayac = fdcursor.getCount();
        for(int x=0;x<cursorSayac;x++) {

            s_id = fdcursor.getString(0);
            s_besinIDno = fdcursor.getString(1);
            stringBesinSQL = veriler.aktarma(s_besinIDno);
            s_servisboyutuGram = fdcursor.getString(2);
            s_servisBoyutuGramolcuB = fdcursor.getString(3);
            s_ServisBoyutuAdet = fdcursor.getString(4);
            s_servisBoyutuAdetolcuBr = fdcursor.getString(5);
            s_hesaplananEnerji = fdcursor.getString(6);

            gidaCurs = veriler.sec("gidalar", fieldsFood, "_id", stringBesinSQL);


            s_besinID = gidaCurs.getString(0);
            s_besinIsmi = gidaCurs.getString(1);
            s_marka  = gidaCurs.getString(2);

            String subLine = s_marka + ", " +
                    s_servisboyutuGram + " " +
                    s_servisBoyutuGramolcuB + ", " +
                    s_ServisBoyutuAdet + " " +
                    s_servisBoyutuAdetolcuBr;

            if(tabloIsim.equals(s_besinIsmi)){

                break;
            }
            else if(tabloIsim.equals(subLine)){
                break;
            }


            fdcursor.moveToNext();
        }

        if(s_besinIsmi.equals("")) {
            Toast.makeText(getActivity(), "Hata: besin kaydedilemedi", Toast.LENGTH_LONG).show();
        }
        else{

            besinIsmi = s_besinIsmi;
            hangibesin = s_besinID;
            besinID = s_id;

            TextView tv_besinIsmi = (TextView)getActivity().findViewById(R.id.tv_besinIsminiGoster);
            tv_besinIsmi.setText(s_besinIsmi);

            TextView tv_marka = (TextView)getActivity().findViewById(R.id.tv_besinUreticiMarkasi);
            tv_marka.setText(s_marka);


            EditText et_servisBoyutu = (EditText)getActivity().findViewById(R.id.et_servisAdet);
            et_servisBoyutu.setText(s_ServisBoyutuAdet);

            TextView tvServisBoyutuBrm = (TextView)getActivity().findViewById(R.id.tv_servisSizeOlcum);
            tvServisBoyutuBrm.setText(s_servisBoyutuAdetolcuBr);

            EditText et_servisGramaj = (EditText)getActivity().findViewById(R.id.et_servisGram);
            et_servisGramaj.setText(s_servisboyutuGram);

            TextView tv_servisgramolcumBr = (TextView)getActivity().findViewById(R.id.tv_servisGramolcumb);
            tv_servisgramolcumBr.setText(s_servisBoyutuGramolcuB);

            et_servisBoyutu.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                    if(!(s.toString().equals(""))){
                        porsiyonadetdegisikligi();
                    }
                }
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                public void onTextChanged(CharSequence s, int start, int before, int count) {}
            });
            et_servisBoyutu.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                    }else {
                        String lock = "portionSizePcs";
                        releaseLock(lock);
                    }
                }
            });

            et_servisGramaj.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                    if(!(s.toString().equals(""))){
                        porsiyonGramDegisikligi();
                    }
                }
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                public void onTextChanged(CharSequence s, int start, int before, int count) {}
            });
            et_servisGramaj.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                    }else {
                        String lock = "portionSizeGram";
                        releaseLock(lock);
                    }
                }
            });

            Button duzenleButonu = (Button)getActivity().findViewById(R.id.duzenleButonu);
            duzenleButonu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    besinDuzenle();
                }
            });
            Button silButonu = (Button)getActivity().findViewById(R.id.silmebutonu);
            silButonu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OnClickDeleteFdLineSubmit();
                }
            });
        }
        veriler.close();
    }

    private void releaseLock(String lock){
        if(lock.equals("portionSizeGram")){
            porsiyonBoyutuGram = false;
        }
        else {
            porsiyonboyutu = false;
        }
    }

    /*- porsiyonAdediDuzenleme ---------------------------------------------------- */
    public void porsiyonadetdegisikligi(){
        if(!(porsiyonBoyutuGram)) {

            porsiyonboyutu = true;

            EditText porsiyonAdetDzuenle = (EditText) getActivity().findViewById(R.id.et_servisAdet);
            String s_porsiyonAded = porsiyonAdetDzuenle.getText().toString();

            double d_porsiyonAdet = 0;

            if (s_porsiyonAded.equals("")) {
                d_porsiyonAdet = 0;
            } else {
                try {
                    d_porsiyonAdet = Double.parseDouble(s_porsiyonAded);
                } catch (NumberFormatException nfe) {
                    System.out.println("Hata " + nfe);
                }
            }

            Verit veriler = new Verit(getActivity());
            veriler.open();

            String fields[] = new String[]{
                    "gida_servisGram"
            };
            String SQL = veriler.aktarma(hangibesin);
            Cursor besinCrs = veriler.sec("gidalar", fields, "_id", SQL);

            String s_ServisBoyutu_ = besinCrs.getString(0);
            veriler.close();

            double doubleServingSize = 0;
            try {
                doubleServingSize = Double.parseDouble(s_ServisBoyutu_);
            } catch (NumberFormatException nfe) {
                System.out.println("Hata " + nfe);
            }

            double d_porsiyonBoyutu = Math.round(d_porsiyonAdet * doubleServingSize);

            EditText et_porsiyonGramaj = (EditText) getActivity().findViewById(R.id.et_servisGram);
            et_porsiyonGramaj.setText("" + d_porsiyonBoyutu);
        }
    }

    public void porsiyonGramDegisikligi(){
        if(!(porsiyonboyutu)) {

            porsiyonBoyutuGram = true;

            EditText et_porsiyonGram = (EditText) getActivity().findViewById(R.id.et_servisGram);
            String s_porsiyonGram = et_porsiyonGram.getText().toString();
            double d_porsiyonGram = 0;
            try {
                d_porsiyonGram = Double.parseDouble(s_porsiyonGram);
            } catch (NumberFormatException nfe) {
                System.out.println("Could not parse " + nfe);
            }

            Verit veriler = new Verit(getActivity());
            veriler.open();

            String fields[] = new String[]{
                    "gida_servisGram"
            };
            String idSQL = veriler.aktarma(hangibesin);
            Cursor besincursor = veriler.sec("gidalar", fields, "_id", idSQL);

            String s_ServisGrams = besincursor.getString(0);
            veriler.close();

            double d_servisGramm = 0;
            try {
                d_servisGramm = Double.parseDouble(s_ServisGrams);
            } catch (NumberFormatException nfe) {
                System.out.println("Could not parse " + nfe);
            }

            double d_porsiyon = Math.round(d_porsiyonGram / d_servisGramm);

            EditText et_porsiyonsGram = (EditText) getActivity().findViewById(R.id.et_servisAdet);
            et_porsiyonsGram.setText("" + d_porsiyon);
        }

    }

    public void besinDuzenle(){


        int hata = 0;

        Verit veriler = new Verit(getActivity());
        veriler.open();

        long long_id = 0;
        try{
            long_id = Long.parseLong(besinID);
        }
        catch(NumberFormatException nfe) {
            System.out.println("Hata " + nfe);
        }

        String fields[] = new String[] {
                "gida_servisGram",
                "gida_enerji",
                "gida_protein",
                "gida_karbonhidrat",
                "gida_yag"
        };
        String currentIdSQL = veriler.aktarma(hangibesin);
        Cursor foodCursor = veriler.sec("gidalar", fields, "_id", currentIdSQL);

        String s_gramajAl = foodCursor.getString(0);
        double d_servisGramaj = Double.parseDouble(s_gramajAl);

        String s_enerji = foodCursor.getString(1);
        double d_enerji = Double.parseDouble(s_enerji);

        String s_protein = foodCursor.getString(2);
        double d_protein = Double.parseDouble(s_protein);

        String s_karbonhidrat = foodCursor.getString(3);
        double d_karbonhidrat = Double.parseDouble(s_karbonhidrat);

        String s_yag = foodCursor.getString(4);
        double d_yag = Double.parseDouble(s_yag);

        EditText et_servisGramaj = (EditText)getActivity().findViewById(R.id.et_servisGram);
        String s_servisGramaj = et_servisGramaj.getText().toString();
        String s_servisGramajB = veriler.aktarma(s_servisGramaj);
        veriler.guncelle("gunlukBesin", "_id", long_id, "gbesin_servisGram", s_servisGramajB);
        double servisGram = Double.parseDouble(s_servisGramaj);

        double servisAdet = Math.round(servisGram / d_servisGramaj);
        String s_ServisAdetSQL = "" + servisAdet;
        String s_servisAdet = veriler.aktarma(s_ServisAdetSQL);
        veriler.guncelle("gunlukBesin", "_id", long_id, "gbesin_servisAadet", s_servisAdet);

        double d_hes_enerji = Math.round((servisGram*d_enerji)/100);
        String s_hes_enerji = "" + d_hes_enerji;
        String s_hes_enerjiSQL = veriler.aktarma(s_hes_enerji);
        veriler.guncelle("gunlukBesin", "_id", long_id, "gbesin_enerji", s_hes_enerjiSQL);

        double d_hes_protein = Math.round((servisGram*d_protein)/100);
        String s_hes_protein = "" + d_hes_protein;
        String s_proteinSQL = veriler.aktarma(s_hes_protein);
        veriler.guncelle("gunlukBesin", "_id", long_id, "gbesin_protein", s_proteinSQL);

        double d_hes_karbonhidrat = Math.round((servisGram*d_karbonhidrat)/100);
        String s_hes_karbonhidrat = "" + d_hes_karbonhidrat;
        String st_karbonhdratSQL = veriler.aktarma(s_hes_karbonhidrat);
        veriler.guncelle("gunlukBesin", "_id", long_id, "gbesin_karbonhidrat", st_karbonhdratSQL);


        // yag hesabi
        double d_hesapYag = Math.round((servisGram*d_yag)/100);
        String s_hesapYag = "" + d_hesapYag;
        String sHesapYagSQL = veriler.aktarma(s_hesapYag);
        veriler.guncelle("gunlukBesin", "_id", long_id, "gbesin_yag", sHesapYagSQL);


        veriler.close();


        Toast.makeText(getActivity(), "Kaydedildi", Toast.LENGTH_SHORT).show();


        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.doldur, new Anasayfa(), Anasayfa.class.getName()).commit();


    }


    public void OnClickDeleteFdLineSubmit(){
        Toast.makeText(getActivity(), "Silindi " + besinIsmi, Toast.LENGTH_SHORT).show();

        Verit veriler = new Verit(getActivity());
        veriler.open();

        long l_key = 0;
        try{
            l_key = Long.parseLong(besinID);
        }
        catch(NumberFormatException nfe) {
            System.out.println("Hata " + nfe);
        }

        veriler.sil("gunlukBesin", "_id", l_key);

        veriler.close();


        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.doldur, new Anasayfa(), Anasayfa.class.getName()).commit();


    }

    public void alinanKaloriyihesapla(String stringDate){


        Verit veriler = new Verit(getActivity());
        veriler.open();

        String tarihSQL = veriler.aktarma(stringDate);

        String gunluk_besin[] = new String[] {
                "_id",
                "besin_gunlukToplam_tarih",
                "besin_gunlukToplam_enerji",
                "besin_gunlukToplam_protein",
                "besin_gunlukToplam_karbonhidrat",
                "besin_gunlukToplam_yag"
        };
        Cursor cursorFoodDiarySum = veriler.sec("besin_gunlukToplam", gunluk_besin, "besin_gunlukToplam_tarih", tarihSQL);
        int ogunBesinSayac = cursorFoodDiarySum.getCount();


        String fields[] = new String[]{
                "_id",
                "besin_alinan_cal_ID",
                "besin_alinan_cal_tarih",
                "besin_alinan_cal_ogunNo",
                "besin_alinan_enerji",
                "besin_alinan_protein",
                "besin_alinan_karbon",
                "besin_alinan_yag"
        };
        Cursor fcursor = veriler.sec("besinlerdenAlinanKalori", fields, "besin_alinan_cal_tarih", tarihSQL);
        int sayac = fcursor.getCount();


        int alinanEnerji = 0;
        int alinanProtein = 0;
        int alinanKarbonhidrat = 0;
        int alinanYag = 0;

        String ogunNo = "";
        String s_alinanEnerji = "";
        String s_alinanProtein = "";
        String s_alinanKArbonhidrat = "";
        String s_alinanYag = "";
        int i_alinanEnerji = 0;
        int i_alinanProtein = 0;
        int i_alinnaKarbonhidrat = 0;
        int i_alinannYag = 0;

        for (int i = 0; i < sayac; i++) {

            ogunNo = fcursor.getString(3);
            s_alinanEnerji = fcursor.getString(4);
            s_alinanProtein = fcursor.getString(5);
            s_alinanKArbonhidrat = fcursor.getString(6);
            s_alinanYag = fcursor.getString(7);


            try {
                i_alinanEnerji = Integer.parseInt(s_alinanEnerji);
            } catch (NumberFormatException nfe) {
                System.out.println("Hata " + nfe);
            }

            try {
                i_alinanProtein = Integer.parseInt(s_alinanProtein);
            } catch (NumberFormatException nfe) {
                System.out.println("Hata " + nfe);
            }

            try {
                i_alinnaKarbonhidrat = Integer.parseInt(s_alinanKArbonhidrat);
            } catch (NumberFormatException nfe) {
                System.out.println("Hata " + nfe);
            }

            try {
                i_alinannYag = Integer.parseInt(s_alinanYag);
            } catch (NumberFormatException nfe) {
                System.out.println("Hata " + nfe);
            }


            alinanEnerji = alinanEnerji + i_alinanEnerji;
            alinanProtein = alinanProtein + i_alinanProtein;
            alinanKarbonhidrat = alinanKarbonhidrat + i_alinnaKarbonhidrat;
            alinanYag = alinanYag + i_alinannYag;

            fcursor.moveToNext();
        }


        if (ogunBesinSayac == 0) {

            String ins = "_id, besin_gunlukToplam_tarih, besin_gunlukToplam_enerji, besin_gunlukToplam_protein, besin_gunlukToplam_karbonhidrat, besin_gunlukToplam_yag";
            String insVal = "NULL, " + tarihSQL + ", '" + alinanEnerji + "', '" +
                    alinanProtein + "', '" + alinanKarbonhidrat + "', '" + alinanYag + "'";
            veriler.insert("besin_gunlukToplam", ins, insVal);
        } else {

            String updateFields[] = new String[]{
                    "besin_gunlukToplam_enerji", "besin_gunlukToplam_protein", "besin_gunlukToplam_karbonhidrat", "besin_gunlukToplam_yag"
            };
            String updateValues[] = new String[]{
                    "'" + alinanEnerji + "'",
                    "'" + alinanProtein + "'",
                    "'" + alinanKarbonhidrat + "'",
                    "'" + alinanYag + "'"
            };

            long long_gunlukBesin = Long.parseLong(cursorFoodDiarySum.getString(0));

            veriler.guncelle("besin_gunlukToplam", "_id", long_gunlukBesin, updateFields, updateValues);
        }

        String fieldsGoal[] = new String[]{
                "_id",
                "diyetAktivite_enerji"
        };
        Cursor hedefCursor = veriler.sec("hedef", fieldsGoal);
        hedefCursor.moveToLast();
        String s_egzersizvediyet = hedefCursor.getString(1);

        TextView tv_egzersizle = (TextView) getActivity().findViewById(R.id.tv_egzersizleHedef);
        tv_egzersizle.setText(s_egzersizvediyet);


        TextView tv_besinicin = (TextView) getActivity().findViewById(R.id.tv_vctBesin);
        tv_besinicin.setText("" + alinanEnerji);

        int hedefEnerjiAktiviteylevediyetle = 0;
        try {
            hedefEnerjiAktiviteylevediyetle = Integer.parseInt(s_egzersizvediyet);
        } catch (NumberFormatException nfe) {
            System.out.println("Hata " + nfe);
        }

        int tv_sonuc = hedefEnerjiAktiviteylevediyetle - alinanEnerji;

        TextView tv_kalan = (TextView) getActivity().findViewById(R.id.tv_vctHatr);
        tv_kalan.setText("" + tv_sonuc);



        veriler.close();
    } // alinanKaloriyihesapla



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " ");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
