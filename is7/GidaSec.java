package com.example.deeptrancer.diyetuygulamam496;

/**
 * Created by deeptrancer on 2.03.2018.
 */

        import android.content.Context;
        import android.database.Cursor;
        import android.database.SQLException;
        import android.net.Uri;
        import android.os.Bundle;
        import android.support.v4.app.Fragment;
        import android.support.v4.app.FragmentManager;
        import android.view.LayoutInflater;
        import android.view.MenuInflater;
        import android.view.MenuItem;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.AdapterView;
        import android.widget.ArrayAdapter;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ImageView;
        import android.widget.ListView;
        import android.widget.Spinner;
        import android.widget.TextView;
        import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GidaSec.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GidaSec#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GidaSec extends Fragment {
    
    private View anahat;
    private Cursor listeCursor;

    private MenuItem menuDuzenle;
    private MenuItem menuBirimSil;

    private String ID_tut = "";
    private String isim_tut;

    
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mlListener;
    
    public GidaSec() {

    }
    
    public static GidaSec newInstance(String param1, String param2) {
        GidaSec fragment = new GidaSec();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((Menu)getActivity()).getSupportActionBar().setTitle("Gidalar");

        setHasOptionsMenu(true);

        Bundle bundle = this.getArguments();
        if(bundle != null){
            ID_tut = bundle.getString("BesinID");

        }
        if(ID_tut.equals("")) {
            besinListOlustur();
        }
        else{
            listeOdaklan();
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        anahat = inflater.inflate(R.layout.gidalar, container, false);
        return anahat;
    }

    private void setAnahat(int id){
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        anahat = inflater.inflate(id, null);
        ViewGroup rootView = (ViewGroup) getView();
        rootView.removeAllViews();
        rootView.addView(anahat);
    }

    public void onCreateOptionsMenu(android.view.Menu menu, MenuInflater inflater) {

        ((Menu)getActivity()).getMenuInflater().inflate(R.menu.menu_besin, menu);

        menuDuzenle = menu.findItem(R.id.besinDuzenle);
        menuBirimSil = menu.findItem(R.id.besinSil);

        menuDuzenle.setVisible(false);
        menuBirimSil.setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        int id = menuItem.getItemId();
        if (id == R.id.besinEkle) {
            besinEkleme();
        }
        if (id == R.id.besinDuzenle) {
            besinDuzenle();
        }
        if (id == R.id.besinSil) {
            besinSil();
        }
        return super.onOptionsItemSelected(menuItem);
    }


    public void besinListOlustur(){

        Verit db = new Verit(getActivity());
        db.open();

        String fields[] = new String[] {
                "_id",
                "gida_isim",
                "gida_Marka",
                "gida_aciklama",
                "gida_servisGram",
                "gida_servisGramolcm",
                "gida_servisAdet",
                "gida_servisAdetolcm",
                "gida_olculenEnerji"
        };
        try{
            listeCursor = db.sec("gidalar", fields, "", "", "gida_isim", "ASC");
        }
        catch (SQLException sqle) {
            Toast.makeText(getActivity(), sqle.toString(), Toast.LENGTH_LONG).show();
        }

        ListView lvItems = (ListView)getActivity().findViewById(R.id.besinliste);

        Gida continentsAdapter = new Gida(getActivity(), listeCursor);

        try{
            lvItems.setAdapter(continentsAdapter);
        }
        catch (Exception e){
            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
        }


        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                listele(arg2);
            }
        });

        db.close();

    };

    public void listeOdaklan(){

        Verit db = new Verit(getActivity());
        db.open();

        String fields[] = new String[] {
                "_id",
                "gida_isim",
                "gida_Marka",
                "gida_aciklama",
                "gida_servisGram",
                "gida_servisGramolcm",
                "gida_servisAdet",
                "gida_servisAdetolcm",
                "gida_olculenEnerji"
        };

        String IDSQL = db.aktarma(ID_tut);

        try{
            listeCursor = db.sec("gidalar", fields, "_id", IDSQL, "gida_isim", "ASC");
        }
        catch (SQLException sqle){
            Toast.makeText(getActivity(), sqle.toString(), Toast.LENGTH_LONG).show();
        }


        int indextut = 0;
        listele(indextut);

        db.close();
    }

    public void listele(int tiklandiginda) {

        int id = R.layout.gidaliste;
        setAnahat(id);

        try {
            menuDuzenle.setVisible(true);
            menuBirimSil.setVisible(true);
        }
        catch (Exception e){

        }

        listeCursor.moveToPosition(tiklandiginda);

        ID_tut = listeCursor.getString(0);
        isim_tut = listeCursor.getString(1);

        ((Menu)getActivity()).getSupportActionBar().setTitle(isim_tut);

        Verit veri = new Verit(getActivity());
        veri.open();

        String fields[] = new String[] {
                "_id",
                "gida_isim",
                "gida_Marka",
                "gida_aciklama",
                "gida_servisGram",
                "gida_servisGramolcm",
                "gida_servisAdet",
                "gida_servisAdetolcm",
                "gida_enerji",
                "gida_protein",
                "gida_karbonhidrat",
                "gida_yag",
                "gida_olculenEnerji",
                "gida_olculenProtein",
                "gida_olculen_karbonhidrat",
                "gida_olculen_yag",
                "gida_kullaniciID",
                "gida_bar",
                "gida_kategori",
                "gida_fa",
                "gida_fb",
                "gida_fc"
        };
        String idSQL = veri.aktarma(ID_tut);
        Cursor besinCursorr = veri.sec("gidalar", fields, "_id", idSQL);

        // Convert cursor to strings
        String s_id = besinCursorr.getString(0);
        String s_isim = besinCursorr.getString(1);
        String s_uretici = besinCursorr.getString(2);
        String s_aciklama = besinCursorr.getString(3);
        String s_servisBoyutu = besinCursorr.getString(4);
        String s_servisOlcum = besinCursorr.getString(5);
        String s_servisNameNo = besinCursorr.getString(6);
        String s_serviceIsimKelime = besinCursorr.getString(7);
        String s_Enerji = besinCursorr.getString(8);
        String s_Protein = besinCursorr.getString(9);
        String s_karbonhidrat = besinCursorr.getString(10);
        String s_yag = besinCursorr.getString(11);
        String s_esaplananEnerji = besinCursorr.getString(12);
        String s_hesaplananProtein = besinCursorr.getString(13);
        String s_hesaplanaKArbonhirat = besinCursorr.getString(14);
        String s_hesaplananYag = besinCursorr.getString(15);
        String s_kullaniciID = besinCursorr.getString(16);
        String s_barkod = besinCursorr.getString(17);
        String s_kategoriID = besinCursorr.getString(18);
        String foto1 = besinCursorr.getString(19);
        String foto2 = besinCursorr.getString(20);
        String foto3 = besinCursorr.getString(21);

        TextView tv_besinIsim = (TextView) getView().findViewById(R.id.tv_besinIsminiGoster);
        tv_besinIsim.setText(s_isim);

        TextView tv_besinUreticiIsmi = (TextView) getView().findViewById(R.id.tv_besinUreticiMarkasi);
        tv_besinUreticiIsmi.setText(s_uretici);

        TextView tv_besinHakkinda = (TextView) getView().findViewById(R.id.tv_besinHakkinda);
        String foodAbout = s_servisBoyutu + " " + s_servisOlcum + " = " +
                s_servisNameNo  + " " + s_serviceIsimKelime + ".";
        tv_besinHakkinda.setText(foodAbout);

        TextView tv_besinAciklama = (TextView) getView().findViewById(R.id.tv_besinAciklama);
        tv_besinAciklama.setText(s_aciklama);

        TextView tv_besinEnerjiYuzdesi = (TextView) getView().findViewById(R.id.tv_yuzdelikBesinEnerjisi);
        TextView tv_besinProteinYuzdesi = (TextView) getView().findViewById(R.id.tv_yuzdelikBesinProtein);
        TextView tv_besinKarbonhidatYuzdesi = (TextView) getView().findViewById(R.id.tv_yuzdelikBesinKarbonhidrat);
        TextView tv_besinYagYuzdesi = (TextView) getView().findViewById(R.id.tv_yuzdelikBesinYag);

        TextView tv_besinEnerjiAdt = (TextView) getView().findViewById(R.id.tv_besinEnerjiAdt);
        TextView tv_besinProteinAdt = (TextView) getView().findViewById(R.id.tv_besinProteinAdt);
        TextView tv_besinKarbonhidratAdt = (TextView) getView().findViewById(R.id.tv_besinKarbonhidratAdt);
        TextView tv_BesinYagAdt = (TextView) getView().findViewById(R.id.tv_besinYagAdt);

        tv_besinEnerjiYuzdesi.setText(s_Enerji);
        tv_besinProteinYuzdesi.setText(s_Protein);
        tv_besinKarbonhidatYuzdesi.setText(s_karbonhidrat);
        tv_besinYagYuzdesi.setText(s_yag);

        tv_besinEnerjiAdt.setText(s_esaplananEnerji);
        tv_besinProteinAdt.setText(s_hesaplananProtein);
        tv_besinKarbonhidratAdt.setText(s_hesaplanaKArbonhirat);
        tv_BesinYagAdt.setText(s_hesaplananYag);

        veri.close();

        ImageView oguneEkle = (ImageView)getActivity().findViewById(R.id.gunlugeEkle);
        oguneEkle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ogunebesinekle();
            }
        });
    }

    
    String kategoriIsmiSec = "";
    public void besinDuzenle(){

        int id = R.layout.gidaduzenle;
        setAnahat(id);

        ID_tut = listeCursor.getString(0);
        isim_tut = listeCursor.getString(1);

        ((Menu)getActivity()).getSupportActionBar().setTitle("Edit " + isim_tut);

        Verit veri = new Verit(getActivity());
        veri.open();


        String fields[] = new String[] {
                "_id",
                "gida_isim",
                "gida_Marka",
                "gida_aciklama",
                "gida_servisGram",
                "gida_servisGramolcm",
                "gida_servisAdet",
                "gida_servisAdetolcm",
                "gida_enerji",
                "gida_protein",
                "gida_karbonhidrat",
                "gida_yag",
                "gida_olculenEnerji",
                "gida_olculenProtein",
                "gida_olculen_karbonhidrat",
                "gida_olculen_yag",
                "gida_kullaniciID",
                "gida_bar",
                "gida_kategori",
                "gida_fa",
                "gida_fb",
                "gida_fc"
        };
        String Sql = veri.aktarma(ID_tut);
        Cursor besinCursor = veri.sec("gidalar", fields, "_id", Sql);

        // Convert cursor to strings
        String s_id = besinCursor.getString(0);
        String s_Isim = besinCursor.getString(1);
        String s_ureticiISmi = besinCursor.getString(2);
        String s_Aciklama = besinCursor.getString(3);

        String s_servisBoyutu = besinCursor.getString(4);
        String servisolcumu = besinCursor.getString(5);
        String s_servisIsimNO = besinCursor.getString(6);
        String S_servingNameWord = besinCursor.getString(7);

        String s_enerji = besinCursor.getString(8);
        String s_Protein = besinCursor.getString(9);
        String s_karbınhidrat = besinCursor.getString(10);
        String stringFat = besinCursor.getString(11);
        String s_hesaplananEnerji = besinCursor.getString(12);
        String s_hesaplananProtein = besinCursor.getString(13);
        String s_hesaplananKarbon = besinCursor.getString(14);
        String s_hesaplananYag = besinCursor.getString(15);

        String s_kullaniciID = besinCursor.getString(16);
        String s_barkod = besinCursor.getString(17);
        String s_kategoriID = besinCursor.getString(18);
        String foto1 = besinCursor.getString(19);
        String foto2 = besinCursor.getString(20);
        String foto3 = besinCursor.getString(21);


        EditText ed_besinISmi = (EditText) getView().findViewById(R.id.et_besinIsmi);
        ed_besinISmi.setText(s_Isim);


        TextView tv_besinUreticiISmi = (TextView) getView().findViewById(R.id.et_uretici);
        tv_besinUreticiISmi.setText(s_ureticiISmi);

        EditText et_besinAciklama = (EditText) getView().findViewById(R.id.et_aciklama);
        et_besinAciklama.setText(s_Aciklama);

        EditText ed_besinBarkod = (EditText) getView().findViewById(R.id.et_barkod);
        ed_besinBarkod.setText(s_barkod);

       
        String s_doldur[] = new String[] {
                "_id",
                "kategori_isim",
                "kategori_pid"
        };

        Cursor dbCursorCurrentFoodCategory = veri.sec("kategoriler", s_doldur, "_id", s_kategoriID, "kategori_isim", "ASC");

        String currentFoodCategoryID = dbCursorCurrentFoodCategory.getString(2);

        Cursor dbCursorSub = veri.sec("kategoriler", s_doldur, "kategori_pid", currentFoodCategoryID, "kategori_isim", "ASC");

        int veriCSayisi = dbCursorSub.getCount();
        String[] kategoriSinif = new String[veriCSayisi];

        int kategoriindex = 0;
        String secilenAltkategori = "0";

        for(int i = 0; i< veriCSayisi; i++){
   
            kategoriSinif[i] = dbCursorSub.getString(1).toString();

            if(dbCursorSub.getString(0).toString().equals(s_kategoriID)){
                kategoriindex = i;
                secilenAltkategori = dbCursorSub.getString(2).toString();
            }

            dbCursorSub.moveToNext();
        }


        Spinner sp_sub = (Spinner) getActivity().findViewById(R.id.spKategoriS);
        ArrayAdapter<String> adt = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, kategoriSinif);
        sp_sub.setAdapter(adt);

        sp_sub.setSelection(kategoriindex);

    
        Cursor veriCursor = veri.sec("kategoriler", s_doldur, "kategori_pid", "0", "kategori_isim", "ASC");

        veriCSayisi = veriCursor.getCount();
        String[] arraySpinnerMainCategories = new String[veriCSayisi];

        int kategoriSec = 0;

        for(int i = 0; i< veriCSayisi; i++){
            arraySpinnerMainCategories[i] = veriCursor.getString(1).toString();


            if(veriCursor.getString(0).toString().equals(secilenAltkategori)){
                kategoriSec = i;
                kategoriIsmiSec = veriCursor.getString(1).toString();
 
            }
            veriCursor.moveToNext();
        }


        Spinner spmain = (Spinner) getActivity().findViewById(R.id.spkategorim);
        ArrayAdapter<String> adapterMain = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, arraySpinnerMainCategories);
        spmain.setAdapter(adapterMain);

        spmain.setSelection(kategoriSec);
   
        EditText et_TextvebesinBoyutu = (EditText) getView().findViewById(R.id.etBesinBoyutu);
        et_TextvebesinBoyutu.setText(s_servisBoyutu);

        EditText et_besinOlcum = (EditText) getView().findViewById(R.id.etbesinOlcum);
        et_besinOlcum.setText(servisolcumu);

        EditText et_besinNumarasi = (EditText) getView().findViewById(R.id.et_besinnO);
        et_besinNumarasi.setText(s_servisIsimNO);

        EditText et_besinKelme = (EditText) getView().findViewById(R.id.et_besinK);
        et_besinKelme.setText(S_servingNameWord);

        //kalori tablosu


        EditText et_yuzdelikEnerji = (EditText) getView().findViewById(R.id.et_besinEnerjiyuzdesi);
        et_yuzdelikEnerji.setText(s_enerji);


        EditText et_yuzdelikProtein = (EditText) getView().findViewById(R.id.et_proteinEnerjiYzdesi);
        et_yuzdelikProtein.setText(s_Protein);


        EditText et_yuzdelikKarbonhidrat = (EditText) getView().findViewById(R.id.et_karonhidratYuzde);
        et_yuzdelikKarbonhidrat.setText(s_karbınhidrat);


        EditText editTextEditFoodFatPerHundred = (EditText) getView().findViewById(R.id.et_yagzyuzde);
        editTextEditFoodFatPerHundred.setText(stringFat);


        spmain.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                String selectedItem = parent.getItemAtPosition(position).toString();

                besinkategoriSecim(selectedItem);
            }
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });


        Button besinDuzenlemeButonu = (Button)getActivity().findViewById(R.id.besinEklebutonu);
        besinDuzenlemeButonu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                besinDuzenleme();
            }
        });

        veri.close();


    }
    public void besinkategoriSecim(String selectedItemCategoryName){
        if(!(selectedItemCategoryName.equals(kategoriIsmiSec))){

            Verit veri = new Verit(getActivity());
            veri.open();

            String selectedSQL = veri.aktarma(selectedItemCategoryName);
            String s_Alan[] = new String[] {
                    "_id",
                    "kategori_isim",
                    "kategori_pid"
            };
            Cursor anakategoriAra = veri.sec("kategoriler", s_Alan, "kategori_isim", selectedSQL);
            String kategoriID  = anakategoriAra.getString(0).toString();
            String s_kategoriSQL = veri.aktarma(kategoriID);


            Cursor veriCursorSQL = veri.sec("kategoriler", s_Alan, "kategori_pid", s_kategoriSQL, "kategori_isim", "ASC");

            int veriCursonSayi = veriCursorSQL.getCount();
            String[] arraySpinnerCategoriesSub = new String[veriCursonSayi];

            for(int x=0;x<veriCursonSayi;x++){
                arraySpinnerCategoriesSub[x] = veriCursorSQL.getString(1).toString();
                veriCursorSQL.moveToNext();
            }

            Spinner spinnerSubCat = (Spinner) getActivity().findViewById(R.id.spKategoriS);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_spinner_item, arraySpinnerCategoriesSub);
            spinnerSubCat.setAdapter(adapter);

            veri.close();
        }
    }

    private void besinDuzenleme(){    //*****************

        Verit veriler = new Verit(getActivity());
        veriler.open();

        int hata = 0;

        long rowID = Long.parseLong(ID_tut);


        EditText et_besinIsim = (EditText)getActivity().findViewById(R.id.et_besinIsmi);
        String s_isim = et_besinIsim.getText().toString();
        String s_isimSQL = veriler.aktarma(s_isim);
        if(s_isim.equals("")){
            Toast.makeText(getActivity(), "Lütfen bir isim giriniz", Toast.LENGTH_SHORT).show();
            hata = 1;
        }

        EditText et_markagir = (EditText)getActivity().findViewById(R.id.et_uretici);
        String s_marka = et_markagir.getText().toString();
        String s_markaSQL = veriler.aktarma(s_marka);
        if(s_marka.equals("")){
            Toast.makeText(getActivity(), "Lütfen bir üretici firma ismi giriniz", Toast.LENGTH_SHORT).show();
            hata = 1;
        }

        EditText et_besinAciklamasi = (EditText)getActivity().findViewById(R.id.et_aciklama);
        String s_aciklama = et_besinAciklamasi.getText().toString();
        String s_aciklamaSQL = veriler.aktarma(s_aciklama);


        EditText et_barkodbilgisi = (EditText)getActivity().findViewById(R.id.et_barkod);
        String s_barkod = et_barkodbilgisi.getText().toString();
        String s_barkodSQL = veriler.aktarma(s_barkod);

        Spinner s_kategori = (Spinner)getActivity().findViewById(R.id.spKategoriS);
        int i_kategoriIndex = s_kategori.getSelectedItemPosition();
        String s_kategoriIsmi = s_kategori.getSelectedItem().toString();

        String s_sb_kategoriISmiSQL = veriler.aktarma(s_kategoriIsmi);
        String spinnerFields[] = new String[] {
                "_id",
                "kategori_isim",
                "kategori_pid"
        };
        Cursor altkategoriAra = veriler.sec("kategoriler", spinnerFields, "kategori_isim", s_sb_kategoriISmiSQL);
        String s_kategoriID  = altkategoriAra.getString(0).toString();
        String kategoriSQL = veriler.aktarma(s_kategoriID);

        EditText et_besinBoyut = (EditText)getActivity().findViewById(R.id.etBesinBoyutu);
        String stringSize = et_besinBoyut.getText().toString();
        String stringSizeSQL = veriler.aktarma(stringSize);
        double d_servisBoyutu = 0;
        if(stringSize.equals("")){
            Toast.makeText(getActivity(), "Lütfen bir sayisal deger girin", Toast.LENGTH_SHORT).show();
            hata = 1;
        }
        else{
            try {
                d_servisBoyutu = Double.parseDouble(stringSize);
            }
            catch(NumberFormatException nfe) {
                Toast.makeText(getActivity(), "Servis miktari numerik bir deger olmali", Toast.LENGTH_SHORT).show();
                hata = 1;
            }
        }

        EditText et_besinOlcum = (EditText)getActivity().findViewById(R.id.etbesinOlcum);
        String s_olcum = et_besinOlcum.getText().toString();
        String s_olcumSQL = veriler.aktarma(s_olcum);
        if(s_olcum.equals("")){
            Toast.makeText(getActivity(), "Lütfen ölcüm birimi giriniz", Toast.LENGTH_SHORT).show();
            hata = 1;
        }

        EditText et_besinNo = (EditText)getActivity().findViewById(R.id.et_besinnO);
        String stringNo = et_besinNo.getText().toString();
        String stringgNo = veriler.aktarma(stringNo);
        if(stringNo.equals("")){
            Toast.makeText(getActivity(), "Lütfen bir numerik deger giriniz", Toast.LENGTH_SHORT).show();
            hata = 1;
        }

        EditText et_besin = (EditText)getActivity().findViewById(R.id.et_besinK);
        String kelimebesin = et_besin.getText().toString();
        String s_kelimeSQL = veriler.aktarma(kelimebesin);
        if(kelimebesin.equals("")){
            Toast.makeText(getActivity(), "Lütfen bir kelime giriniz", Toast.LENGTH_SHORT).show();
            hata = 1;
        }

        // Eneerji degerleri
        EditText et_yuzdelikEnerji = (EditText)getActivity().findViewById(R.id.et_besinEnerjiyuzdesi);
        String enerjiYuzdesi = et_yuzdelikEnerji.getText().toString();
        enerjiYuzdesi = enerjiYuzdesi.replace(",", ".");
        double yuzdelikEnerji = 0;
        if(enerjiYuzdesi.equals("")){
            Toast.makeText(getActivity(), "Lütfen bir enerji degeri girin.", Toast.LENGTH_SHORT).show();
            hata = 1;
        }
        else{
            try {
                yuzdelikEnerji = Double.parseDouble(enerjiYuzdesi);
            }
            catch(NumberFormatException nfe) {
                Toast.makeText(getActivity(), "Enerji sayisal deger olmalı", Toast.LENGTH_SHORT).show();
                hata = 1;
            }
        }
        String stringEnergyPerHundredSQL = veriler.aktarma(enerjiYuzdesi);

        EditText et_proteinYuzdesi = (EditText)getActivity().findViewById(R.id.et_proteinEnerjiYzdesi);
        String yuzdelikprotein = et_proteinYuzdesi.getText().toString();
        yuzdelikprotein = yuzdelikprotein.replace(",", ".");
        double s_doubleProteinyuzdesi = 0;
        if(yuzdelikprotein.equals("")){
            Toast.makeText(getActivity(), "Lütfen bir protein miktarı giriniz", Toast.LENGTH_SHORT).show();
            hata = 1;
        }
        else{
            try {
                s_doubleProteinyuzdesi = Double.parseDouble(yuzdelikprotein);
            }
            catch(NumberFormatException nfe) {
                Toast.makeText(getActivity(), "Protein bir sayisal deger olmali.\n" + "Siz şunu yazdiniz : " + yuzdelikprotein, Toast.LENGTH_SHORT).show();
                hata = 1;
            }
        }
        String stringProteinsPerHundredSQL = veriler.aktarma(yuzdelikprotein);


        EditText et_karbonhidratyuzdesi = (EditText)getActivity().findViewById(R.id.et_karonhidratYuzde);
        String s_karbonhidratYuzdesi = et_karbonhidratyuzdesi.getText().toString();
        s_karbonhidratYuzdesi = s_karbonhidratYuzdesi.replace(",", ".");
        double d_karbonhidratYuzdesii = 0;
        if(s_karbonhidratYuzdesi.equals("")){
            Toast.makeText(getActivity(), "Karbonhidrat degerini Giriniz", Toast.LENGTH_SHORT).show();
            hata = 1;
        }
        else{
            try {
                d_karbonhidratYuzdesii = Double.parseDouble(s_karbonhidratYuzdesi);
            }
            catch(NumberFormatException nfe) {
                Toast.makeText(getActivity(), "Karbonhidrat sayisal bir deger olmalı. \n Siz şunu yazdınız : " + s_karbonhidratYuzdesi, Toast.LENGTH_SHORT).show();
                hata = 1;
            }
        }
        String stringCarbsPerHundredSQL = veriler.aktarma(s_karbonhidratYuzdesi);

        // yag
        EditText et_yagYuzdesi = (EditText)getActivity().findViewById(R.id.et_yagzyuzde);
        String s_yagYuzdesi = et_yagYuzdesi.getText().toString();
        s_yagYuzdesi = s_yagYuzdesi.replace(",", ".");
        double d_yagYuzde = 0;
        if(s_yagYuzdesi.equals("")){
            Toast.makeText(getActivity(), "Lütfen bir yag degeri girin", Toast.LENGTH_SHORT).show();
            hata = 1;
        }
        else{
            try {
                d_yagYuzde = Double.parseDouble(s_yagYuzdesi);
            }
            catch(NumberFormatException nfe) {
                Toast.makeText(getActivity(), "Lütfen Numerik bir deger giriniz.", Toast.LENGTH_SHORT).show();
                hata = 1;
            }
        }
        String ss_yagYuzdesi = veriler.aktarma(s_yagYuzdesi);


        if(hata == 0){
            //ogun icin kalori tablosu
            double hesaplananKalori = Math.round((yuzdelikEnerji*d_servisBoyutu)/100);
            double hesaplananProtein = Math.round((s_doubleProteinyuzdesi*d_servisBoyutu)/100);
            double hesaplananKarbonhidrat = Math.round((d_karbonhidratYuzdesii*d_servisBoyutu)/100);
            double hesaplananYag = Math.round((d_yagYuzde*d_servisBoyutu)/100);

            String s_hesaplanmisKalori = "" + hesaplananKalori;
            String s_hesaplanmilProtein = "" + hesaplananProtein;
            String s_hesaplanmisKarbonhidrat = "" + hesaplananKarbonhidrat;
            String s_hesaplananYag = "" + hesaplananYag;

            String hesaplananErnerjiSQL = veriler.aktarma(s_hesaplanmisKalori);
            String hesaplananproteinSQL = veriler.aktarma(s_hesaplanmilProtein);
            String hesaplannaKarbonhidratSQL = veriler.aktarma(s_hesaplanmisKarbonhidrat);
            String hesaplananYagSQL = veriler.aktarma(s_hesaplananYag);


            String fields[] = new String[] {
                    "gida_isim",
                    "gida_Marka",
                    "gida_aciklama",
                    "gida_servisGram",
                    "gida_servisGramolcm",
                    "gida_servisAdet",
                    "gida_servisAdetolcm",
                    "gida_enerji",
                    "gida_protein",
                    "gida_karbonhidrat",
                    "gida_yag",
                    "gida_olculenEnerji",
                    "gida_olculenProtein",
                    "gida_olculen_karbonhidrat",
                    "gida_olculen_yag",
                    "gida_bar",
                    "gida_kategori"
            };
            String values[] = new String[] {
                    s_isimSQL,
                    s_markaSQL,
                    s_aciklamaSQL,
                    stringSizeSQL,
                    s_olcumSQL,
                    stringgNo,
                    s_kelimeSQL,
                    stringEnergyPerHundredSQL,
                    stringProteinsPerHundredSQL,
                    stringCarbsPerHundredSQL,
                    ss_yagYuzdesi,
                    hesaplananErnerjiSQL,
                    hesaplananproteinSQL,
                    hesaplannaKarbonhidratSQL,
                    hesaplananYagSQL,
                    s_barkodSQL,
                    kategoriSQL
            };

            long l_IDval = Long.parseLong(ID_tut);

            veriler.guncelle("gidalar", "_id", l_IDval, fields, values);

            // Toast
            Toast.makeText(getActivity(), "Değişiklikler başarıyla kaydedildi", Toast.LENGTH_SHORT).show();

        } // hata == 0

        veriler.close();
    } // besinDuzenleme




    public void besinSil(){

        int idno = R.layout.gidasil;
        setAnahat(idno);

        Button vazgecButonu = (Button)getActivity().findViewById(R.id.vazgecbutonu);
        vazgecButonu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                besinSilmedenvazgec();
            }
        });

        Button silmeDogrulama = (Button)getActivity().findViewById(R.id.silmeyiDogrula);
        silmeDogrulama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                besinsilmeDogrulama();
            }
        });

    } // besinSil
    public void besinSilmedenvazgec(){

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.doldur, new GidaSec(), GidaSec.class.getName()).commit();


    }
    public void besinsilmeDogrulama() {


        Verit veriler = new Verit(getActivity());
        veriler.open();

        long l_IDno = Long.parseLong(ID_tut);

        long currentIDSQL = veriler.aktarma(l_IDno);

        veriler.sil("gidalar", "_id", currentIDSQL);

        veriler.close();

        Toast.makeText(getActivity(), "Besin silindi", Toast.LENGTH_LONG).show();

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.doldur, new GidaSec(), GidaSec.class.getName()).commit();

      }
    public void besinEkleme(){

        Verit veriiler = new Verit(getActivity());
        veriiler.open();

        int id_no = R.layout.gidaduzenle;
        setAnahat(id_no);

        ((Menu)getActivity()).getSupportActionBar().setTitle("Gida ekleme");

        String spinnerFields[] = new String[] {
                "_id",
                "kategori_isim",
                "kategori_pid"
        };
        Cursor veriCursor = veriiler.sec("kategoriler", spinnerFields, "kategori_pid", "0", "kategori_isim", "ASC");

        int cursorSayi = veriCursor.getCount();
        String[] kategorilerMain = new String[cursorSayi];

        for(int x=0;x<cursorSayi;x++){
            kategorilerMain[x] = veriCursor.getString(1).toString();
            veriCursor.moveToNext();
        }


        Spinner s_cMain = (Spinner) getActivity().findViewById(R.id.spkategorim);
        ArrayAdapter<String> adapterMain = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, kategorilerMain);
        s_cMain.setAdapter(adapterMain);

        s_cMain.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                String selectedItem = parent.getItemAtPosition(position).toString(); //secilen item

                besinkategoriSecim(selectedItem);
            }
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

        Button besinDuzenlemeButonu = (Button)getActivity().findViewById(R.id.besinEklebutonu);
        besinDuzenlemeButonu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                besinEklemeAction();
            }
        });

        veriiler.close();
    } // besinEkleme


    public void besinEklemeAction(){

        Verit verilerr = new Verit(getActivity());
        verilerr.open();

        int hata_ = 0;

        EditText et_foodName = (EditText)getActivity().findViewById(R.id.et_besinIsmi);
        String s_name = et_foodName.getText().toString();
        String stringNameSQLS = verilerr.aktarma(s_name);
        if(s_name.equals("")){
            Toast.makeText(getActivity(), "Lütfen bir isim girin", Toast.LENGTH_SHORT).show();
            hata_ = 1;
        }


        EditText et_ureticiIsmi = (EditText)getActivity().findViewById(R.id.et_uretici);
        String s_marka_ = et_ureticiIsmi.getText().toString();
        String s_markaSQL_ = verilerr.aktarma(s_marka_);
        if(s_marka_.equals("")){
            Toast.makeText(getActivity(), "Lütfen bir üretici ismi girin", Toast.LENGTH_SHORT).show();
            hata_ = 1;
        }


        EditText et_besinAciklamasi = (EditText)getActivity().findViewById(R.id.et_aciklama);
        String s_aciklama_ = et_besinAciklamasi.getText().toString();
        String s_aciklamaSQL = verilerr.aktarma(s_aciklama_);

        EditText et_besinBarkod = (EditText)getActivity().findViewById(R.id.et_barkod);
        String s_barkod = et_besinBarkod.getText().toString();
        String s_barkodSQQL = verilerr.aktarma(s_barkod);


        Spinner s_subCat = (Spinner)getActivity().findViewById(R.id.spKategoriS);
        int i_skategoriIndex = s_subCat.getSelectedItemPosition();
        String Subkategoriismi = s_subCat.getSelectedItem().toString();

        String subkategorinameSQL = verilerr.aktarma(Subkategoriismi);
        String sdoldur[] = new String[] {
                "_id",
                "kategori_isim",
                "kategori_pid"
        };
        Cursor spinnerSub = verilerr.sec("kategoriler", sdoldur, "kategori_isim", subkategorinameSQL);
        String spinnerSubkategori  = spinnerSub.getString(0).toString();
        String spinnerSubkategoriSQL = verilerr.aktarma(spinnerSubkategori);


        EditText et_besinMiktarı = (EditText)getActivity().findViewById(R.id.etBesinBoyutu);
        String s_miktar = et_besinMiktarı.getText().toString();
        String s_miktarSQL = verilerr.aktarma(s_miktar);
        double s_servisMiktarı = 0;
        if(s_miktar.equals("")){
            Toast.makeText(getActivity(), "Lütfen bir ölcü girin", Toast.LENGTH_SHORT).show();
            hata_ = 1;
        }
        else{
            try {
                s_servisMiktarı = Double.parseDouble(s_miktar);
            }
            catch(NumberFormatException nfe) {
                Toast.makeText(getActivity(), "Numerik bri deger girmelisiniz", Toast.LENGTH_SHORT).show();
                hata_ = 1;
            }
        }

        EditText et_besinolcuBrm = (EditText)getActivity().findViewById(R.id.etbesinOlcum);
        String s_olcuBrm = et_besinolcuBrm.getText().toString();
        String s_olcumSQL = verilerr.aktarma(s_olcuBrm);
        if(s_olcuBrm.equals("")){
            Toast.makeText(getActivity(), "Lütfen bir olcu birimi girin", Toast.LENGTH_SHORT).show();
            hata_ = 1;
        }


        EditText et_besinNo = (EditText)getActivity().findViewById(R.id.et_besinnO);
        String s_number_ = et_besinNo.getText().toString();
        String s_numaraSQL = verilerr.aktarma(s_number_);
        if(s_number_.equals("")){
            Toast.makeText(getActivity(), "Lütfen numerik bir deger girin", Toast.LENGTH_SHORT).show();
            hata_ = 1;
        }


        EditText et_besinkelime = (EditText)getActivity().findViewById(R.id.et_besinK);
        String s_besinKelime = et_besinkelime.getText().toString();
        String besinKelmeSQL = verilerr.aktarma(s_besinKelime);
        if(s_besinKelime.equals("")){
            Toast.makeText(getActivity(), "Lütfen bir kelime girin.", Toast.LENGTH_SHORT).show();
            hata_ = 1;
        }


        EditText et_besinEnerjiYuzdesidegeri = (EditText)getActivity().findViewById(R.id.et_besinEnerjiyuzdesi);
        String s_besinEnerjiYuzdesi = et_besinEnerjiYuzdesidegeri.getText().toString();
        s_besinEnerjiYuzdesi = s_besinEnerjiYuzdesi.replace(",", ".");
        double d_besnEnerjiYuzdesi = 0;
        if(s_besinEnerjiYuzdesi.equals("")){
            Toast.makeText(getActivity(), "Lütfen bir Enerji degeri giriniz", Toast.LENGTH_SHORT).show();
            hata_ = 1;
        }
        else{
            try {
                d_besnEnerjiYuzdesi = Double.parseDouble(s_besinEnerjiYuzdesi);
            }
            catch(NumberFormatException nfe) {
                Toast.makeText(getActivity(), "Lütfen numerik bir deger giriniz", Toast.LENGTH_SHORT).show();
                hata_ = 1;
            }
        }
        String s_enerjiyuzdesi = verilerr.aktarma(s_besinEnerjiYuzdesi);


        EditText et_besinproteinyuzdesi = (EditText)getActivity().findViewById(R.id.et_proteinEnerjiYzdesi);
        String s_besinproteinYuzdesi = et_besinproteinyuzdesi.getText().toString();
        s_besinproteinYuzdesi = s_besinproteinYuzdesi.replace(",", ".");
        double d_besinproteinyuzdesi = 0;
        if(s_besinproteinYuzdesi.equals("")){
            Toast.makeText(getActivity(), "Lütfen protein degeri giriniz.", Toast.LENGTH_SHORT).show();
            hata_ = 1;
        }
        else{
            try {
                d_besinproteinyuzdesi = Double.parseDouble(s_besinproteinYuzdesi);
            }
            catch(NumberFormatException nfe) {
                Toast.makeText(getActivity(), "Lütfen numerik bir deger girin.\n" + "You wrote: " + s_besinproteinYuzdesi, Toast.LENGTH_SHORT).show();
                hata_ = 1;
            }
        }
        String s_proteinyuzdesi = verilerr.aktarma(s_besinproteinYuzdesi);


        EditText etkarbonhidratYuzdesi = (EditText)getActivity().findViewById(R.id.et_karonhidratYuzde);
        String s_karbonhidratYuzdesi = etkarbonhidratYuzdesi.getText().toString();
        s_karbonhidratYuzdesi = s_karbonhidratYuzdesi.replace(",", ".");
        double d_karbonhidratYuzdesi = 0;
        if(s_karbonhidratYuzdesi.equals("")){
            Toast.makeText(getActivity(), "Lütfen karbonhidrat degeri giriniz.", Toast.LENGTH_SHORT).show();
            hata_ = 1;
        }
        else{
            try {
                d_karbonhidratYuzdesi = Double.parseDouble(s_karbonhidratYuzdesi);
            }
            catch(NumberFormatException nfe) {
                Toast.makeText(getActivity(), "Karbonhidrat degeri numerik olmalı. Sizin yazdıgınız : " + s_karbonhidratYuzdesi, Toast.LENGTH_SHORT).show();
                hata_ = 1;
            }
        }
        String s_karbonhidratYuzdedegeri = verilerr.aktarma(s_karbonhidratYuzdesi);


        EditText et_besinYagYuzdesi = (EditText)getActivity().findViewById(R.id.et_yagzyuzde);
        String s_yagYuzdesi = et_besinYagYuzdesi.getText().toString();
        s_yagYuzdesi = s_yagYuzdesi.replace(",", ".");
        double d_yagYuzdesi = 0;
        if(s_yagYuzdesi.equals("")){
            Toast.makeText(getActivity(), "Lütfen bir yag degeri giriniz", Toast.LENGTH_SHORT).show();
            hata_ = 1;
        }
        else{
            try {
                d_yagYuzdesi = Double.parseDouble(s_yagYuzdesi);
            }
            catch(NumberFormatException nfe) {
                Toast.makeText(getActivity(), "Yag degeri numerik bir deger olmalı.", Toast.LENGTH_SHORT).show();
                hata_ = 1;
            }
        }
        String s_yagYuzdelikDeger = verilerr.aktarma(s_yagYuzdesi);

        if(hata_ == 0){

            double hesaplananEnerjiMiktari = Math.round((d_besnEnerjiYuzdesi*s_servisMiktarı)/100);
            double hesaplananProteinMiktari = Math.round((d_besinproteinyuzdesi*s_servisMiktarı)/100);
            double hesaplananKarbonhidratMiktari = Math.round((d_karbonhidratYuzdesi*s_servisMiktarı)/100);
            double hesaplananYagmiktarii = Math.round((d_yagYuzdesi*s_servisMiktarı)/100);

            String s_hesaplananEnerjiMiktari = "" + hesaplananEnerjiMiktari;
            String s_hesaplananProteinMiktari = "" + hesaplananProteinMiktari;
            String s_hesaplananKarbonhidratMiktari = "" + hesaplananKarbonhidratMiktari;
            String s_hesaplananYagMiktari = "" + hesaplananYagmiktarii;

            String s_hesaplananEnerjiMiktariSQL = verilerr.aktarma(s_hesaplananEnerjiMiktari);
            String hesaplananProteinMiktariSQL = verilerr.aktarma(s_hesaplananProteinMiktari);
            String hesaplananKarbonhidratMiktariSQL = verilerr.aktarma(s_hesaplananKarbonhidratMiktari);
            String hesaplananYagMiktariSQL = verilerr.aktarma(s_hesaplananYagMiktari);


            String fields =
                    "_id, " +
                            "gida_isim, " +
                            "gida_Marka, " +
                            "gida_aciklama, " +
                            "gida_servisGram, " +
                            "gida_servisGramolcm, " +
                            "gida_servisAdet, " +
                            "gida_servisAdetolcm, " +
                            "gida_enerji, " +
                            "gida_protein, " +
                            "gida_karbonhidrat, " +
                            "gida_yag, " +
                            "gida_olculenEnerji, " +
                            "gida_olculenProtein, " +
                            "gida_olculen_karbonhidrat, " +
                            "gida_olculen_yag, " +
                            "gida_bar, " +
                            "gida_kategori";

            String values =
                    "NULL, " +
                            stringNameSQLS + ", " +
                            s_markaSQL_ + ", " +
                            s_aciklamaSQL + ", " +
                            s_miktarSQL + ", " +
                            s_olcumSQL + ", " +
                            s_numaraSQL + ", " +
                            besinKelmeSQL + ", " +
                            s_enerjiyuzdesi + ", " +
                            s_proteinyuzdesi + ", " +
                            s_karbonhidratYuzdedegeri + ", " +
                            s_yagYuzdelikDeger + ", " +
                            s_hesaplananEnerjiMiktariSQL + ", " +
                            hesaplananProteinMiktariSQL + ", " +
                            hesaplananKarbonhidratMiktariSQL + ", " +
                            hesaplananYagMiktariSQL + ", " +
                            s_barkodSQQL + ", " +
                            spinnerSubkategoriSQL;


            verilerr.insert("gidalar", fields, values);

            Toast.makeText(getActivity(), "Besin başarıyla oluşturuldu.", Toast.LENGTH_SHORT).show();

            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.doldur, new GidaSec(), GidaSec.class.getName()).commit();

        }

        verilerr.close();
    } // besinEklemeAction

    public void ogunebesinekle(){
  
        int newViewID = R.layout.anasayfaogun;
        setAnahat(newViewID);


        TextView tv_kahfalti = (TextView)getActivity().findViewById(R.id.tv_kahfalti);
        tv_kahfalti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                secilenogunebelirlenenbesiniekleme(0);
            }
        });

        TextView tv_ogleYemegi = (TextView)getActivity().findViewById(R.id.tv_oglen);
        tv_ogleYemegi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                secilenogunebelirlenenbesiniekleme(1);
            }
        });

        TextView tv_aktiviteOncesi = (TextView)getActivity().findViewById(R.id.tv_aktiviteoncesi);
        tv_aktiviteOncesi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                secilenogunebelirlenenbesiniekleme(2);
            }
        });

        TextView tv_aktivideSonrasi = (TextView)getActivity().findViewById(R.id.tv_aktSonrasi);
        tv_aktivideSonrasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                secilenogunebelirlenenbesiniekleme(3);
            }
        });

        TextView tv_aksamYemegi = (TextView)getActivity().findViewById(R.id.tv_aksm);
        tv_aksamYemegi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                secilenogunebelirlenenbesiniekleme(4);
            }
        });

        TextView tv_aburcubur = (TextView)getActivity().findViewById(R.id.tv_atistirma);
        tv_aburcubur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                secilenogunebelirlenenbesiniekleme(5);
            }
        });

        TextView tv_extra = (TextView)getActivity().findViewById(R.id.textViewExt);
        tv_extra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                secilenogunebelirlenenbesiniekleme(6);
            }
        });


    } // ogunebesinekle


    public void secilenogunebelirlenenbesiniekleme(int mealNumber){

        Fragment fragment = null;
        Class fragmentClass = null;
        fragmentClass = OguneEkle.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Bundle bundle = new Bundle();
        bundle.putString("mealNumber", ""+mealNumber);
        bundle.putString("currentFoodId", ""+mealNumber);
        bundle.putString("action", "tıklandigindeKategoridekiBesinleriListele");
        fragment.setArguments(bundle);

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.doldur, fragment).commit();


    }


    // TODO: Rename method, guncelle argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mlListener != null) {
            mlListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mlListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " !");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mlListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
