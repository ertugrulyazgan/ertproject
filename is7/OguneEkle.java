package com.example.deeptrancer.diyetuygulamam496;

/**
 * Created by deeptrancer on 2.03.2018.
 */



        import android.content.Context;
        import android.database.Cursor;
        import android.net.Uri;
        import android.os.Bundle;
        import android.support.v4.app.Fragment;
        import android.support.v4.app.FragmentManager;
        import android.text.Editable;
        import android.text.TextWatcher;
        import android.view.LayoutInflater;
        import android.view.MenuInflater;
        import android.view.MenuItem;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.AdapterView;
        import android.widget.ArrayAdapter;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ListView;
        import android.widget.TextView;
        import android.widget.Toast;
        import java.util.ArrayList;

        import java.util.Calendar;

public class OguneEkle extends Fragment {


    private View mainView;
    private Cursor kategoriCursor;   // o satır degeri icin. veritabanındaki
    private Cursor gidaCursor;

    // üst menu (toolbar) kullanımı
    private MenuItem menuOgunDuzenle;
    private MenuItem menuDegerSil;

    // degerleri tut
    private String tut_hangiOgun;
    private String tut_kategoriID;
    private String tut_kategoriIsmi;
    private String tut_kategoriGida;
    private String tut_gidaIsmine;

    private boolean porsiyonAdet;
    private boolean porsiyonGram;


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mlListener;


    //constructor eklemem gerekti yok calısmıyor. default constructor
    public OguneEkle() {

    }

    //olusturma
    public static OguneEkle newInstance(String param1, String param2) {
        OguneEkle fragment = new OguneEkle();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    // program olusturldgnda bunları calistir.
    // üst menununde buna göre ayarlanması gerek.
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //kullanıcıya mesaj gösteriyorum
        ((Menu)getActivity()).getSupportActionBar().setTitle("Öğün için besin ekle");


        String hangiEylem = "";
        Bundle bundle = this.getArguments();
        if(bundle != null){
            tut_hangiOgun = bundle.getString("mealNumber");
            tut_kategoriGida = bundle.getString("currentFoodId");
            hangiEylem = bundle.getString("action");
        }

        if(hangiEylem.equals("")) {
            // kategorileri oluştur
            list("0", "");
        }
        else if(hangiEylem.equals("kategoridekibesinsecildi")){
            kategoriGel();
        }

        // menu oluştur


    } //

    // secim yapmamız icin
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.gidaekle, container, false);
        return mainView;
    }

    // gösterilen degeleri siliyorum.
    private void setMainView(int id){
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mainView = inflater.inflate(id, null);
        ViewGroup rootView = (ViewGroup) getView();
        rootView.removeAllViews();
        rootView.addView(mainView);
    }


    // üstteki menude tekrar menuye girebilmek için . toolbar icin
    public void onCreateOptionsMenu(android.view.Menu menu, MenuInflater inflater) {

        ((Menu)getActivity()).getMenuInflater().inflate(R.menu.manu_kategori, menu);

        // menuyu oluşturma. gecerli degerleri buraya atıyorum.
        menuOgunDuzenle = menu.findItem(R.id.action_edit);
        menuDegerSil = menu.findItem(R.id.action_delete);

        // ekranda gorunurlugu kapat.
        menuOgunDuzenle.setVisible(false);
        menuDegerSil.setVisible(false);
    }

    // toolbardaki icona tıklayınca çıkacak pencere
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        int id = menuItem.getItemId();
        return super.onOptionsItemSelected(menuItem);
    }

    // kategorilerle listeyi oluştur.
    public void list(String s_kategorisinif, String stringCatgoryName){

        // veritabanı kullanarak
        Verit veriler = new Verit(getActivity());
        veriler.open();

        // kategorileri al
        String fields[] = new String[] {
                "_id",
                "kategori_isim",
                "kategori_pid"
        };
        kategoriCursor = veriler.sec("kategoriler", fields, "kategori_pid", s_kategorisinif, "kategori_isim", "ASC");

        // kategoriler için string arrayi oluşturdum. burada tutacagim
        ArrayList<String> kategorileriTut = new ArrayList<String>();

        // tum kategorileri alip bu string e atıyorum.
        int kategorisayisi = kategoriCursor.getCount();
        for(int x=0;x<kategorisayisi;x++){
            kategorileriTut.add(kategoriCursor.getString(kategoriCursor.getColumnIndex("kategori_isim")));
            kategoriCursor.moveToNext();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, kategorileriTut);

        ListView lv = (ListView)getActivity().findViewById(R.id.listViewAddFoodToDiary);
        lv.setAdapter(adapter);

        if(s_kategorisinif.equals("0")) {
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    kategorileriListele(arg2);
                }
            });
        }

        veriler.close();

    }
    //tiklandiginda listele kategorilerle oluştur.
    public void kategorileriListele(int listItemIndexClicked){

        kategoriCursor.moveToPosition(listItemIndexClicked);

        // id numarası ve ismini almak için daha önce olusturdugum degiskenleri kullanıyorum.
        tut_kategoriID = kategoriCursor.getString(0);
        tut_kategoriIsmi = kategoriCursor.getString(1);
        String parentCategoryID = kategoriCursor.getString(2);
        ((Menu)getActivity()).getSupportActionBar().setTitle(" " + tut_kategoriIsmi + " dan besin ekle ..");


        // diger sınıfa gidelim.
        list(tut_kategoriID, tut_kategoriIsmi);


        // besinleri kategori listesinde gösterdm // calisti !!
        besinleriKGoster(tut_kategoriID, tut_kategoriIsmi, parentCategoryID);


    }

    //besinleri kategoride göstermek için
    public void besinleriKGoster(String categoryId, String kategoriIsmi, String kategoriPID){
        if(!(kategoriPID.equals("0"))) {


            //kalorilerle beraber listelenmesi için navigationda belirttigm sayfaya gec.
            int id = R.layout.gidalar;
            setMainView(id);

            Verit verile = new Verit(getActivity());
            verile.open();

            // kategoriler
            String fields[] = new String[] {
                    "_id",
                    "gida_isim",
                    "gida_Marka",
                    "gida_aciklama",
                    "gida_servisGram",
                    "gida_servisGramolcm",
                    "gida_servisAdet",
                    "gida_servisAdetolcm",
                    "gida_olculenEnerji"
            };
            gidaCursor = verile.sec("gidalar", fields, "gida_kategori", categoryId, "gida_isim", "ASC");

            ListView i_item = (ListView)getActivity().findViewById(R.id.besinliste);

            // son islemle birlikte olustur.
            Gida adptr = new Gida(getActivity(), gidaCursor);

            try{
                i_item.setAdapter(adptr);
            }
            catch (Exception e){
                Toast.makeText(getActivity(), "E: " + e.toString(), Toast.LENGTH_LONG).show();
            }

            i_item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    tıklandigindeKategoridekiBesinleriListele(arg2);
                }
            });

            verile.close();

        } //kategori id esitse bunlar gecerli
    }


    public void kategoriGel() {
        Verit veriler = new Verit(getActivity());
        veriler.open();

        // burda degerleri görüntülememiz lazım.
        String fields[] = new String[] {
                "_id",
                "gida_isim",
                "gida_Marka",
                "gida_aciklama",
                "gida_servisGram",
                "gida_servisGramolcm",
                "gida_servisAdet",
                "gida_servisAdetolcm",
                "gida_olculenEnerji"
        };

        String stringBesinIDSQL = veriler.aktarma(tut_kategoriGida);

        gidaCursor = veriler.sec("gidalar", fields, "_id", stringBesinIDSQL);

        int simulatedIndex = 0;
        tıklandigindeKategoridekiBesinleriListele(simulatedIndex);

    } // kategoriGel


    public void tıklandigindeKategoridekiBesinleriListele(int listele){

        //besin icin porsiyon ve miktar bilgisi alıp, kalori degerlerini gösterip ogune ekleyip eklemek istemediğini sorup ekleme islemini gerçeklestiemek icin
        int id = R.layout.gidaeklegoster;
        setMainView(id);


        gidaCursor.moveToPosition(listele); // oraya odaklan

        // id numarasını ve ismini göstermek için al
        tut_kategoriGida = gidaCursor.getString(0);
        tut_gidaIsmine = gidaCursor.getString(1);

        // toolbarda gidanin eklenip eklenmemesine dair kullaniciya uyarı
        ((Menu)getActivity()).getSupportActionBar().setTitle(""+tut_gidaIsmine+" eklensin mi ?");
        Verit veriler = new Verit(getActivity());
        veriler.open();
        String fields[] = new String[] {
                "_id",
                "gida_isim",
                "gida_Marka",
                "gida_aciklama",
                "gida_servisGram",
                "gida_servisGramolcm",
                "gida_servisAdet",
                "gida_servisAdetolcm",
                "gida_enerji",
                "gida_protein",
                "gida_karbonhidrat",
                "gida_yag",
                "gida_olculenEnerji",
                "gida_olculenProtein",
                "gida_olculen_karbonhidrat",
                "gida_olculen_yag",
                "gida_kullaniciID",
                "gida_bar",
                "gida_kategori",
                "gida_fa",
                "gida_fb",
                "gida_fc"
        };
        String currentIdSQL = veriler.aktarma(tut_kategoriGida);
        Cursor cursorBesin = veriler.sec("gidalar", fields, "_id", currentIdSQL);

        String s_IDno = cursorBesin.getString(0);
        String s_Isim = cursorBesin.getString(1);
        String s_ureticiIsmi = cursorBesin.getString(2);
        String s_aciklama = cursorBesin.getString(3);
        String s_servisBoyutu = cursorBesin.getString(4);
        String s_servisOlcuB = cursorBesin.getString(5);
        String s_servisIsimN = cursorBesin.getString(6);
        String s_servisIsimKelime = cursorBesin.getString(7);
        String s_Enerji = cursorBesin.getString(8);
        String s_protein = cursorBesin.getString(9);
        String s_karbonhidrat = cursorBesin.getString(10);
        String s_yag = cursorBesin.getString(11);
        String s_olculenEnerji = cursorBesin.getString(12);
        String s_olculenProtein = cursorBesin.getString(13);
        String s_olculenKarbonhidrat = cursorBesin.getString(14);
        String s_olculenYag = cursorBesin.getString(15);
        String s_KullaniciID = cursorBesin.getString(16);
        String s_barkod = cursorBesin.getString(17);
        String s_kategoriID = cursorBesin.getString(18);
        String s_foto1 = cursorBesin.getString(19);
        String s_foto2 = cursorBesin.getString(20);
        String s_foto3 = cursorBesin.getString(21);


        // baslik
        TextView tv_besinIsmi = (TextView) getView().findViewById(R.id.tv_besinIsminiGoster);
        tv_besinIsmi.setText(s_Isim);


        TextView tc_besinUreticiIsmi = (TextView) getView().findViewById(R.id.tv_besinUreticiMarkasi);
        tc_besinUreticiIsmi.setText(s_ureticiIsmi);


        EditText et_porsiyonAdedi = (EditText)getActivity().findViewById(R.id.et_porsiyonAdediGir);
        et_porsiyonAdedi.setText(s_servisIsimN);

        TextView tv_adet = (TextView)getActivity().findViewById(R.id.tv_adetgir);
        tv_adet.setText(s_servisIsimKelime);

        EditText et_porsiyonmiktari = (EditText)getActivity().findViewById(R.id.et_porsiyonMiktariGr);
        et_porsiyonmiktari.setText(s_servisBoyutu);

        TextView besinHakkinda = (TextView) getView().findViewById(R.id.tv_besinHakkinda);
        String besinhakkinda = s_servisBoyutu + " " + s_servisOlcuB + " = " +
                s_servisIsimN  + " " + s_servisIsimKelime + ".";
        besinHakkinda.setText(besinhakkinda);


        TextView besinAciklamasi = (TextView) getView().findViewById(R.id.tv_besinAciklama);
        besinAciklamasi.setText(s_aciklama);

        // öğune ekleme tablosunun alt kısmında besin hakkında bilgiler olsun. bunları ekrana verelim
        TextView tv_yuzdelikEnerji = (TextView) getView().findViewById(R.id.tv_yuzdelikBesinEnerjisi);
        TextView tv_yuzdelikProtein = (TextView) getView().findViewById(R.id.tv_yuzdelikBesinProtein);
        TextView tv_yuzdelikKarbonhidrat = (TextView) getView().findViewById(R.id.tv_yuzdelikBesinKarbonhidrat);
        TextView tv_yuzdelikYag = (TextView) getView().findViewById(R.id.tv_yuzdelikBesinYag);

        TextView tv_enerji_Birim = (TextView) getView().findViewById(R.id.tv_besinEnerjiAdt);
        TextView tv_protein_Birim = (TextView) getView().findViewById(R.id.tv_besinProteinAdt);
        TextView tv_karbonhidratBirim = (TextView) getView().findViewById(R.id.tv_besinKarbonhidratAdt);
        TextView tv_yagBirim = (TextView) getView().findViewById(R.id.tv_besinYagAdt);

        tv_yuzdelikEnerji.setText(s_Enerji);
        tv_yuzdelikProtein.setText(s_protein);
        tv_yuzdelikKarbonhidrat.setText(s_karbonhidrat);
        tv_yuzdelikYag.setText(s_yag);

        tv_enerji_Birim.setText(s_olculenEnerji);
        tv_protein_Birim.setText(s_olculenProtein);
        tv_karbonhidratBirim.setText(s_olculenKarbonhidrat);
        tv_yagBirim.setText(s_olculenYag);

        //porsiyon miktarı
        et_porsiyonAdedi.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if(!(s.toString().equals(""))){
                    porsiyonAdediDuzenleme();
                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}      //BU SABİT
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });
        et_porsiyonAdedi.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                }else {
                    String lock = "portionSizePcs";
                    releaseLock(lock);
                }
            }
        });

        et_porsiyonmiktari.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if(!(s.toString().equals(""))){
                    // My code here
                    et_porsiyonGramDegeri();
                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });
        et_porsiyonmiktari.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                }else {
                    String lock = "portionSizeGram";
                    releaseLock(lock);
                }
            }
        });



        // porsiyongramdegeri icin listener. eklemek için.

        Button buttonSubmit = (Button)getActivity().findViewById(R.id.eklebutonu);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ogunebesinEkle();
            }
        });

        veriler.close();

    } // tıklandiginda Kategorideki Besinleri Listele

    private void releaseLock(String lock){
        if(lock.equals("portionSizeGram")){
            porsiyonGram = false;
        }
        else {
            porsiyonAdet = false;
        }
    }

    //  porsiyon adedi duzenleme
    public void porsiyonAdediDuzenleme(){
        if(!(porsiyonGram)) {

            porsiyonAdet = true;

            // adet bilgisi al
            EditText et_porsiyonAdedi = (EditText) getActivity().findViewById(R.id.et_porsiyonAdediGir);
            String s_porsiyonAdedi = et_porsiyonAdedi.getText().toString();

            double d_porsiyonAdedi = 0;

            if (s_porsiyonAdedi.equals("")) {
                d_porsiyonAdedi = 0;
            } else {
                try {
                    d_porsiyonAdedi = Double.parseDouble(s_porsiyonAdedi);
                } catch (NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }
            }

            Verit db = new Verit(getActivity());
            db.open();

            String fields[] = new String[]{
                    "gida_servisGram"
            };
            String currentIdSQL = db.aktarma(tut_kategoriGida);
            Cursor foodCursor = db.sec("gidalar", fields, "_id", currentIdSQL);

            String stringServingSize = foodCursor.getString(0);
            db.close();

            double d_servisGram = 0;
            try {
                d_servisGram = Double.parseDouble(stringServingSize);
            } catch (NumberFormatException nfe) {
                System.out.println("Could not parse " + nfe);
            }

            // bir porsiyonun kaç kram oldugunu hesapla
            // gram bilgisini güncelle
            double d_porsiyonGram = Math.round(d_porsiyonAdedi * d_servisGram);


            EditText editTextPortionSizeGram = (EditText) getActivity().findViewById(R.id.et_porsiyonMiktariGr);
            editTextPortionSizeGram.setText("" + d_porsiyonGram);
        }
    } // porsiyongramı

    //  porsiyon Gram Degeri
    public void et_porsiyonGramDegeri(){
        if(!(porsiyonAdet)) {

            porsiyonGram = true;
            // gram bilgisine göre devam et
            EditText editTextPortionSizeGram = (EditText) getActivity().findViewById(R.id.et_porsiyonMiktariGr);
            String s_porsiyonGrami = editTextPortionSizeGram.getText().toString();
            double d_porsiyonGrami = 0;
            try {
                d_porsiyonGrami = Double.parseDouble(s_porsiyonGrami);
            } catch (NumberFormatException nfe) {
                System.out.println("Could not parse " + nfe);
            }

            Verit db = new Verit(getActivity());
            db.open();

            String fields[] = new String[]{
                    "gida_servisGram"
            };
            String idSQL = db.aktarma(tut_kategoriGida);
            Cursor cursorBesin = db.sec("gidalar", fields, "_id", idSQL);

            String s_servisGramii = cursorBesin.getString(0);
            db.close();

            double d_servisGramii = 0;
            try {
                d_servisGramii = Double.parseDouble(s_servisGramii);
            } catch (NumberFormatException nfe) {
                System.out.println("Hata " + nfe);
            }

              //adet hesapla
            double doublePortionSizePcs = Math.round(d_porsiyonGrami / d_servisGramii);

            EditText editTextPortionSizePcs = (EditText) getActivity().findViewById(R.id.et_porsiyonAdediGir);
            editTextPortionSizePcs.setText("" + doublePortionSizePcs);
        }

    } // porsiyon Gram Degeri

    //ogune eklemek için
    public void ogunebesinEkle(){
        int error = 0;

        Verit veriler = new Verit(getActivity());
        veriler.open();

        String fields[] = new String[] {
                "_id",
                "gida_isim",
                "gida_Marka",
                "gida_aciklama",
                "gida_servisGram",
                "gida_servisGramolcm",
                "gida_servisAdet",
                "gida_servisAdetolcm",
                "gida_enerji",
                "gida_protein",
                "gida_karbonhidrat",
                "gida_yag",
                "gida_olculenEnerji",
                "gida_olculenProtein",
                "gida_olculen_karbonhidrat",
                "gida_olculen_yag",
                "gida_kullaniciID",
                "gida_bar",
                "gida_kategori",
                "gida_fa",
                "gida_fb",
                "gida_fc"
        };
        String currentIdSQL = veriler.aktarma(tut_kategoriGida);
        Cursor cursorBesin = veriler.sec("gidalar", fields, "_id", currentIdSQL);

        String s_id = cursorBesin.getString(0);
        String s_Name = cursorBesin.getString(1);
        String s_ureticiIsmi = cursorBesin.getString(2);
        String s_aciklama = cursorBesin.getString(3);
        String s_servisGrami = cursorBesin.getString(4);
        String s_servisGramB = cursorBesin.getString(5);
        String s_servisAdedi = cursorBesin.getString(6);
        String s_servisAdetB = cursorBesin.getString(7);
        String s_enerji = cursorBesin.getString(8);
        String s_protein = cursorBesin.getString(9);
        String s_karbonhidrat = cursorBesin.getString(10);
        String s_yag = cursorBesin.getString(11);
        String s_hesaplananEnerji = cursorBesin.getString(12);
        String s_hesaplananProtein = cursorBesin.getString(13);
        String s_hesaplananKarbonhidrat = cursorBesin.getString(14);
        String s_hesaplananYag = cursorBesin.getString(15);
        String s_KullaniciID = cursorBesin.getString(16);
        String s_barkod = cursorBesin.getString(17);
        String s_kategori = cursorBesin.getString(18);
        String s_foto1 = cursorBesin.getString(19);
        String s_foto2 = cursorBesin.getString(20);
        String s_foto3 = cursorBesin.getString(21);


        // gram degeri al
        EditText et_porsiyonGramaji = (EditText)getActivity().findViewById(R.id.et_porsiyonMiktariGr);
        String ServisGramaji = et_porsiyonGramaji.getText().toString();
        String fdServingSizeGramSQL = veriler.aktarma(ServisGramaji);
        double d_porsiyonGramaji = 0;
        try{
            d_porsiyonGramaji = Double.parseDouble(ServisGramaji);
        }
        catch (NumberFormatException nfe){
            error = 1;
            Toast.makeText(getActivity(), "Lütfen gram degeri için bir sayi giriniz ..", Toast.LENGTH_SHORT).show();
        }
        if(ServisGramaji.equals("")){
            error = 1;
            Toast.makeText(getActivity(), "Gram degeri boş olamaz .. ", Toast.LENGTH_SHORT).show();
        }



        // yaş
        Calendar calendar = Calendar.getInstance();
        int yil = calendar.get(Calendar.YEAR);
        int ay = calendar.get(Calendar.MONTH);
        int gun = calendar.get(Calendar.DAY_OF_MONTH);

        ay = ay+1; //ay degeri 0 la başladığında
        String s_ay = "";
        if(ay < 10){
            s_ay = "0" + ay;
        }
        else{
            s_ay = "" + ay;
        }

        String s_gun = "";
        if(gun < 10){
            s_gun = "0" + gun;
        }
        else{
            s_gun = "" + gun;
        }


        String s_tarih = yil + "-" + s_ay + "-" + s_gun;
        String stringFdDateSQL = veriler.aktarma(s_tarih);

        // ogunu belirlemek
        String s_ogunNumarasi = tut_hangiOgun;
        String s_ogunNumarasiSQL = veriler.aktarma(s_ogunNumarasi);

        // besini belirlemek
        String s_besinID = tut_kategoriGida;
        String s_besinIDSQL = veriler.aktarma(s_besinID);

        // porsiyon boyutu
        String porsiyonGramSQL = veriler.aktarma(s_servisGramB);

        double porsiyonAdet = 0;
        try {
            porsiyonAdet = Double.parseDouble(s_servisGrami);
        } catch (NumberFormatException nfe) {
            System.out.println("Could not parse " + nfe);
        }
        double d_porsiyonunMiktarı = Math.round(d_porsiyonGramaji / porsiyonAdet);
        String s_porsiyonunMiktarı = "" + d_porsiyonunMiktarı;
        String s_porsiyonunMiktarıSQL = veriler.aktarma(s_porsiyonunMiktarı);

        // adetne kadar
        String stringFdServingSizePcsMesurmentSQL = veriler.aktarma(s_servisAdetB);
        double doubleEnergyPerHundred = Double.parseDouble(s_enerji);

        double doubleFdEnergyCalculated = Math.round((d_porsiyonGramaji*doubleEnergyPerHundred)/100);
        String stringFdEnergyCalcualted = "" + doubleFdEnergyCalculated;
        String stringFdEnergyCalcualtedSQL = veriler.aktarma(stringFdEnergyCalcualted);

        // proteini hesapla
        double d_proteinYuzdesi = Double.parseDouble(s_protein);

        double d_olculenProtein = Math.round((d_porsiyonGramaji*d_proteinYuzdesi)/100);
        String s_olculenProtein = "" + d_olculenProtein;
        String olculenProteinSQL = veriler.aktarma(s_olculenProtein);


        //karbonhidratı hesapla
        double d_karbonhidratYuzdesi = Double.parseDouble(s_karbonhidrat);

        double d_olculenkarbonhidrat = Math.round((d_porsiyonGramaji*d_karbonhidratYuzdesi)/100);
        String s_olculekarbonhidrat = "" + d_olculenkarbonhidrat;
        String olculenKarbonhidratSQL = veriler.aktarma(s_olculekarbonhidrat);

        // yagi hesapla
        double d_yagYuzdesi = Double.parseDouble(s_yag);

        double d_olculenYAg = Math.round((d_porsiyonGramaji*d_yagYuzdesi)/100);
        String s_olculenYag = "" + d_olculenYAg;
        String s_olculenyagSQL = veriler.aktarma(s_olculenYag);

        // sql e ekleme işlemi
        if(error == 0){
            String alan = "_id, gbesin_tarih, gbesin_ogun, gbesin_besinID," +
                    "gbesin_servisGram, gbesin_servisGramOlc," +
                    " gbesin_servisAadet, gbesin_servisAadetOlcmbr," +
                    " gbesin_enerji, gbesin_protein," +
                    " gbesin_karbonhidrat, gbesin_yag";

            String degerd = "NULL, " + stringFdDateSQL + ", " + s_ogunNumarasiSQL + ", " + s_besinIDSQL + ", " +
                    fdServingSizeGramSQL + ", " + porsiyonGramSQL + ", " +
                    s_porsiyonunMiktarıSQL + ", " + stringFdServingSizePcsMesurmentSQL + ", " +
                    stringFdEnergyCalcualtedSQL + ", " + olculenProteinSQL + ", " +
                    olculenKarbonhidratSQL + ", " + s_olculenyagSQL;

            veriler.insert("gunlukBesin", alan, degerd);

            Toast.makeText(getActivity(), "Öğün başariyla güncelledi", Toast.LENGTH_SHORT).show();


            Fragment fragment = null;
            Class fragmentClass = null;
            fragmentClass = Anasayfa.class;
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }

            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.doldur, fragment).commit();

        }
        // veritabanı işlemi bitti
        veriler.close();
    } //besin eklendi


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    // TODO: Rename method, guncelle argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mlListener != null) {
            mlListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mlListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mlListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
