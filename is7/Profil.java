package com.example.deeptrancer.diyetuygulamam496;

/**
 * Created by deeptrancer on 2.03.2018.
 */


        import android.content.Context;
        import android.database.Cursor;
        import android.net.Uri;
        import android.os.Bundle;
        import android.support.v4.app.Fragment;
        import android.view.LayoutInflater;
        import android.view.MenuInflater;
        import android.view.MenuItem;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.AdapterView;
        import android.widget.ArrayAdapter;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.RadioButton;
        import android.widget.RadioGroup;
        import android.widget.Spinner;
        import android.widget.TextView;
        import android.widget.Toast;
        import java.util.Calendar;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Profil.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Profil#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Profil extends Fragment {

    private View anahat;

    private MenuItem menuDuzenle;
    private MenuItem menuBsil;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    public Profil() {

    }

    public static Profil newInstance(String param1, String param2) {
        Profil fragment = new Profil();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((Menu)getActivity()).getSupportActionBar().setTitle("Profil");

        verial();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        anahat = inflater.inflate(R.layout.profil, container, false);
        return anahat;
    }

    private void setAnahat(int id){
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        anahat = inflater.inflate(id, null);
        ViewGroup rootView = (ViewGroup) getView();
        rootView.removeAllViews();
        rootView.addView(anahat);
    }

    public void onCreateOptionsMenu(android.view.Menu menu, MenuInflater inflater) {


        MenuInflater menuInflater = ((Menu)getActivity()).getMenuInflater();
        inflater.inflate(R.menu.menu_goal, menu);

        menuDuzenle = menu.findItem(R.id.besinDuzenle);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        int ino = menuItem.getItemId();



        return super.onOptionsItemSelected(menuItem);
    }

    public void verial(){


        Verit veriler = new Verit(getActivity());
        veriler.open();

        long long_ID = 1;
        String fields[] = new String[] {
                "_id",
                "kullanici_d",
                "kullanici_cinsiyet",
                "kullanici_boy",
                "kullanici_birim"
        };
        Cursor c = veriler.sec("kullanicilar", fields, "_id", long_ID);
        String kullaniciAdi = c.getString(1);
        String kullaniciCinsiyet  = c.getString(2);
        String kullaniciBoy = c.getString(3);
        String kulalnicibirim = c.getString(4);

        String[] tut = kullaniciAdi.split("-");
        String kullanici_yil  = tut[0];
        String kullanici_ay = tut[1];
        String kullancii_gun  = tut[2];

        int siraligunsecimi = 0;
        String[] gun = new String[31];
        int kisi_sayac = 0;
        for(int i=0;i<31;i++){
            kisi_sayac=i+1;
            gun[i] = "" + kisi_sayac;

            if(kullancii_gun.equals("0" + kisi_sayac) || kullancii_gun.equals(""+kisi_sayac)){
                siraligunsecimi = i;
            }

        }
        Spinner k_gun = (Spinner) getActivity().findViewById(R.id.sp_gun);
        ArrayAdapter<String> deneme = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, gun);
        k_gun.setAdapter(deneme);

        k_gun.setSelection(siraligunsecimi);

        int kullaniic_ay = 0;
        kullancii_gun.replace("0", "");
        try {
            kullaniic_ay = Integer.parseInt(kullanici_ay);
        }
        catch(NumberFormatException nfe) {
            System.out.println("Could not parse " + nfe);
        }
        kullaniic_ay = kullaniic_ay-1;
        Spinner sp_kullanici_ay = (Spinner) getActivity().findViewById(R.id.sp_ay);
        sp_kullanici_ay.setSelection(kullaniic_ay);

        int spi_yil = 0;

        String[] kul_yil = new String[100];
        Calendar clndr = Calendar.getInstance();
        int yil = clndr.get(Calendar.YEAR);
        int bit = yil-100;
        int nerede = 0;
        for(int i=yil;i>bit;i--){
            kul_yil[nerede] = "" + i;

            if(kullanici_yil.equals(""+i)){
                spi_yil = nerede;
            }
            nerede++;
        }
        Spinner spkullanici_yil = (Spinner)getActivity().findViewById(R.id.sp_yil);
        ArrayAdapter<String> yiladapt = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, kul_yil);
        spkullanici_yil.setAdapter(yiladapt);
        spkullanici_yil.setSelection(spi_yil); // Select index

        RadioButton butonBayan = (RadioButton)getActivity().findViewById(R.id.rbutonbayan);
        RadioButton butonBay = (RadioButton)getActivity().findViewById(R.id.rbuton_bay);
        if(kullaniciCinsiyet.startsWith("bay ")){                                             //burayadikkattt
            butonBay.setChecked(true);
            butonBayan.setChecked(false);
        }
        else{
            butonBay.setChecked(false);
            butonBayan.setChecked(true);
        }


        EditText kisi_uzunluk = (EditText)getActivity().findViewById(R.id.et_uzunlukCm);
        EditText kisi_uzunluk_inc = (EditText)getActivity().findViewById(R.id.et_uzunlukInc);
        TextView tv_profilDuzenlecm = (TextView)getActivity().findViewById(R.id.tv_ProfilCm);
        if(kulalnicibirim.startsWith("m")) {
            kisi_uzunluk_inc.setVisibility(View.GONE);
            kisi_uzunluk.setText(kullaniciBoy);
        }
        else{
            tv_profilDuzenlecm.setText("uzunluk");                                             //burayaDikkat
            double uzunlukCM = 0;
            double uzunlukF = 0;
            double heightInches = 0;

            try {
                uzunlukCM = Double.parseDouble(kullaniciBoy);
            }
            catch(NumberFormatException nfe) {

            }
            if(uzunlukCM != 0){
                uzunlukF = (uzunlukCM * 0.3937008)/12;
                int boyft = (int) uzunlukF;

                kisi_uzunluk.setText("" + boyft);

            }

        }


        Spinner profilbirim = (Spinner)getActivity().findViewById(R.id.sp_olcum);
        if(kulalnicibirim.startsWith("m")) {
            profilbirim.setSelection(0);

        }
        else{
            profilbirim.setSelection(1);
        }

        profilbirim.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                olcudegisimi();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }
        });


        Button profilDuzenlebutonu = (Button)getActivity().findViewById(R.id.profilDuzenleButon);
        profilDuzenlebutonu.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                profilDuzenleDegistir();
            }
        });


        veriler.close();

    }


    public void olcudegisimi() {

        Spinner olcum = (Spinner)getActivity().findViewById(R.id.sp_olcum);
        String s_olcum = olcum.getSelectedItem().toString();


        EditText et_uzunlukcm = (EditText)getActivity().findViewById(R.id.et_uzunlukCm);
        EditText et_uzunlukinc = (EditText)getActivity().findViewById(R.id.et_uzunlukInc);

        TextView tv_profilDuzenleme = (TextView)getActivity().findViewById(R.id.tv_ProfilCm);



        if(s_olcum.startsWith("M")) {

            et_uzunlukinc.setVisibility(View.GONE);
            tv_profilDuzenleme.setText("cm");
        }
        else{

            et_uzunlukinc.setVisibility(View.VISIBLE);
            tv_profilDuzenleme.setText("inc");

        }


    }

    private void profilDuzenleDegistir(){

        Verit veriler = new Verit(getActivity());
        veriler.open();


        int hata = 0;


        Spinner kul_gun = (Spinner)getActivity().findViewById(R.id.sp_gun);
        String s_gun = kul_gun.getSelectedItem().toString();
        int i_gun = 0;
        try {
            i_gun = Integer.parseInt(s_gun);

            if(i_gun < 10){
                s_gun = "0" + s_gun;
            }

        }
        catch(NumberFormatException nfe) {
            System.out.println("Hata ! " + nfe);
            hata = 1;
            Toast.makeText(getActivity(), "Doğum gününüz için bir gün giriniz", Toast.LENGTH_SHORT).show();
        }


        Spinner s_ay = (Spinner)getActivity().findViewById(R.id.sp_ay);
        String string_ay = s_ay.getSelectedItem().toString();
        int i_ay = s_ay.getSelectedItemPosition();
        int intAy = i_ay+1;
        if(intAy < 10){
            string_ay = "0" + intAy;
        }
        else{
            string_ay = "" + intAy;
        }

        Spinner sp_yil = (Spinner)getActivity().findViewById(R.id.sp_yil);
        String s_yil = sp_yil.getSelectedItem().toString();
        int i_year = 0;
        try {
            i_year = Integer.parseInt(s_yil);
        }
        catch(NumberFormatException nfe) {
            System.out.println("Hata " + nfe);
            hata = 1;
            Toast.makeText(getActivity(), "Doğum tarihiniz için bir yıl giriniz", Toast.LENGTH_SHORT).show();
        }

        String dogumtarihi = i_year + "-" + string_ay + "-" + s_gun;
        String dogumtarihiSQL = veriler.aktarma(dogumtarihi);

        RadioGroup kullanici_cinsiyet = (RadioGroup)getActivity().findViewById(R.id.rg_cinsiyet);
        int butonid = kullanici_cinsiyet.getCheckedRadioButtonId();
        View rd_cinsiyet = kullanici_cinsiyet.findViewById(butonid);
        int durum = kullanici_cinsiyet.indexOfChild(rd_cinsiyet);

        String cinsiyet = "";
        if(durum == 0){
            cinsiyet = "bay";
        }
        else{
            cinsiyet = "bayan";
        }
        String cinsiyetSQL = veriler.aktarma(cinsiyet);

        EditText et_uzunlukcm = (EditText)getActivity().findViewById(R.id.et_uzunlukCm);
        EditText et_uzunlukinc = (EditText)getActivity().findViewById(R.id.et_uzunlukInc);
        String s_uzunlukCm = et_uzunlukcm.getText().toString();
        String s_uzunlukinc = et_uzunlukinc.getText().toString();

        double uzunlukCm = 0;
        double uzunlukF = 0;
        double uzunlukinc = 0;
        boolean metre = true;


        Spinner s_olcum = (Spinner)getActivity().findViewById(R.id.sp_olcum);
        String string_olcum = s_olcum.getSelectedItem().toString();

        int i_olcum = s_olcum.getSelectedItemPosition();
        if(i_olcum == 0){
            string_olcum = "metre";
        }
        else{
            string_olcum = "inc";
            metre = false;
        }
        String olcumSQL = veriler.aktarma(string_olcum);

        if(metre == true) {

            try {
                uzunlukCm = Double.parseDouble(s_uzunlukCm);
                uzunlukCm = Math.round(uzunlukCm);
            }
            catch(NumberFormatException nfe) {
                hata = 1;
                Toast.makeText(getActivity(), "uzunluk bir sayi girdisi olmalı. tekrar deneyin", Toast.LENGTH_SHORT).show();
            }
        }
        else {


            try {
                uzunlukF = Double.parseDouble(s_uzunlukCm);
            }
            catch(NumberFormatException nfe) {
                hata = 1;
                Toast.makeText(getActivity(), "uzunluk bir sayi girdisi olmalı. tekrar deneyin", Toast.LENGTH_SHORT).show();
            }

            try {
                uzunlukinc = Double.parseDouble(s_uzunlukinc);
            }
            catch(NumberFormatException nfe) {
                hata = 1;
                Toast.makeText(getActivity(), "uzunluk bir sayi girdisi olmalı. tekrar deneyin", Toast.LENGTH_SHORT).show();
            }

            uzunlukCm = ((uzunlukF * 12) + uzunlukinc) * 2.54;
            uzunlukCm = Math.round(uzunlukCm);
        }
        s_uzunlukCm = "" + uzunlukCm;
        String uzunlukSQL = veriler.aktarma(s_uzunlukCm);



        if(hata == 0){

            long id = 1;

            String fields[] = new String[] {
                    "kullanici_d",
                    "kullanici_cinsiyet",
                    "kullanici_boy",
                    "kullanici_birim"
            };
            String values[] = new String[] {
                    dogumtarihiSQL,
                    cinsiyetSQL,
                    uzunlukSQL,
                    olcumSQL
            };

            veriler.guncelle("kullanicilar", "_id", id, fields, values);

            Toast.makeText(getActivity(), "Degisiklikler basarıyla kaydedildi", Toast.LENGTH_SHORT).show();

        }


        veriler.close();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    // TODO: Rename method, guncelle argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " ");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
