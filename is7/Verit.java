package com.example.deeptrancer.diyetuygulamam496;

/**
 * Created by deeptrancer on 2.03.2018.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;


public class Verit {

    private static final String databaseName = "DiyetProgramim";
    private static final int databaseVersion = 57;


    private final Context context;
    private DatabaseHelper DBHelper;
    private SQLiteDatabase veriler;

    public Verit(Context ctx){
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context){
            super(context, databaseName, null, databaseVersion);
        }

        @Override
        public void onCreate(SQLiteDatabase db){
            try{
                // tablo olusturuyorum
                db.execSQL("CREATE TABLE IF NOT EXISTS hedef (" +
                        " _id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        " hedef_id INT, "+
                        " hedef_guncel_kilo INT, "+
                        " hedef_kilo INT, "+
                        " hedef_almakvermek VARCHAR, "+
                        " hedef_haftalik_tempo VARCHAR, "+
                        " hedef_tarih DATE, "+
                        " goal_activity_level INT, " +
                        " hedef_enerji INT, "+
                        " hedef_protein INT, "+
                        " hedef_karbonhidrat INT, "+
                        " hedef_yag INT, "+
                        " diyet_enerji INT, "+
                        " diyet_protein INT, "+
                        " diyet_kalori INT, "+
                        " diyet_yag INT, "+
                        " aktivite_enerji INT, "+
                        " aktivite_protein INT, "+
                        " aktivite_karbonhidrat INT, "+
                        " aktivite_yağ INT, "+
                        " diyetAktivite_enerji INT, "+
                        " diyetAktivite_protein INT, "+
                        " diyetAktivite_karbonhidrat INT, "+
                        " diyetAktivite_yag INT, "+
                        " aciklama VARCHAR);");
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
            try{

                db.execSQL("CREATE TABLE IF NOT EXISTS kullanicilar (" +
                        " _id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        " kullanici_id INTEGER, " +
                        " kullanici_isim VARCHAR," +
                        " kullanici_sifre VARCHAR, " +
                        " kullanici_t VARCHAR, " +
                        " kullanici_rmz VARCHAR," +
                        " kullanici_d DATE, " +
                        " kullanici_cinsiyet INT, " +
                        " kullanici_yeri VARHCAR, " +
                        " kullanici_boy INT, " +
                        " kullanici_birim VARHCAR, " +
                        " kullanici_sonakt TIME," +
                        " kullanici_aciklama VARCHAR);");

            }
            catch (SQLException e) {
                e.printStackTrace();
            }

            try{
                db.execSQL("CREATE TABLE IF NOT EXISTS besinlerdenAlinanKalori (" +
                        " _id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        " besin_alinan_cal_ID INTEGER, " +
                        " besin_alinan_cal_tarih DATE, " +
                        " besin_alinan_cal_ogunNo INT, " +
                        " besin_alinan_enerji INT, " +
                        " besin_alinan_protein INT, " +
                        " besin_alinan_karbon INT, " +
                        " besin_alinan_yag INT);");
            }
            catch (SQLException e) {
                e.printStackTrace();
            }

            try{
                db.execSQL("CREATE TABLE IF NOT EXISTS besin_gunlukToplam (" +
                        " _id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        " besin_gunlukToplam_id INTEGER, " +
                        " besin_gunlukToplam_tarih DATE, " +
                        " besin_gunlukToplam_enerji INT, " +
                        " besin_gunlukToplam_protein INT, " +
                        " besin_gunlukToplam_karbonhidrat INT, " +
                        " besin_gunlukToplam_yag INT);");
            }
            catch (SQLException e) {
                e.printStackTrace();
            }



            try{
                db.execSQL("CREATE TABLE IF NOT EXISTS gunlukBesin (" +
                        " _id INTEGER PRIMARY KEY AUTOINCREMENT," +
                        " gbesin_ID INTEGER," +
                        " gbesin_tarih DATE," +
                        " gbesin_ogun INT," +
                        " gbesin_besinID INT," +
                        " gbesin_servisGram DOUBLE," +
                        " gbesin_servisGramOlc VARCHAR," +
                        " gbesin_servisAadet DOUBLE," +
                        " gbesin_servisAadetOlcmbr VARCHAR," +
                        " gbesin_enerji DOUBLE," +
                        " gbesin_protein DOUBLE," +
                        " gbesin_karbonhidrat DOUBLE," +
                        " gbesin_yag DOUBLE" +
                        " gbesin_ogunID INT);");

            }
            catch (SQLException e) {
                e.printStackTrace();
            }
            try{
                db.execSQL("CREATE TABLE IF NOT EXISTS kategoriler (" +
                        " _id INTEGER PRIMARY KEY AUTOINCREMENT," +
                        " kategori_id INTEGER," +
                        " kategori_isim VARCHAR," +
                        " kategori_pid INT," +
                        " kategori_ikn VARCHAR," +
                        " kategori_aciklama VARCHAR);");

            }
            catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                db.execSQL("CREATE TABLE IF NOT EXISTS gidalar (" +
                        " _id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        " gida_id INTEGER, " +
                        " gida_isim VARCHAR," +
                        " gida_Marka VARCHAR," +
                        " gida_s VARCHAR," +
                        " gida_aciklama VARCHAR," +
                        " gida_servisGram DOUBLE," +
                        " gida_servisGramolcm VARCHAR," +
                        " gida_servisAdet DOUBLE," +
                        " gida_servisAdetolcm VARCHAR," +
                        " gida_enerji DOUBLE," +
                        " gida_protein DOUBLE," +
                        " gida_karbonhidrat DOUBLE," +
                        " gida_yag DOUBLE," +
                        " gida_olculenEnerji DOUBLE," +
                        " gida_olculenProtein DOUBLE," +
                        " gida_olculen_karbonhidrat DOUBLE," +
                        " gida_olculen_yag DOUBLE," +
                        " gida_kullaniciID INT," +
                        " gida_bar VARCHAR," +
                        " gida_kategori INT," +
                        " gida_tmb VARCHAR," +
                        " gida_fa VARCHAR," +
                        " gida_fb VARCHAR," +
                        " gida_fc VARCHAR," +
                        " gida_skt DATE," +
                        " gida_mnsi VARCHAR," +
                        " gida_not VARCHAR);");



            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            db.execSQL("DROP TABLE IF EXISTS hedef");
            db.execSQL("DROP TABLE IF EXISTS kullanicilar");
            db.execSQL("DROP TABLE IF EXISTS besinlerdenAlinanKalori");
            db.execSQL("DROP TABLE IF EXISTS besin_gunlukToplam");
            db.execSQL("DROP TABLE IF EXISTS gunlukBesin");
            db.execSQL("DROP TABLE IF EXISTS kategoriler");
            db.execSQL("DROP TABLE IF EXISTS gidalar");
            onCreate(db);

            String TAG = "Tag";
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + "");

        }
    }


    public Verit open() throws SQLException {
        veriler = DBHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        DBHelper.close();
    }


    public String aktarma(String deger){
        //deegerin numerik olup olmadigi
        boolean sayiysa = false;
        try {
            double ondalikDeger = Double.parseDouble(deger);
            sayiysa = true;
        }
        catch(NumberFormatException nfe) {
            System.out.println("Hata " + nfe);
        }
        if(sayiysa == false){
            // özel karakterlerin yazılmaması için
            if (deger != null && deger.length() > 0) {
                deger = deger.replace("\\", "\\\\");
                deger = deger.replace("'", "\\'");
                deger = deger.replace("\0", "\\0");
                deger = deger.replace("\n", "\\n");
                deger = deger.replace("\r", "\\r");
                deger = deger.replace("\"", "\\\"");
                deger = deger.replace("\\x1a", "\\Z");
            }
        }

        deger = "'" + deger + "'";

        return deger;
    }
    public double aktarma(double value) { return value; }
    public int aktarma(int value) { return value; }
    public long aktarma(long value) { return value; }

    //veri aktarma
    public void insert(String tablo, String alan, String deger){

        try {
            veriler.execSQL("INSERT INTO " + tablo +  "(" + alan + ") VALUES (" + deger + ")");
        }
        catch(SQLiteException e){
            System.out.println("Hata : " + e.toString());
            Toast.makeText(context, "Hata! " +  e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    public int sayim(String tablo)
    {
        try {
            Cursor saym = veriler.rawQuery("SELECT COUNT(*) FROM " + tablo + "", null);
            saym.moveToFirst();
            int say = saym.getInt(0);
            saym.close();
            return say;
        }
        catch(SQLiteException e){
            return -1;
        }

    }

    public Cursor sec(String tablo, String[] alan) throws SQLException
    {
        Cursor c = veriler.query(tablo, alan, null, null, null, null, null, null);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    public Cursor sec(String tablo, String[] alan, String bulundugu, String bag) throws SQLException
    {
        Cursor cr = veriler.query(tablo, alan, bulundugu + "=" + bag, null, null, null, null, null);
        if (cr != null) {
            cr.moveToFirst();
        }
        return cr;
    }

    // stringlerial
    public Cursor sec(String tablo, String[] alan, String[] nere, String[] neresart, String[] yerdurum) throws SQLException
    {

        String nerede = "";
        int boyuttut = nere.length;
        for(int i=0;i<boyuttut;i++) {
            if(nerede.equals("")) {
                nerede = nere[i] + "=" + neresart[i];
            }
            else{
                nerede = nerede + " " + yerdurum[i-1] + " " + nere[i] + "=" + neresart[i];
            }
        }

        Cursor crs = veriler.query(tablo, alan, nerede, null, null, null, null, null);
        if (crs != null) {
            crs.moveToFirst();
        }
        return crs;
    }

    public Cursor sec(String tablo, String[] alan, String bulundugu, long bag) throws SQLException {
        Cursor crs = veriler.query(tablo, alan, bulundugu + "=" + bag, null, null, null, null, null);
        if (crs != null) {
            crs.moveToFirst();
        }
        return crs;
    }

    public Cursor sec(String tablo, String[] alan, String bulundugu, String bag, String orderBy, String OrderMethod) throws SQLException
    {
        Cursor csr = null;
        if(bulundugu.equals("")) {
            csr = veriler.query(tablo, alan, null, null, null, null, orderBy + " " + OrderMethod, null);
        }
        else {
            csr = veriler.query(tablo, alan, bulundugu + "=" + bag, null, null, null, orderBy + " " + OrderMethod, null);
        }
        if (csr != null) {
            csr.moveToFirst();
        }
        return csr;
    }

    public boolean guncelle(String tablo, String ky, long long_rw, String alan, String deger) throws SQLException {


        deger = deger.substring(1, deger.length()-1);

        ContentValues ag = new ContentValues();
        ag.put(alan, deger);
        return veriler.update(tablo, ag, ky + "=" + long_rw, null) > 0;
    }
    public boolean guncelle(String tablo, String ky, long long_rw, String field, double deger) throws SQLException {
        ContentValues args = new ContentValues();
        args.put(field, deger);
        return veriler.update(tablo, args, ky + "=" + long_rw, null) > 0;
    }
    public boolean guncelle(String tablo, String ky, long long_rw, String yer, int value) throws SQLException {
        ContentValues ag = new ContentValues();
        ag.put(yer, value);
        return veriler.update(tablo, ag, ky + "=" + long_rw, null) > 0;
    }
    public boolean guncelle(String tablo, String ky, long long_rw, String alan[], String deger[]) throws SQLException {


        ContentValues args = new ContentValues();
        int uzunlukTut = alan.length;
        for(int i=0;i<uzunlukTut;i++){
            // ilk ve sonu sil
            deger[i] = deger[i].substring(1, deger[i].length()-1);

            // koy
            args.put(alan[i], deger[i]);


        }

        return veriler.update(tablo, args, ky + "=" + long_rw, null) > 0;
    }

    public int sil(String tablo, String primaryKey, long rowID) throws SQLException {
        return veriler.delete(tablo, primaryKey + "=" + rowID, null);
    }
}
