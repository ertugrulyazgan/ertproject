package com.example.deeptrancer.diyetuygulamam496;

/**
 * Created by deeptrancer on 2.03.2018.
 */



        import android.content.Context;
        import android.database.Cursor;
        import android.support.v4.widget.CursorAdapter;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.TextView;

public class Gida extends CursorAdapter {
    public Gida(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.gidalistele, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView tv_isimListele = (TextView) view.findViewById(R.id.tvIsimList);
        TextView tv_listeNo = (TextView) view.findViewById(R.id.tv_NoList);
        TextView tv_listSubname = (TextView) view.findViewById(R.id.tv_subIsim);
        
        int Idal = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
        String getName = cursor.getString(cursor.getColumnIndexOrThrow("gida_isim"));

        String getUreticiIsmi = cursor.getString(cursor.getColumnIndexOrThrow("gida_Marka"));     //********************************
        String getAciklama = cursor.getString(cursor.getColumnIndexOrThrow("gida_aciklama"));
        String getPorBoyut = cursor.getString(cursor.getColumnIndexOrThrow("gida_servisGram"));
        String get_porOlcBrim = cursor.getString(cursor.getColumnIndexOrThrow("gida_servisGramolcm"));
        String gerPorAdet = cursor.getString(cursor.getColumnIndexOrThrow("gida_servisAdet"));
        String getPorAdetBrm = cursor.getString(cursor.getColumnIndexOrThrow("gida_servisAdetolcm"));
        int getHesaplananEnerji = cursor.getInt(cursor.getColumnIndexOrThrow("gida_olculenEnerji"));

        String subLine = getUreticiIsmi + ", " +
                getPorBoyut + " " +
                get_porOlcBrim + ", " +
                gerPorAdet + " " +
                getPorAdetBrm;

        tv_isimListele.setText(getName);
        tv_listeNo.setText(String.valueOf(getHesaplananEnerji));
        tv_listSubname.setText(subLine);

    }
}