package com.example.deeptrancer.diyetuygulamam496;
/**
 * Created by deeptrancer on 2.03.2018.
 */

import android.content.Intent;
import android.database.Cursor;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class KayitHedef extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up_goal);

        // hedef ve plan belirlendikten sonra devam.
        Button devametbutonu = (Button)findViewById(R.id.devametbutonu);
        devametbutonu.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                kayitHedefYolla();
            }
        });
        hatadurumu();
        hangibirimkullanildi();
    }

    public void kayitHedefYolla(){

        Verit veriler = new Verit(this); // veritabanını açıyoruz.
        veriler.open();

        // hata
        ImageView hataicindeneme = (ImageView)findViewById(R.id.hataicindeneme);
        TextView text_hata = (TextView)findViewById(R.id.text_hata);
        String hatamesaji = "";


        //hedef kilo
        EditText hedefkilogir = (EditText)findViewById(R.id.hedefkilogir);
        String s_hedefKilo = hedefkilogir.getText().toString();
        double d_hedefkilo = 0;
        try{
            d_hedefkilo = Double.parseDouble(s_hedefKilo);
        }
        catch(NumberFormatException nfe) {
            hatamesaji = "Hedef kilo bir sayi olmalıdır.";
        }

        // kilo almak mı istiyor vermek mi istiyor. vermek için 0 almak için 1 değeri

        Spinner s_almakveyavermek = (Spinner)findViewById(R.id.s_almakveyavermek);
        int almakveyavermek = s_almakveyavermek.getSelectedItemPosition();

        // haftalik hangi oranda degisiklik
        Spinner s_haftalıkdegisim = (Spinner)findViewById(R.id.s_haftalıkdegisim);
        String stringWeeklyGoal = s_haftalıkdegisim.getSelectedItem().toString();

        // guncelleyelim
        if(hatamesaji.isEmpty()){

            long goalID = 1;

            double d_hedefKiloSQL = veriler.aktarma(d_hedefkilo);
            veriler.guncelle("hedef", "_id", goalID, "hedef_kilo", d_hedefKiloSQL);

            int i_almakvermekSQL = veriler.aktarma(almakveyavermek);
            veriler.guncelle("hedef", "_id", goalID, "hedef_almakvermek", i_almakvermekSQL);

            String s_haftalıkTempoSQL = veriler.aktarma(stringWeeklyGoal);
            veriler.guncelle("hedef", "_id", goalID, "hedef_haftalik_tempo", s_haftalıkTempoSQL);

        }

        //enerji hesaplama
        if(hatamesaji.isEmpty()){

            // kullanicidan bir satir degeri alip dene
            long rowID = 1;
            String fields[] = new String[] {
                    "_id",
                    "kullanici_d",
                    "kullanici_cinsiyet",
                    "kullanici_boy"
            };
            Cursor c = veriler.sec("kullanicilar", fields, "_id", rowID);
            String stringUserDob = c.getString(1);
            String stringUserGender  = c.getString(2);
            String stringUserHeight = c.getString(3);

            // suanki kilo ve hedef kilosu nu kullan
            rowID = 1;
            String fieldsGoal[] = new String[] {
                    "_id",
                    "hedef_guncel_kilo",
                    "goal_activity_level"
            };
            Cursor cGoal = veriler.sec("hedef", fieldsGoal, "_id", rowID);
            String s_kullaniciguncelkilosu = cGoal.getString(1);
            String s_kullanicitemposu = cGoal.getString(2);

            // guncel kilo
            double d_guncelKilo = 0;
            try{
                d_guncelKilo = Double.parseDouble(s_kullaniciguncelkilosu);
            }
            catch(NumberFormatException nfe) {
                System.out.println("Could not parse " + nfe);
            }


            // yaş değerini al
            String[] items1 = stringUserDob.split("-");
            String s_yil = items1[0];
            String s_ay = items1[1];
            String s_gun = items1[2];

            int i_yilicin = 0;
            try {
                i_yilicin = Integer.parseInt(s_yil);
            }
            catch(NumberFormatException nfe) {
                System.out.println("Could not parse " + nfe);
            }
            int i_ayicin = 0;
            try {
                i_ayicin = Integer.parseInt(s_ay);
            }
            catch(NumberFormatException nfe) {
                System.out.println("Could not parse " + nfe);
            }
            int i_gunicin = 0;
            try {
                i_gunicin = Integer.parseInt(s_gun);
            }
            catch(NumberFormatException nfe) {
                System.out.println("Could not parse " + nfe);
            }
            String stringUserAge = yasAl(i_yilicin, i_ayicin, i_gunicin);

            int i_kullaniciYasi = 0;
            try {
                i_kullaniciYasi = Integer.parseInt(stringUserAge);
            }
            catch(NumberFormatException nfe) {
                System.out.println("Could not parse " + nfe);
            }

            // boyu
            double d_kullaniciBoyu = 0;
            try {
                d_kullaniciBoyu = Double.parseDouble(stringUserHeight);
            }
            catch(NumberFormatException nfe) {
                System.out.println("hata ! " + nfe);
            }

            long goalID = 1;


            // hesaplama ( bir yerde hata var düzelt) // Halloldu
            double hedefEnerjtut = 0;
            if(stringUserGender.startsWith("m")){
                hedefEnerjtut = 66.5+(13.75*d_guncelKilo)+(5.003*d_kullaniciBoyu)-(6.755*i_kullaniciYasi);
            }
            else{
                hedefEnerjtut = 655+(9.563*d_guncelKilo)+(1.850*d_kullaniciBoyu)-(4.676*i_kullaniciYasi);
            }
            hedefEnerjtut = Math.round(hedefEnerjtut);
            double hedefEnerjiSQL = veriler.aktarma(hedefEnerjtut);
            veriler.guncelle("hedef", "_id", goalID, "hedef_enerji", hedefEnerjiSQL);

            double proteintut = Math.round(hedefEnerjtut*25/100);
            double karbonhidrattut = Math.round(hedefEnerjtut*50/100);
            double yagtut = Math.round(hedefEnerjtut*25/100);

            double proteintutSQL = veriler.aktarma(proteintut);
            double karbonhidrattutSQL = veriler.aktarma(karbonhidrattut);
            double yagtutSQL = veriler.aktarma(yagtut);
            veriler.guncelle("hedef", "_id", goalID, "hedef_protein", proteintutSQL);
            veriler.guncelle("hedef", "_id", goalID, "hedef_karbonhidrat", karbonhidrattutSQL);
            veriler.guncelle("hedef", "_id", goalID, "hedef_yag", yagtutSQL);


            // hareket etmeden aktivite olmadan kilo vermek istiyorsa hedefi ne olacak ?? yarn burdan devam.
            double d_haftalikhedefineolacak = 0;
            try {
                d_haftalikhedefineolacak = Double.parseDouble(stringWeeklyGoal);
            }
            catch(NumberFormatException nfe) {
                System.out.println("hata ! " + nfe);
            }

            // 1 kilo yag yaklasik 7000 - 8000 kcal imiş
            double kcal = 0;
            double diyeticinEnerji = 0;

            kcal = 7700*d_haftalikhedefineolacak;
            if(almakveyavermek == 0){
                // kilo verme
                diyeticinEnerji = Math.round((hedefEnerjtut - (kcal/7)) * 1.2);

            }
            else{
                // kilo alma
                diyeticinEnerji = Math.round((hedefEnerjtut + (kcal/7)) * 1.2);
            }

            // verileri guncelle
            double diyeticinEnerjiSQL = veriler.aktarma(diyeticinEnerji);
            veriler.guncelle("hedef", "_id", goalID, "diyet_enerji", diyeticinEnerjiSQL);

            // protein karbonhidrat ve yag en önemli 3 unsur. yüzde 20-25 civarı protein, 45-50 civarı karbonhidrat 25-35 civarı yağ etkiliymiş.

            double diyetProteinSQL = Math.round(diyeticinEnerji*25/100);
            double diyetKarbonhidratSQL = Math.round(diyeticinEnerji*50/100);
            double diyetYagSQL = Math.round(diyeticinEnerji*25/100);

            double proteinSQL = veriler.aktarma(diyetProteinSQL);
            double karbonhidratSQL = veriler.aktarma(diyetKarbonhidratSQL);
            double yagSQL = veriler.aktarma(diyetYagSQL);
            veriler.guncelle("hedef", "_id", goalID, "diyet_protein", proteinSQL);
            veriler.guncelle("hedef", "_id", goalID, "diyet_kalori", karbonhidratSQL);
            veriler.guncelle("hedef", "_id", goalID, "diyet_yag", yagSQL);


            //isin icine aktivite girince
            double hareketliProgramdaEnerji = 0;
            if(s_kullanicitemposu.equals("0")) {
                hareketliProgramdaEnerji = hedefEnerjtut * 1.2;
            }
            else if(s_kullanicitemposu.equals("1")) {
                hareketliProgramdaEnerji = hedefEnerjtut * 1.375;
            }
            else if(s_kullanicitemposu.equals("2")) {
                hareketliProgramdaEnerji = hedefEnerjtut*1.55;
            }
            else if(s_kullanicitemposu.equals("3")) {
                hareketliProgramdaEnerji = hedefEnerjtut*1.725;
            }
            else if(s_kullanicitemposu.equals("4")) {
                hareketliProgramdaEnerji = hedefEnerjtut * 1.9;
            }
            hareketliProgramdaEnerji = Math.round(hareketliProgramdaEnerji);
            double hareketliprogramdaEnerjiSQL = veriler.aktarma(hareketliProgramdaEnerji);
            veriler.guncelle("hedef", "_id", goalID, "aktivite_enerji", hareketliprogramdaEnerjiSQL);

            double hareketliProgramdaProteinEnerjiSQL = Math.round(hareketliProgramdaEnerji*25/100);
            double hareketliProgramdaKarbonhidratEnerjiSQL = Math.round(hareketliProgramdaEnerji*50/100);
            double hareketliPtogramdaYagEnerjiSQL = Math.round(hareketliProgramdaEnerji*25/100);

            double hareketliProteinSQL = veriler.aktarma(hareketliProgramdaProteinEnerjiSQL);
            double hareketliKarbonhdratSQL = veriler.aktarma(hareketliProgramdaKarbonhidratEnerjiSQL);
            double hareketliYagSQL = veriler.aktarma(hareketliPtogramdaYagEnerjiSQL);
            veriler.guncelle("hedef", "_id", goalID, "aktivite_protein", hareketliProteinSQL);
            veriler.guncelle("hedef", "_id", goalID, "aktivite_karbonhidrat", hareketliKarbonhdratSQL);
            veriler.guncelle("hedef", "_id", goalID, "aktivite_yağ", hareketliYagSQL);


            // kilo vermek isteyen kullanici icin hakeketli diyette durum ( 1 kilo yag 7500 ile 8000 arası bir kcal degere sahip)
            kcal = 0;
            double harekettevediyetteEnerjiTut = 0;
            kcal = 7700*d_haftalikhedefineolacak;
            if(almakveyavermek == 0){
                // kilo verme durumu
                harekettevediyetteEnerjiTut = hedefEnerjtut - (kcal/7);

            }
            else{
                // alma durumu
                harekettevediyetteEnerjiTut = hedefEnerjtut + (kcal/7);
            }

            if(s_kullanicitemposu.equals("0")) {
                harekettevediyetteEnerjiTut= harekettevediyetteEnerjiTut* 1.2;
            }
            else if(s_kullanicitemposu.equals("1")) {
                harekettevediyetteEnerjiTut= harekettevediyetteEnerjiTut* 1.375; // az hareket
            }
            else if(s_kullanicitemposu.equals("2")) {
                harekettevediyetteEnerjiTut= harekettevediyetteEnerjiTut*1.55; // orta harekete
            }
            else if(s_kullanicitemposu.equals("3")) {
                harekettevediyetteEnerjiTut= harekettevediyetteEnerjiTut*1.725; // orta ustu
            }
            else if(s_kullanicitemposu.equals("4")) {
                harekettevediyetteEnerjiTut = harekettevediyetteEnerjiTut* 1.9; // cok hareketli
            }
            harekettevediyetteEnerjiTut = Math.round(harekettevediyetteEnerjiTut);

            // guncelle ve enerjiyi hesapla
            double energyWithActivityAndDietSQL = veriler.aktarma(harekettevediyetteEnerjiTut);
            veriler.guncelle("hedef", "_id", goalID, "diyetAktivite_enerji", energyWithActivityAndDietSQL);

            double protein = Math.round(harekettevediyetteEnerjiTut*25/100);
            double karbonhidrat = Math.round(harekettevediyetteEnerjiTut*50/100);
            double yag = Math.round(harekettevediyetteEnerjiTut*25/100);

            double protein_SQL = veriler.aktarma(protein);
            double karbonhidrat_SQL = veriler.aktarma(karbonhidrat);
            double yag_SQL = veriler.aktarma(yag);
            veriler.guncelle("hedef", "_id", goalID, "diyetAktivite_protein", protein_SQL);
            veriler.guncelle("hedef", "_id", goalID, "diyetAktivite_karbonhidrat", karbonhidrat_SQL);
            veriler.guncelle("hedef", "_id", goalID, "diyetAktivite_yag", yag_SQL);

        }

        // hata
        if(!(hatamesaji.isEmpty())){
            text_hata.setText(hatamesaji);
            hataicindeneme.setVisibility(View.VISIBLE);
            text_hata.setVisibility(View.VISIBLE);
        }
        veriler.close();
        if(hatamesaji.isEmpty()){
            Intent i = new Intent(KayitHedef.this, MainActivity.class);
            startActivity(i);
        }
    }

    public void hatadurumu(){

        ImageView hataicindeneme = (ImageView)findViewById(R.id.hataicindeneme);
        hataicindeneme.setVisibility(View.GONE);

        TextView text_hata = (TextView)findViewById(R.id.text_hata);
        text_hata.setVisibility(View.GONE);
    }
    public void hangibirimkullanildi(){
        Verit veriler = new Verit(this);
        veriler.open();
        long rowID = 1;
        String fields[] = new String[] {
                "_id",
                "kullanici_birim"
        };
        Cursor c = veriler.sec("kullanicilar", fields, "_id", rowID);
        String mesurment;
        mesurment = c.getString(1);
        // Metre mi
        if(mesurment.startsWith("m")){
        }
        else{
            TextView olcumbirimi5 = (TextView)findViewById(R.id.olcumbirimi5);
            olcumbirimi5.setText("pound");


            // her hafta kilo olcu donuşmu
            TextView t_haftalıkbilgi = (TextView)findViewById(R.id.t_haftalıkbilgi);
            t_haftalıkbilgi.setText("pound her hafta");
        }
        veriler.close();
    }
    private String yasAl(int yilDegeri, int ayDegeri, int gunDegeri){
        Calendar vr = Calendar.getInstance();
        Calendar gecrli = Calendar.getInstance();

        vr.set(yilDegeri, ayDegeri, gunDegeri);

        int kullaniciYasi = gecrli.get(Calendar.YEAR) - vr.get(Calendar.YEAR);

        if (gecrli.get(Calendar.DAY_OF_YEAR) < vr.get(Calendar.DAY_OF_YEAR)){
            kullaniciYasi--;
        }
        Integer i_kullaniciYasi = new Integer(kullaniciYasi);
        String s_kullaniciYasi = i_kullaniciYasi.toString();

        return s_kullaniciYasi;
    }
}
