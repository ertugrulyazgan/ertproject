package com.example.deeptrancer.diyetuygulamam496;

/**
 * Created by deeptrancer on 2.03.2018.
 */


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Kayit extends AppCompatActivity {




    public void  gecis(View view) {

        Intent gec = new Intent (this, durum.class);
        startActivity(gec);

    }


    //gun ve yil icin  */
    private String[] guntut = new String[31];
    private String[] yiltut = new String[100];



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up);


        //dogum gunune gore doldurabililiyormu kontrol et. // EVET
        int kisicinSayac = 0;
        for(int x=0;x<31;x++){
            kisicinSayac=x+1;
            this.guntut[x] = "" + kisicinSayac;
        }
        Spinner gun = (Spinner) findViewById(R.id.gun);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, guntut);
        gun.setAdapter(adapter);

        //doldur
        // guncel yilla kiyasla
        Calendar calendar = Calendar.getInstance();
        int yili = calendar.get(Calendar.YEAR);
        int kadar = yili-100;
        int nerede = 0;
        for(int x=yili;x>kadar;x--){
            this.yiltut[nerede] = "" + x;
            nerede++;
        }

        Spinner yil = (Spinner) findViewById(R.id.yil);
        ArrayAdapter<String> adapterYear = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, yiltut);
        yil.setAdapter(adapterYear);




        // hata durumunda
        ImageView i_hata = (ImageView)findViewById(R.id.i_hata);
        i_hata.setVisibility(View.GONE);

        TextView t_hata = (TextView)findViewById(R.id.t_hata);
        t_hata.setVisibility(View.GONE);

        //uygun olmayan deger
        EditText uzunlugunuz_inc = (EditText)findViewById(R.id.uzunlugunuz_inc);
        uzunlugunuz_inc.setVisibility(View.GONE);


        // uygun olcu mu
        Spinner olcumbirimi = (Spinner)findViewById(R.id.olcumbirimi);
        olcumbirimi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int yeri, long id) {
                olcudonusumu();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // olcudonusumu();
            }
        });





        // bilgileri al go to hedef sayfası
        Button b_kayit = (Button)findViewById(R.id.b_kayit);
        b_kayit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                kayitdoldur();
            }
        });


    }
    public void olcudonusumu() {

        Spinner olcumbirimi = (Spinner)findViewById(R.id.olcumbirimi);
        String s_olcum = olcumbirimi.getSelectedItem().toString();


        EditText uzunlugunuz_cm = (EditText)findViewById(R.id.uzunlugunuz_cm);
        EditText uzunlugunuz_inc = (EditText)findViewById(R.id.uzunlugunuz_inc);
        String s_cm = uzunlugunuz_cm.getText().toString();
        String s_inc = uzunlugunuz_inc.getText().toString();

        double d_uzunluk_cm = 0;
        double d_uzunluk_feet = 0;
        double d_uzunluk_inc = 0;

        TextView santim = (TextView)findViewById(R.id.santim);
        TextView t_kilo = (TextView)findViewById(R.id.t_kilo);

        if(s_olcum.startsWith("I")){
            uzunlugunuz_inc.setVisibility(View.VISIBLE);
            santim.setText(" feet ve inch");
            t_kilo.setText("ibs birimi");

            try {
                d_uzunluk_cm = Double.parseDouble(s_cm);
            }
            catch(NumberFormatException nfe) {

            }
            if(d_uzunluk_cm != 0){
                // cm ye donusum icin.
                d_uzunluk_feet = (d_uzunluk_cm * 0.3937008)/12;
                // d_uzunluk_feet = Math.round(d_uzunluk_feet);
                int f_uzunluk = (int) d_uzunluk_feet;

                uzunlugunuz_cm.setText("" + f_uzunluk);

            }

        }
        else{
            // Metre
            uzunlugunuz_inc.setVisibility(View.GONE);
            santim.setText("cm");
            t_kilo.setText("kg");

            // cm ye cevir

            try {
                d_uzunluk_feet = Double.parseDouble(s_cm);
            }
            catch(NumberFormatException nfe) {

            }

            //inch e cevir
            try {
                d_uzunluk_inc = Double.parseDouble(s_inc);
            }
            catch(NumberFormatException nfe) {

            }

            // cm üzerinden islem yapıyorum. çevrip öyle kullanacağım.
            if(d_uzunluk_feet != 0 && d_uzunluk_inc != 0) {
                d_uzunluk_cm = ((d_uzunluk_feet * 12) + d_uzunluk_inc) * 2.54;
                d_uzunluk_cm = Math.round(d_uzunluk_cm);
                uzunlugunuz_cm.setText("" + d_uzunluk_cm);
            }
        }



        // agirlik
        EditText edt_agirlik = (EditText)findViewById(R.id.edt_agirlik);
        String s_agirlik = edt_agirlik.getText().toString();
        double d_agirlik = 0;

        try {
            d_agirlik = Double.parseDouble(s_agirlik);
        }
        catch(NumberFormatException nfe) {
        }

        if(d_agirlik != 0) {

            if (s_olcum.startsWith("I")) {
                // p ye
                d_agirlik = Math.round(d_agirlik / 0.45359237);
            } else {
                // ky ye
                d_agirlik = Math.round(d_agirlik * 0.45359237);
            }
            edt_agirlik.setText("" + d_agirlik);
        }

    }


    public void kayitdoldur() {
        // bos girildi
        ImageView i_hata = (ImageView)findViewById(R.id.i_hata);
        TextView t_hata = (TextView)findViewById(R.id.t_hata);
        String hataliGirdi = "";

        // isim bos
        TextView t_isim = (TextView)findViewById(R.id.t_isim);
        EditText text_isim = (EditText)findViewById(R.id.text_isim);
        String s_email = text_isim.getText().toString();
        if(s_email.isEmpty() || s_email.startsWith(" ")){
            hataliGirdi = "gecerli bir isim giriniz";
        }

        // tarih hatalı format
        Spinner gun = (Spinner)findViewById(R.id.gun);
        String s_gun = gun.getSelectedItem().toString();
        int gunicin = 0;
        try {
            gunicin = Integer.parseInt(s_gun);

            if(gunicin < 10){
                s_gun = "0" + s_gun;
            }

        }
        catch(NumberFormatException nfe) {
            System.out.println("hata ! " + nfe);
            hataliGirdi = "doğum tarihi için geçerli bir deger girin.";
        }

        // dogum ay
        Spinner ay = (Spinner)findViewById(R.id.ay);
        String s_ay = ay.getSelectedItem().toString();
        int ayicin = ay.getSelectedItemPosition();
        int month = ayicin+1;
        if(month < 10){
            s_ay = "0" + month;
        }
        else{
            s_ay = "" + month;
        }

        //yil
        Spinner yil = (Spinner)findViewById(R.id.yil);
        String s_yil = yil.getSelectedItem().toString();
        int yilicin = 0;
        try {
            yilicin = Integer.parseInt(s_yil);
        }
        catch(NumberFormatException nfe) {
            System.out.println("hata !" + nfe);
            hataliGirdi = "doğum tarihi için  gecerli bir yıl giriniz";
        }

        // tarih koy
        String s_dogumTarihi = yilicin + "-" + s_ay + "-" + s_gun;


        // cinsiyet// buna göre hesaplama degisecek
        RadioGroup rg_cinsiyet = (RadioGroup)findViewById(R.id.rg_cinsiyet);
        int r_button_Id = rg_cinsiyet.getCheckedRadioButtonId();
        View radioButtonGender = rg_cinsiyet.findViewById(r_button_Id);
        int yeri = rg_cinsiyet.indexOfChild(radioButtonGender);

        String cinsiyetiNe = "";
        if(yeri == 0){
            cinsiyetiNe = "bay";
        }
        else{
            cinsiyetiNe = "bayan";
        }

        //uzunluk
        EditText uzunlugunuz_cm = (EditText)findViewById(R.id.uzunlugunuz_cm);
        EditText uzunlugunuz_inc = (EditText)findViewById(R.id.uzunlugunuz_inc);
        String s_cm = uzunlugunuz_cm.getText().toString();
        String s_inc = uzunlugunuz_inc.getText().toString();

        double d_uzunluk_cm = 0;
        double d_uzunluk_feet = 0;
        double d_uzunluk_inc = 0;
        boolean metre = true;

        //metre cinsinden mi
        Spinner olcumbirimi = (Spinner)findViewById(R.id.olcumbirimi);
        String s_olcum = olcumbirimi.getSelectedItem().toString();

        int olcubirimiInt = olcumbirimi.getSelectedItemPosition();
        if(olcubirimiInt == 0){
            s_olcum = "metre";
        }
        else{
            s_olcum = "imp";
            metre = false;
        }

        if(metre == true) {

            //cm ye donustur
            try {
                d_uzunluk_cm = Double.parseDouble(s_cm);
                d_uzunluk_cm = Math.round(d_uzunluk_cm);
            }
            catch(NumberFormatException nfe) {
                hataliGirdi = "boyunuz bir sayi girdisi olmalı";
            }
        }
        else {

            try {
                d_uzunluk_feet = Double.parseDouble(s_cm);
            }
            catch(NumberFormatException nfe) {
                hataliGirdi = "Boyunuz bir sayi girdisi olmalı";
            }

            // inç
            try {
                d_uzunluk_inc = Double.parseDouble(s_inc);
            }
            catch(NumberFormatException nfe) {
                hataliGirdi = "boyunuz bir sayi girdisi olmalı";
            }

            // cm ile ilem yapıcam.
            d_uzunluk_cm = ((d_uzunluk_feet * 12) + d_uzunluk_inc) * 2.54;
            d_uzunluk_cm = Math.round(d_uzunluk_cm);
        }

        // agirlik
        EditText edt_agirlik = (EditText)findViewById(R.id.edt_agirlik);
        String s_agirlik = edt_agirlik.getText().toString();
        double d_agirlik = 0;
        try {
            d_agirlik = Double.parseDouble(s_agirlik);
        }
        catch(NumberFormatException nfe) {
            hataliGirdi = "Gecerli bir agirlik birimi girin. sayisal bir deger olmalı";
        }
        if(metre == true) {
        }
        else{
            d_agirlik = Math.round(d_agirlik*0.45359237);
        }

        Spinner aktiviteyogunluk = (Spinner)findViewById(R.id.aktiviteyogunluk);
        //  0: gerek yok
        // 1: biraz (haftada 1-3 gun)
        // 2: orta düzey (haftada 3-5 gun))
        // 3: yogun (neredeyse her gun)
        // 4: cok yogun (gunde 2 kademe)
        int programyogunlugu = aktiviteyogunluk.getSelectedItemPosition();

        //hata
        if(hataliGirdi.isEmpty()){
            // veritbanı hata
            i_hata.setVisibility(View.GONE);
            t_hata.setVisibility(View.GONE);


            // veritabanına veri Ekleme
            Verit db = new Verit(this);
            db.open();

            String isimSQL = db.aktarma(s_email);
            String s_dogumTarihiSQL = db.aktarma(s_dogumTarihi);
            String s_cinsiyetSQL = db.aktarma(cinsiyetiNe);
            double uzunlukSQL = db.aktarma(d_uzunluk_cm);
            int programyogunluguSQL = db.aktarma(programyogunlugu);
            double agirlikSQL = db.aktarma(d_agirlik);
            String s_olcumbSQL = db.aktarma(s_olcum);

            // kullanici girisi. sorun var // çözüldü
            String s_giris = "NULL, " + isimSQL + "," + s_dogumTarihiSQL + "," + s_cinsiyetSQL + "," + uzunlukSQL + "," + s_olcumbSQL;
            db.insert("kullanicilar",
                    "_id, kullanici_isim, kullanici_d, kullanici_cinsiyet, kullanici_boy, kullanici_birim",
                    s_giris);

            // hedef belirleme
            SimpleDateFormat tf = new SimpleDateFormat("yyyy-MM-dd");
            String hedefTarihi = tf.format(Calendar.getInstance().getTime());

            String s_hedefSQL = db.aktarma(hedefTarihi);

            s_giris = "NULL, " + agirlikSQL + "," + s_hedefSQL + "," + programyogunluguSQL;
            db.insert("hedef",
                    "_id, hedef_guncel_kilo, hedef_tarih, goal_activity_level",
                    s_giris);



            db.close();

            // her halukarda hedef belirlemesi için sonraki sayfaya yonlendirdim.
            Intent i = new Intent(Kayit.this, KayitHedef.class);
            startActivity(i);
        }
        else {
            // hata durumu
            t_hata.setText(hataliGirdi);
            i_hata.setVisibility(View.VISIBLE);
            t_hata.setVisibility(View.VISIBLE);
        }
    }

}
