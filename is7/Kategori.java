package com.example.deeptrancer.diyetuygulamam496;

/**
 * Created by deeptrancer on 2.03.2018.
 */


import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Kategori.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Kategori#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Kategori extends Fragment {

    
    private View anahat;
    private Cursor kategoriListele;
    private Cursor besinListele;

    // ust menudeki butonların islevi
    private MenuItem tollbar_besineDegis;
    private MenuItem toolbar_besinSil;

    // ust menu ve buton
    private String kategoriID;
    private String kategoriIsim;

    private String gidalarID;
    private String gidaISmi;




    // TODO: Rename parameter arguments, choose names that match

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mlListener;



    //default constructor gerekli oldu
    public Kategori() {
    }


    public static Kategori newInstance(String param1, String param2) {
        Kategori fragment = new Kategori();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }



    // toolbarmenusuayarlar
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((Menu)getActivity()).getSupportActionBar().setTitle("Kategoriler");

        // kategorileri olustur
        listle("0", ""); // Parent

        // menuyu olustr
        setHasOptionsMenu(true);


    }


    // gerektirginde degisiklik yapabilme. anatema
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        anahat = inflater.inflate(R.layout.kategoriler, container, false);
        return anahat;
    }

    private void setAnahat(int id){
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        anahat = inflater.inflate(id, null);
        ViewGroup rootView = (ViewGroup) getView();
        rootView.removeAllViews();
        rootView.addView(anahat);
    }

    public void onCreateOptionsMenu(android.view.Menu menu, MenuInflater inflater) {

        ((Menu)getActivity()).getMenuInflater().inflate(R.menu.manu_kategori, menu);


        tollbar_besineDegis = menu.findItem(R.id.action_edit);
        toolbar_besinSil = menu.findItem(R.id.action_delete);

        tollbar_besineDegis.setVisible(false);
        toolbar_besinSil.setVisible(false);
    }


    // ustteki icon tıklandıgında
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        int id = menuItem.getItemId();
        if (id == R.id.action_add) {
            yeniKategoriOlustur();
        }
        else if (id == R.id.action_edit) {
            kategoriEditle();
        }
        else if (id == R.id.action_delete) {
            kategoriSil();
        }
        return super.onOptionsItemSelected(menuItem);
    }


    public void listle(String PaID, String parentName){


        Verit veriler = new Verit(getActivity());
        veriler.open();

        // kategorileri alalım
        String fields[] = new String[] {
                "_id",
                "kategori_isim",
                "kategori_pid"
        };
        kategoriListele = veriler.sec("kategoriler", fields, "kategori_pid", PaID, "kategori_isim", "ASC");


        ArrayList<String> veri = new ArrayList<String>();

        // strnge ceviriyorum.
        int kategoriSayisi = kategoriListele.getCount();
        for(int i=0;i<kategoriSayisi;i++){
            veri.add(kategoriListele.getString(kategoriListele.getColumnIndex("kategori_isim")));
            kategoriListele.moveToNext();
        }


        ArrayAdapter<String> adt = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, veri);

        ListView lv = (ListView)getActivity().findViewById(R.id.kategorilerGost);
        lv.setAdapter(adt);

        if(PaID.equals("0")) {
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    tiklandigindaListele(arg2);
                }
            });
        }
        veriler.close();

        if(PaID.equals("0")){

        }
        else{
            // duzeltme iconunu kullanılabilir hale getir
            tollbar_besineDegis.setVisible(true);
            toolbar_besinSil.setVisible(true);
        }

    }


    public void tiklandigindaListele(int listItemIDClicked){   //**********
        kategoriListele.moveToPosition(listItemIDClicked);

        kategoriID = kategoriListele.getString(0);
        kategoriIsim = kategoriListele.getString(1);
        String parentID = kategoriListele.getString(2);

        ((Menu)getActivity()).getSupportActionBar().setTitle(kategoriIsim);

        listle(kategoriID, kategoriIsim);

        kategorideGoster(kategoriID, kategoriIsim, parentID);


    }

    //yeni bir kategori oluşturmak için;s
    public void yeniKategoriOlustur(){                         //**************
        int IDn = R.layout.kategoriduzenle;
        setAnahat(IDn);

        Verit veriler = new Verit(getActivity());
        veriler.open();

        String fields[] = new String[] {
                "_id",
                "kategori_isim",
                "kategori_pid"
        };
        Cursor dataCursor = veriler.sec("kategoriler", fields, "kategori_pid", "0", "kategori_isim", "ASC");

        int cursoySay = dataCursor.getCount();
        String[] diziKategori = new String[cursoySay+1];

        diziKategori[0] = "-";

        // stringe cevir
        for(int i=1;i<cursoySay+1;i++){
            diziKategori[i] = dataCursor.getString(1).toString();
            dataCursor.moveToNext();
        }

        Spinner s_Parent = (Spinner) getActivity().findViewById(R.id.spKategori);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, diziKategori);
        s_Parent.setAdapter(adapter);


        Button anabuton = (Button)getActivity().findViewById(R.id.butonKategori);
        anabuton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yollamakIcinYeniKategoriOlustur();
            }
        });

        veriler.close();

    }

    public void yollamakIcinYeniKategoriOlustur() {                              //*******************

        Verit veriler = new Verit(getActivity());
        veriler.open();

        int hata = 0;

        EditText editTextName = (EditText)getActivity().findViewById(R.id.et_isim);
        String stringName = editTextName.getText().toString();
        if(stringName.equals("")){
            Toast.makeText(getActivity(), "Lütfen bir isim degeri giriniz", Toast.LENGTH_SHORT).show();
            hata = 1;
        }

        Spinner spinner = (Spinner)getActivity().findViewById(R.id.spKategori);
        String spinnerkategori = spinner.getSelectedItem().toString();
        String pIDno;
        if(spinnerkategori.equals("-")){
            pIDno = "0";
        }
        else{
            // parent id bulmak istiyoruz
            String kategoriParentSQL = veriler.aktarma(spinnerkategori);
            String fields[] = new String[] {
                    "_id",
                    "kategori_isim",
                    "kategori_pid"
            };
            Cursor bul_parentID = veriler.sec("kategoriler", fields, "kategori_isim", kategoriParentSQL);
            pIDno = bul_parentID.getString(0).toString();


        }

        if(hata == 0){

            String isimSQL = veriler.aktarma(stringName);
            String idSQL = veriler.aktarma(pIDno);

            String input = "NULL, " + isimSQL + ", " + idSQL;
            veriler.insert("kategoriler", "_id, kategori_isim, kategori_pid", input);

            Toast.makeText(getActivity(), "Kategori başarıyla oluşturuldu .", Toast.LENGTH_LONG).show();

            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.doldur, new Kategori(), Kategori.class.getName()).commit();

        }

        veriler.close();
    } // yollamakIcinYeniKategoriOlustur


    //kategori düzenleme
    public void kategoriEditle(){

        //diger sayfaya gec
        int id = R.layout.kategoriduzenle;
        setAnahat(id);

        Verit veriler = new Verit(getActivity());
        veriler.open();

        //parent id icin sorgu
        Cursor c;
        String fieldsC[] = new String[] { "kategori_pid" };
        String currentIdSQL = veriler.aktarma(kategoriID);
        c = veriler.sec("kategoriler", fieldsC, "_id", currentIdSQL);
        String s_parentID = c.getString(0);
        int i_parentID = 0;
        try {
            i_parentID = Integer.parseInt(s_parentID);
        }
        catch(NumberFormatException nfe) {
            System.out.println("hata ! " + nfe);
        }
        // ismi koy
        EditText ed_duzenle = (EditText) getActivity().findViewById(R.id.et_isim);
        ed_duzenle.setText(kategoriIsim);


        //kategori koy
        String fields[] = new String[] {
                "_id",
                "kategori_isim",
                "kategori_pid"
        };
        Cursor veriCursor = veriler.sec("kategoriler", fields, "kategori_pid", "0", "kategori_isim", "ASC");

        int i_cursorSayisi = veriCursor.getCount();
        String[] kategori_s_dizi = new String[i_cursorSayisi+1];

        // parent
        kategori_s_dizi[0] = "-";

        // cursor stringe cevir
        int parentIDkontrol = 0;
        for(int i=1;i<i_cursorSayisi+1;i++){
            kategori_s_dizi[i] = veriCursor.getString(1).toString();

            if(veriCursor.getString(0).toString().equals(s_parentID)){
                parentIDkontrol = i;
            }

            veriCursor.moveToNext();
        }


        Spinner s_parent = (Spinner) getActivity().findViewById(R.id.spKategori);
        ArrayAdapter<String> adt = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, kategori_s_dizi);
        s_parent.setAdapter(adt);

        s_parent.setSelection(parentIDkontrol);


        veriler.close();


        Button buttonHome = (Button)getActivity().findViewById(R.id.butonKategori);
        buttonHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kategoriDuzenle();                                             //**********
            }
        });

    }
    
    public void kategoriDuzenle(){

        Verit veriler = new Verit(getActivity());
        veriler.open();

        int hata = 0;

        // isim
        EditText editTextName = (EditText)getActivity().findViewById(R.id.et_isim);
        String s_isim = editTextName.getText().toString();
        if(s_isim.equals("")){
            Toast.makeText(getActivity(), "Lütfen bir isim giriniz", Toast.LENGTH_SHORT).show();
            hata = 1;
        }

        Spinner spinner = (Spinner)getActivity().findViewById(R.id.spKategori);
        String KategoriPar = spinner.getSelectedItem().toString();
        String s_id;
        if(KategoriPar.equals("-")){
            s_id = "0";
        }
        else{
            String skategoriParSQL = veriler.aktarma(KategoriPar);
            String fields[] = new String[] {
                    "_id",
                    "kategori_isim",
                    "kategori_pid"
            };
            Cursor findParentID = veriler.sec("kategoriler", fields, "kategori_isim", skategoriParSQL);
            s_id = findParentID.getString(0).toString();


        }

        if(hata == 0){
            long l_IDdegeri = Long.parseLong(kategoriID);

            long IDSQL = veriler.aktarma(l_IDdegeri);
            String s_isimSQL = veriler.aktarma(s_isim);
            String s_p_idSQL = veriler.aktarma(s_id);

            // veriyi koy
            String giris = "NULL, " + s_isimSQL + ", " + s_p_idSQL;
            // veriler.insert("kategoriler", "_id, kategori_isim, kategori_pid", input);
            veriler.guncelle("kategoriler", "_id", IDSQL, "kategori_isim", s_isimSQL);
            veriler.guncelle("kategoriler", "_id", IDSQL, "kategori_pid", s_p_idSQL);

            // kulalniciya bilgi verelim
            Toast.makeText(getActivity(), "Değişiklikler başarıyla kaydedildi . ", Toast.LENGTH_LONG).show();

            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.doldur, new Kategori(), Kategori.class.getName()).commit();

        }

        veriler.close();
    }


    // kategori silmek istediğimizde;
    public void kategoriSil(){


        int gec = R.layout.kategorisil;                              //******
        setAnahat(gec);


        Button buttonCancel = (Button)getActivity().findViewById(R.id.vazgecbutonu);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kategoriyiSill();
            }
        });

        Button silmeDogrulama = (Button)getActivity().findViewById(R.id.silmeyiDogrula);
        silmeDogrulama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kategoriSilmeDogrulamasi();
            }
        });


    }
    public void kategoriyiSill(){
        // Move user back to correct design
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.doldur, new Kategori(), Kategori.class.getName()).commit();

    }
    public void kategoriSilmeDogrulamasi(){


        Verit veriler = new Verit(getActivity());
        veriler.open();

        long ID = 0;
        try {
            ID = Long.parseLong(kategoriID);
        }
        catch (NumberFormatException e){
            Toast.makeText(getActivity(), "Hata ! " + e.toString(), Toast.LENGTH_LONG).show();
        }

        long currentIDSQL = veriler.aktarma(ID);

        veriler.sil("kategoriler", "_id", currentIDSQL);

        veriler.close();

        //bilgilendirme
        Toast.makeText(getActivity(), "Kategori başarıyla Silindi", Toast.LENGTH_LONG).show();

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.doldur, new Kategori(), Kategori.class.getName()).commit();

    }


    public void kategorideGoster(String kategori, String kategoriName, String kategoripar){
        if(!(kategoripar.equals("0"))) {

            int idsi = R.layout.gidalar;
            setAnahat(idsi);
            Verit veri = new Verit(getActivity());
            veri.open();
            String str[] = new String[] {
                    "_id",
                    "gida_isim",
                    "gida_Marka",
                    "gida_aciklama",
                    "gida_servisGram",
                    "gida_servisGramolcm",
                    "gida_servisAdet",
                    "gida_servisAdetolcm",
                    "gida_olculenEnerji"
            };
            besinListele = veri.sec("gidalar", str, "gida_kategori", kategori, "gida_isim", "ASC");

            int boyut = besinListele.getCount();
            for(int i=0;i<boyut;i++){
                Toast.makeText(getActivity(), "_id: " + besinListele.getString(0) + "\ngida ismi: " + besinListele.getString(1), Toast.LENGTH_SHORT).show();
                besinListele.moveToNext();
            }


            ListView besinliste = (ListView)getActivity().findViewById(R.id.besinliste);


            Gida icerik = new Gida(getActivity(), besinListele);


            besinliste.setAdapter(icerik);


            besinliste.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    besinListele(arg2);
                }
            });

            veri.close();

        }
    } // besinleriKGoster


    private void besinListele(int besinlisteleindex){

        gidalarID = besinListele.getString(0);
        gidaISmi = besinListele.getString(1);

        Fragment fragment = null;
        Class fragmentClass = null;
        fragmentClass = GidaSec.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Bundle bundle = new Bundle();
        bundle.putString("gidalarID", ""+ gidalarID); //ekle
        fragment.setArguments(bundle);

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.doldur, fragment).commit();



    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    // TODO: Rename method, guncelle argument and hook method into UI event
    public void butonabasildiginda(Uri uri) {
        if (mlListener != null) {
            mlListener.onFragmentInteraction(uri);
        }
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mlListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " !");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mlListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
