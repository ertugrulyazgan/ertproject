package com.example.deeptrancer.diyetuygulamam496;

        import android.content.Intent;
        import android.os.Bundle;
        import android.view.View;
        import android.support.v7.app.AppCompatActivity;

        import com.facebook.stetho.Stetho;
        import com.facebook.stetho.okhttp3.StethoInterceptor;

        import okhttp3.OkHttpClient;

public class MainActivity extends AppCompatActivity{


    public void  yonlendir(View view) {

        Intent gec = new Intent (this, Kayit.class);
        startActivity(gec);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //stetho kullandm
        Stetho.initializeWithDefaults(this);

        new OkHttpClient.Builder()
                .addNetworkInterceptor(new StethoInterceptor())
                .build();


        Verit veriler = new Verit(this);
        veriler.open();


        int dizi = veriler.sayim("gidalar");

        if(dizi < 1){

            VeritEkleme ekle = new VeritEkleme(this);
            ekle.tumKategorileriAktar();
            ekle.tumbesinleriAktar();


        }


        dizi = veriler.sayim("kullanicilar");


        veriler.close();

        if(dizi < 1){


            Intent i = new Intent(MainActivity.this, Kayit.class);
            startActivity(i);
        }
        else{
            Intent i = new Intent(MainActivity.this, Menu.class);
            startActivity(i);

        }

    }

}
