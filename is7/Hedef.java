package com.example.deeptrancer.diyetuygulamam496;

/**
 * Created by deeptrancer on 2.03.2018.
 */

       import android.content.Context;
        import android.database.Cursor;
        import android.net.Uri;
        import android.os.Bundle;
        import android.support.v4.app.Fragment;
        import android.support.v4.app.FragmentManager;
        import android.view.LayoutInflater;
       import android.view.MenuInflater;
        import android.view.MenuItem;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.Button;
        import android.widget.CheckBox;
        import android.widget.CompoundButton;
        import android.widget.EditText;
        import android.widget.Spinner;
        import android.widget.TableRow;
        import android.widget.TextView;
        import android.widget.Toast;

        import java.text.SimpleDateFormat;
        import java.util.Calendar;
        import java.text.DateFormat;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Hedef.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Hedef#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Hedef extends Fragment {

    private View anahat;


    private MenuItem menuDuzenle;
    private MenuItem menuBirimSil;


    private String idNo;
    private String isim;


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    public Hedef() {
    }


    public static Hedef newInstance(String param1, String param2) {  //constructor tamam
        Hedef fragment = new Hedef();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //baslik
        ((Menu)getActivity()).getSupportActionBar().setTitle("Hedef");

        // verial
        verial();

        // menu oluştur
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        anahat = inflater.inflate(R.layout.hedef, container, false);
        return anahat;
    }

    private void gorunumuAyarla(int id){
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        anahat = inflater.inflate(id, null);
        ViewGroup rootView = (ViewGroup) getView();
        rootView.removeAllViews();
        rootView.addView(anahat);
    }


    public void onCreateOptionsMenu(android.view.Menu menu, MenuInflater inflater) {


        MenuInflater minf = ((Menu)getActivity()).getMenuInflater();
        inflater.inflate(R.menu.menu_goal, menu);

        menuDuzenle = menu.findItem(R.id.besinDuzenle);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        int deger = menuItem.getItemId();
        if (deger == R.id.menu_hedefAyarlama) {
            hedefDuzenleme();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public void verial(){



        // veri
        Verit veriler = new Verit(getActivity());
        veriler.open();

        long long_rowID = 1;
        String fields[] = new String[] {
                "_id",
                "kullanici_birim"
        };
        Cursor crsrtut = veriler.sec("kullanicilar", fields, "_id", long_rowID);
        String mesurment;
        mesurment = crsrtut.getString(1);


        String fieldsGoal[] = new String[] {
                "_id",
                "hedef_guncel_kilo",
                "hedef_kilo",
                "hedef_almakvermek",
                "hedef_haftalik_tempo",
                "goal_activity_level",
                "hedef_tarih",
        };
        Cursor hedefSatir = veriler.sec("hedef", fieldsGoal, "", "", "_id", "DESC");

        String hedefID = hedefSatir.getString(0);
        String suanAgirlik = hedefSatir.getString(1);
        String hedeflenenAgirlik = hedefSatir.getString(2);
        String almakyadavermek = hedefSatir.getString(3);
        String hedefTempo = hedefSatir.getString(4);
        String hedeftemptohaftalik = hedefSatir.getString(5);
        String hedefZaman = hedefSatir.getString(6);

        TextView agirlikbelirtec = (TextView)getActivity().findViewById(R.id.tv_hedefKilo);
        if(mesurment.startsWith("m")) {
            // Metre
            agirlikbelirtec.setText(suanAgirlik + " kg (" + hedefZaman + ")");
        }
        else{
            //kilo yada pound
            double suankiAgirlik = 0;

            try {
                suankiAgirlik = Double.parseDouble(suanAgirlik);
            }
            catch(NumberFormatException nfe) {
                System.out.println("hata !" + nfe);
            }

            double agirlikP =  Math.round(suankiAgirlik / 0.45359237);


            agirlikbelirtec.setText(agirlikP + " pounds (" + hedefZaman + ")");
        }

        TextView hedefdeger = (TextView)getActivity().findViewById(R.id.tv_suankihedef);
        if(mesurment.startsWith("m")) {
            //metr
            hedefdeger.setText(hedeflenenAgirlik + " kg");
        }
        else{
            //kg dan cvr
            double hedeflenenkilo_no = 0;

            try {
                hedeflenenkilo_no = Double.parseDouble(hedeflenenAgirlik);
            }
            catch(NumberFormatException nfe) {
                System.out.println("Hata " + nfe);
            }
            // kilidan
            double hedeflenenkilodanpounda =  Math.round(hedeflenenkilo_no / 0.45359237);


            hedefdeger.setText(hedeflenenkilodanpounda + " pounds");
        }



        TextView hedefmetod = (TextView)getActivity().findViewById(R.id.HedefText);

        String method = "";
        if(almakyadavermek.equals("0")){
            method = "ver "  + hedefTempo;
        }
        else{
            method = "al "  + hedefTempo;
        }
        if(mesurment.startsWith("m")) {
            method = method + " kg/hafta";
        }
        else{
            method = method + " pounds/hafta";
        }
        hedefmetod.setText(method);


        TextView egzersizSeviye = (TextView)getActivity().findViewById(R.id.tv_aktivitesetempo);
        if(hedeftemptohaftalik.equals("0")){
            egzersizSeviye.setText("Egzersize gerek yok");
        }
        else if(hedeftemptohaftalik.equals("1")){
            egzersizSeviye.setText("Hafif egzersiz. Haftada 1 veya 3 gun");
        }
        else if(hedeftemptohaftalik.equals("2")){
            egzersizSeviye.setText("Orta duzen egzersiz. Haftada 3 4 gun");
        }
        else if(hedeftemptohaftalik.equals("3")){
            egzersizSeviye.setText("Agir egzersiz. Haftanin 6 gunu");
        }
        else if(hedeftemptohaftalik.equals("4")){
            egzersizSeviye.setText("Ekstra agir egzersiz. Haftanin her gunu x2 plan");
        }

        tabloguncelle();

        hedefGoruntule(false);

        CheckBox checkBoxAdvanced = (CheckBox)getActivity().findViewById(R.id.chechHedef);

        checkBoxAdvanced.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                        @Override
                                                        public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                                                            hedefGoruntule(isChecked);
                                                        }
                                                    }
        );

        veriler.close();
    }


    public void hedefGoruntule(boolean isChecked){



        TableRow hedefMetodA = (TableRow)getActivity().findViewById(R.id.tv_hedefyontem);

        TableRow hedefMetodB = (TableRow)getActivity().findViewById(R.id.tv_hedefmetodu);

        TextView hedefenerji = (TextView)getActivity().findViewById(R.id.textViewGoalHeadcellEnergy );
        TextView hedefProtein = (TextView)getActivity().findViewById(R.id.tv_hedefPro);
        TextView hedefKarbonhidrat = (TextView)getActivity().findViewById(R.id.tv_hedefKar);
        TextView hedefYag = (TextView)getActivity().findViewById(R.id.tv_hedefYag);

        TextView hedefProteinBirim = (TextView)getActivity().findViewById(R.id.tv_hedefProteinBRM);
        TextView hedefkarbonhidaratbrm = (TextView)getActivity().findViewById(R.id.tv_hedefKArbonBrm);
        TextView hedefyagbrm = (TextView)getActivity().findViewById(R.id.tvYagBrm);

        TextView hedefproteinDiet = (TextView)getActivity().findViewById(R.id.tv_hedefDiyetProtein);
        TextView hedefkarbonDiet = (TextView)getActivity().findViewById(R.id.tv_hedefDiyetKArb);
        TextView hedefyagDiet = (TextView)getActivity().findViewById(R.id.tv_hedefDiyetYag);

        TextView hedeEgzersizliProtein = (TextView)getActivity().findViewById(R.id.hedefproteinaktiviteyle);
        TextView hedefEgzersizliKArbonhidrat = (TextView)getActivity().findViewById(R.id.hedefkarbaktiviteyle);
        TextView hedefEgzersizliYag = (TextView)getActivity().findViewById(R.id.tv_hedefyagaktiviteyle);

        TextView hedefProteinAktiviteveDiet = (TextView)getActivity().findViewById(R.id.tv_hedefProteinDiyetle);
        TextView hedefKarbonAktivitivediyet = (TextView)getActivity().findViewById(R.id.tv_karbonhidratDiyetle);
        TextView hedefAktivityveDiet = (TextView)getActivity().findViewById(R.id.hedefEgzersizveDiyet);

        if(isChecked == false){
            hedefMetodA.setVisibility(View.GONE);
            hedefMetodB.setVisibility(View.GONE);
            hedefenerji.setVisibility(View.GONE);
            hedefProtein.setVisibility(View.GONE);
            hedefKarbonhidrat.setVisibility(View.GONE);
            hedefYag.setVisibility(View.GONE);
            hedefProteinBirim.setVisibility(View.GONE);
            hedefkarbonhidaratbrm.setVisibility(View.GONE);
            hedefyagbrm.setVisibility(View.GONE);
            hedefproteinDiet.setVisibility(View.GONE);
            hedefkarbonDiet.setVisibility(View.GONE);
            hedefyagDiet.setVisibility(View.GONE);
            hedeEgzersizliProtein.setVisibility(View.GONE);
            hedefEgzersizliKArbonhidrat.setVisibility(View.GONE);
            hedefEgzersizliYag.setVisibility(View.GONE);
            hedefProteinAktiviteveDiet.setVisibility(View.GONE);
            hedefKarbonAktivitivediyet.setVisibility(View.GONE);
            hedefAktivityveDiet.setVisibility(View.GONE);
        }
        else {
            hedefMetodA.setVisibility(View.VISIBLE);
            hedefMetodB.setVisibility(View.VISIBLE);
            hedefenerji.setVisibility(View.VISIBLE);
            hedefProtein.setVisibility(View.VISIBLE);
            hedefKarbonhidrat.setVisibility(View.VISIBLE);
            hedefYag.setVisibility(View.VISIBLE);
            hedefProteinBirim.setVisibility(View.VISIBLE);
            hedefkarbonhidaratbrm.setVisibility(View.VISIBLE);
            hedefyagbrm.setVisibility(View.VISIBLE);
            hedefproteinDiet.setVisibility(View.VISIBLE);
            hedefkarbonDiet.setVisibility(View.VISIBLE);
            hedefyagDiet.setVisibility(View.VISIBLE);
            hedeEgzersizliProtein.setVisibility(View.VISIBLE);
            hedefEgzersizliKArbonhidrat.setVisibility(View.VISIBLE);
            hedefEgzersizliYag.setVisibility(View.VISIBLE);
            hedefProteinAktiviteveDiet.setVisibility(View.VISIBLE);
            hedefKarbonAktivitivediyet.setVisibility(View.VISIBLE);
            hedefAktivityveDiet.setVisibility(View.VISIBLE);

        }
    }

    public void hedefDuzenleme(){
        int id = R.layout.hedefduzenle;
        gorunumuAyarla(id);

        Verit veriler = new Verit(getActivity());
        veriler.open();


        long long_sira = 1;
        String fields[] = new String[] {
                "_id",
                "kullanici_birim"
        };
        Cursor c = veriler.sec("kullanicilar", fields, "_id", long_sira);
        String olcum;
        olcum = c.getString(1);

        String fieldsGoal[] = new String[] { // hedef verileri alıyorum
                "_id",
                "hedef_guncel_kilo",
                "hedef_kilo",
                "hedef_almakvermek",
                "hedef_haftalik_tempo",
                "goal_activity_level"
        };
        Cursor hedefCursor = veriler.sec("hedef", fieldsGoal, "", "", "_id", "DESC");

        String goalID = hedefCursor.getString(0);
        String mecvutkilo_ = hedefCursor.getString(1);
        String hedefKilo = hedefCursor.getString(2);
        String s_almakyadavermek = hedefCursor.getString(3);
        String s_haftalikHedef = hedefCursor.getString(4);
        String s_tempo = hedefCursor.getString(5);

        // gecerli kilo
        EditText mecvutKilo = (EditText) getActivity().findViewById(R.id.et_mevcutKilo);
        if(olcum.startsWith("m")) {
            // metre
            mecvutKilo.setText(mecvutkilo_);
        }
        else{
            //metre
            double mevcutKiloNo = 0;

            try {
                mevcutKiloNo = Double.parseDouble(mecvutkilo_);
            }
            catch(NumberFormatException nfe) {
                System.out.println("Hata " + nfe);
            }
            double mevcutKilonoP =  Math.round(mevcutKiloNo / 0.45359237);


            mecvutKilo.setText(mevcutKilonoP+"");

            TextView mevcutKiloTur = (TextView)getActivity().findViewById(R.id.tv_hedfKilo);
            mevcutKiloTur.setText("pounds");
        }


        TextView hedeflenenAgirlik = (TextView)getActivity().findViewById(R.id.et_hedefKilosu);
        if(olcum.startsWith("m")) {
            // metre
            hedeflenenAgirlik.setText(hedefKilo);
        }
        else{

            // kg dan
            double hedefAgirlik = 0;

            try {
                hedefAgirlik = Double.parseDouble(hedefKilo);
            }
            catch(NumberFormatException nfe) {
                System.out.println("Hata " + nfe);
            }
            // kg dan
            double hedefAgirlikp =  Math.round(hedefAgirlik / 0.45359237);


            hedeflenenAgirlik.setText(hedefAgirlikp + "");


            // neden kapaniyor ! // TAMAM.
            TextView hedefagirliktur = (TextView)getActivity().findViewById(R.id.tv_hedefKilosu);
            hedefagirliktur.setText("pounds/week");
        }

        // vermek yada almak
        Spinner akmakyadavermek = (Spinner)getActivity().findViewById(R.id.spvermekAlmak);
        if(s_almakyadavermek.equals("0")){
            akmakyadavermek.setSelection(0);
        }
        else{
            akmakyadavermek.setSelection(1);
        }

        // haftalık hedef belirleme
        Spinner s_haftalikhedefBelirleme = (Spinner)getActivity().findViewById(R.id.haftalikAmac);
        if(s_haftalikHedef.equals("0.5")){
            s_haftalikhedefBelirleme.setSelection(0);
        }
        else if(s_haftalikHedef.equals("1")){
            s_haftalikhedefBelirleme.setSelection(1);
        }
        else if(s_haftalikHedef.equals("1.5")){
            s_haftalikhedefBelirleme.setSelection(2);
        }

        // tempo
        Spinner s_egzersizTempo_durumu = (Spinner)getActivity().findViewById(R.id.sp_tempo);
        int egzersizSeviye = 0;
        try{
            egzersizSeviye = Integer.parseInt(s_tempo);
        }
        catch (NumberFormatException e){

        }
        s_egzersizTempo_durumu.setSelection(egzersizSeviye);


        tabloguncelle();

        Button hedefbutonu = (Button)getActivity().findViewById(R.id.HedefGuncellebuto);
        hedefbutonu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HedefiBelirle();
            }
        });

        veriler.close();



    } // hedefDuzenleme

   public void HedefiBelirle(){

        int hata = 0;

        Verit veriler = new Verit(getActivity());
        veriler.open();

        long long_ID = 1;
        String fields[] = new String[] {
                "_id",
                "kullanici_d",
                "kullanici_cinsiyet",
                "kullanici_boy",
                "kullanici_birim"
        };
        Cursor curs = veriler.sec("kullanicilar", fields, "_id", long_ID);
        String s_kullaniciVeri = curs.getString(1);
        String s_kullaniciCinsiyet  = curs.getString(2);
        String s_kullaniciBoy = curs.getString(3);
        String olcumbirimi = curs.getString(4);


        String[] veri = s_kullaniciVeri.split("-");
        String s_yil = veri[0];
        String s_ay = veri[1];
        String s_gun = veri[2];

        int i_yil = 0;
        try {
            i_yil = Integer.parseInt(s_yil);
        }
        catch(NumberFormatException nfe) {
            System.out.println("Hata" + nfe);
        }
        int i_ay = 0;
        try {
            i_ay = Integer.parseInt(s_ay);
        }
        catch(NumberFormatException nfe) {
            System.out.println("Hata " + nfe);
        }
        int i_gun = 0;
        try {
            i_gun = Integer.parseInt(s_gun);
        }
        catch(NumberFormatException nfe) {
            System.out.println("Hata " + nfe);
        }
        String stringUserAge = getAge(i_yil, i_ay, i_gun);

        int i_yas = 0;
        try {
            i_yas = Integer.parseInt(stringUserAge);
        }
        catch(NumberFormatException nfe) {
            System.out.println("Hata " + nfe);
        }


        double d_uzunluk = 0;

        try {
            d_uzunluk = Double.parseDouble(s_kullaniciBoy);
        }
        catch(NumberFormatException nfe) {
            System.out.println("Hata " + nfe);
        }


        EditText mevcutAgirlik = (EditText) getActivity().findViewById(R.id.et_mevcutKilo);
        String s_mevcutAgirlik = mevcutAgirlik.getText().toString();
        double d_mevcutAgirlik = 0;
        if(s_mevcutAgirlik.isEmpty()){
            Toast.makeText(getActivity(), "Lütfen şimdiki kilonuzu girin", Toast.LENGTH_LONG).show();
            hata = 1;
        }
        else{
            try {
                d_mevcutAgirlik = Double.parseDouble(s_mevcutAgirlik);
            }
            catch(NumberFormatException nfe) {
                Toast.makeText(getActivity(), "Girdiginiz deger numerik bir deger olmalı . \nError: " + nfe.toString(), Toast.LENGTH_LONG).show();
                hata = 1;
            }
        }
        String stringCurrentWeightSQL = veriler.aktarma(s_mevcutAgirlik);

        // hedeflenen kilo
        EditText hedefAgirlik = (EditText) getActivity().findViewById(R.id.et_hedefKilosu);
        String s_hedefkilonuz = hedefAgirlik.getText().toString();
        double d_hedefKilo = 0;
        if(s_hedefkilonuz.isEmpty()){
            Toast.makeText(getActivity(), "Hedef Kilonuzu giriniz", Toast.LENGTH_LONG).show();
            hata = 1;
        }
        else{
            try {
                d_hedefKilo = Double.parseDouble(s_hedefkilonuz);
            }
            catch(NumberFormatException nfe) {
                Toast.makeText(getActivity(), "Hedef kilonuz numerik bir deger olmali.\nError: " + nfe.toString(), Toast.LENGTH_LONG).show();
                hata = 1;
            }
        }
        String s_hedefAgirlikSQL = veriler.aktarma(s_hedefkilonuz);

        // almakyadavermek

        Spinner s_almakvermek = (Spinner)getActivity().findViewById(R.id.spvermekAlmak);
        int i_almakvermek = s_almakvermek.getSelectedItemPosition();
        String s_almakyadavermek = "" + i_almakvermek;
        String s_almakyadavermekSQL = veriler.aktarma(s_almakyadavermek);

        //haftalik hedef ne olmali
        Spinner s_haftalikHedef = (Spinner)getActivity().findViewById(R.id.haftalikAmac);
        String s_haftalikhedeff = s_haftalikHedef.getSelectedItem().toString();
        String s_haftalikHedefSQL = veriler.aktarma(s_haftalikhedeff);

        Spinner s_egzersizTemposu = (Spinner)getActivity().findViewById(R.id.sp_tempo);
      // tempo sayinin degerine gore artıyor. 4 e kadar // burada kalndı
        int i_egzersizDerecesi = s_egzersizTemposu.getSelectedItemPosition();
        String s_egzersizztemposu = ""+i_egzersizDerecesi;
        String s_EgzersizzDerecesiSQL = veriler.aktarma(s_egzersizztemposu);


        TextView hesaplama = (TextView)getActivity().findViewById(R.id.tv_hesaplama);                  //*************

        if(hata == 0) {

            DateFormat tarih = new SimpleDateFormat("yyyy-MM-dd");
            String hedefZaman = tarih.format(Calendar.getInstance().getTime());
            String hedefZamanSQL = veriler.aktarma(hedefZaman);

            double hedefEnerjibirim = 0;

            if(s_kullaniciCinsiyet.startsWith("m")){

                hedefEnerjibirim = 66.5+(13.75*d_mevcutAgirlik)+(5.003*d_uzunluk)-(6.755*i_yas); // hesaplar internetten araştırıldı

            }
            else{

                hedefEnerjibirim = 655+(9.563*d_mevcutAgirlik)+(1.850*d_uzunluk)-(4.676*i_yas);

            }
            hedefEnerjibirim = Math.round(hedefEnerjibirim);
            String hedefEnerkiBrimSQL = veriler.aktarma(""+hedefEnerjibirim);

            double proteinb = Math.round(hedefEnerjibirim*25/100);
            double karbonhidratb = Math.round(hedefEnerjibirim*50/100);
            double yagB = Math.round(hedefEnerjibirim*25/100);

            double proteinBsql = veriler.aktarma(proteinb);
            double karbonhidratBSQL = veriler.aktarma(karbonhidratb);
            double yagbSQL = veriler.aktarma(yagB);


            double haftalikHedef = 0;
            try {
                haftalikHedef = Double.parseDouble(s_haftalikhedeff);
            }
            catch(NumberFormatException nfe) {
                System.out.println("Could not parse " + nfe);
            }

            // 1 kg yag yaklasik 8000 kalori e karsilik geliyormus
            double kalori = 0;
            double diyetEnerji = 0;
            kalori = 7800*haftalikHedef;
            if(i_almakvermek == 0){
                diyetEnerji = Math.round((hedefEnerjibirim - (kalori/7)) * 1.2);

            }
            else{
                // kiloAlmakistiyorsa
                diyetEnerji = Math.round((hedefEnerjibirim + (kalori/7)) * 1.2);
            }

            //verileri cuncelle
            double enerjiDiyetSQL = veriler.aktarma(diyetEnerji);

            double proteinDiet = Math.round(diyetEnerji*25/100);
            double karbonhidratDiet = Math.round(diyetEnerji*50/100);
            double yagDiyet = Math.round(diyetEnerji*25/100);

            double proteinDiyetSQL = veriler.aktarma(proteinDiet);
            double karbonhidratDietSQL = veriler.aktarma(karbonhidratDiet);
            double yagDiyetSQL = veriler.aktarma(yagDiyet);


            double enerjiEgzersizDurumunda = 0;
            if(s_egzersizztemposu.equals("0")) {
                enerjiEgzersizDurumunda = hedefEnerjibirim * 1.2;
            }
            else if(s_egzersizztemposu.equals("1")) {
                enerjiEgzersizDurumunda = hedefEnerjibirim * 1.375; // hafif egzersiz
            }
            else if(s_egzersizztemposu.equals("2")) {
                enerjiEgzersizDurumunda = hedefEnerjibirim*1.55; // orta
            }
            else if(s_egzersizztemposu.equals("3")) {
                enerjiEgzersizDurumunda = hedefEnerjibirim*1.725; // cok
            }
            else if(s_egzersizztemposu.equals("4")) {
                enerjiEgzersizDurumunda = hedefEnerjibirim * 1.9; // encok
            }
            enerjiEgzersizDurumunda = Math.round(enerjiEgzersizDurumunda);
            double energyWithActivitySQL = veriler.aktarma(enerjiEgzersizDurumunda);

            double egzersizleProtein = Math.round(enerjiEgzersizDurumunda*25/100);
            double egzersizleKarbonhidrat = Math.round(enerjiEgzersizDurumunda*50/100);
            double egzersizleYag = Math.round(enerjiEgzersizDurumunda*25/100);

            double egzersizProSQL = veriler.aktarma(egzersizleProtein);
            double egzersizleKarbonSQL = veriler.aktarma(egzersizleKarbonhidrat);
            double egzersizyagSQL = veriler.aktarma(egzersizleYag);

            kalori = 0;
            double egzersizdekivediyettekiEnerjiDurumu = 0;
            kalori = 7800*haftalikHedef;
            if(i_almakvermek == 0){ // vermek istiyorsa 0 , almak sitiyorsa 1

                egzersizdekivediyettekiEnerjiDurumu = hedefEnerjibirim - (kalori/7);
            }
            else{

                egzersizdekivediyettekiEnerjiDurumu = hedefEnerjibirim + (kalori/7);
            }
            if(s_egzersizztemposu.equals("0")) {
                egzersizdekivediyettekiEnerjiDurumu= egzersizdekivediyettekiEnerjiDurumu* 1.2;
            }
            else if(s_egzersizztemposu.equals("1")) {
                egzersizdekivediyettekiEnerjiDurumu= egzersizdekivediyettekiEnerjiDurumu* 1.375;
            }
            else if(s_egzersizztemposu.equals("2")) {
                egzersizdekivediyettekiEnerjiDurumu= egzersizdekivediyettekiEnerjiDurumu*1.55;
            }
            else if(s_egzersizztemposu.equals("3")) {
                egzersizdekivediyettekiEnerjiDurumu= egzersizdekivediyettekiEnerjiDurumu*1.725;
            }
            else if(s_egzersizztemposu.equals("4")) {
                egzersizdekivediyettekiEnerjiDurumu = egzersizdekivediyettekiEnerjiDurumu* 1.9;
            egzersizdekivediyettekiEnerjiDurumu = Math.round(egzersizdekivediyettekiEnerjiDurumu);

            double egzersizdevediyettekienerjiSQL = veriler.aktarma(egzersizdekivediyettekiEnerjiDurumu);

            double egzersizdekivediyettekiProtein = Math.round(egzersizdekivediyettekiEnerjiDurumu*25/100);
            double egzersizdekivediyettekiKarbonhidrat = Math.round(egzersizdekivediyettekiEnerjiDurumu*50/100);
            double egzersizdekivediyettekiYag = Math.round(egzersizdekivediyettekiEnerjiDurumu*25/100);

            double egzersizdekivediyettekiProteinSQL = veriler.aktarma(egzersizdekivediyettekiProtein);
            double egzersizdekivediyettekikarbonhidratSQL = veriler.aktarma(egzersizdekivediyettekiKarbonhidrat);
            double egzersizdekivediyettekiYagSQL = veriler.aktarma(egzersizdekivediyettekiYag);

            String inpFields = "'_id', " +
                    "'hedef_guncel_kilo', " +
                    "'hedef_kilo', " +
                    "'hedef_almakvermek', " +
                    "'hedef_haftalik_tempo', " +
                    "'hedef_tarih'," +
                    "'goal_activity_level'," +
                    "'hedef_enerji'," +
                    "'hedef_protein'," +
                    "'hedef_karbonhidrat'," +
                    "'hedef_yag'," +
                    "'diyet_enerji'," +
                    "'diyet_protein'," +
                    "'diyet_kalori'," +
                    "'diyet_yag'," +
                    "'aktivite_enerji'," +
                    "'aktivite_protein'," +
                    "'aktivite_karbonhidrat'," +
                    "'aktivite_yağ'," +

                    "'diyetAktivite_enerji'," +
                    "'diyetAktivite_protein'," +
                    "'diyetAktivite_karbonhidrat'," +
                    "'diyetAktivite_yag'";

            String inpValues = "NULL, " +
                    stringCurrentWeightSQL + ", " +
                    s_hedefAgirlikSQL + ", " +
                    s_almakyadavermekSQL  + ", " +
                    s_haftalikHedefSQL + ", " +
                    hedefZamanSQL  + ", " +
                    s_EgzersizzDerecesiSQL + ", " +
                    hedefEnerkiBrimSQL + ", " +
                    proteinBsql + ", " +
                    karbonhidratBSQL  + ", " +
                    yagbSQL + ", " +

                    enerjiDiyetSQL + ", " +
                    proteinDiyetSQL + ", " +
                    karbonhidratDietSQL  + ", " +
                    yagDiyetSQL + ", " +

                    enerjiEgzersizDurumunda + ", " +
                    egzersizProSQL  + ", " +
                    egzersizleKarbonSQL   + ", " +
                    egzersizyagSQL   + ", " +

                    egzersizdevediyettekienerjiSQL + ", " +
                    egzersizdekivediyettekiProteinSQL + ", " +
                    egzersizdekivediyettekikarbonhidratSQL + ", " +
                    egzersizdekivediyettekiYagSQL;

            veriler.insert("hedef", inpFields, inpValues);


            tabloguncelle();


            Toast.makeText(getActivity(), "Degisiklikler başariyla Kaydedildi", Toast.LENGTH_SHORT).show();

            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();



        }

    }


    private String getAge(int year, int month, int day){
        Calendar veritarih = Calendar.getInstance();
        Calendar bugun = Calendar.getInstance();

        veritarih.set(year, month, day);

        int yastut = bugun.get(Calendar.YEAR) - veritarih.get(Calendar.YEAR);

        if (bugun.get(Calendar.DAY_OF_YEAR) < veritarih.get(Calendar.DAY_OF_YEAR)){
            yastut--;
        }

        Integer i_yasDegeri = new Integer(yastut);
        String s_yasDegeri = i_yasDegeri.toString();

        return s_yasDegeri;
    }


    private void tabloguncelle(){
        Verit veriler = new Verit(getActivity());
        veriler.open();

        String fieldsGoal[] = new String[] {
                "hedef_enerji",
                "hedef_protein",
                "hedef_karbonhidrat",
                "hedef_yag",
                "diyet_enerji",
                "diyet_protein",
                "diyet_kalori",
                "diyet_yag",
                "aktivite_enerji",
                "aktivite_protein",
                "aktivite_karbonhidrat",
                "aktivite_yağ",
                "diyetAktivite_enerji",
                "diyetAktivite_protein",
                "diyetAktivite_karbonhidrat",
                "diyetAktivite_yag"
        };
        Cursor goalCursor = veriler.sec("hedef", fieldsGoal, "", "", "_id", "DESC");


        String hedefEnerjiB = goalCursor.getString(0);
        String hedefProteinB = goalCursor.getString(1);
        String hedefKarbonhidratB = goalCursor.getString(2);
        String hedefYagB = goalCursor.getString(3);
        String hedefEnerjiDiyet = goalCursor.getString(4);
        String hedefProteinDiyet = goalCursor.getString(5);
        String hedefKArbonhidratDiyet = goalCursor.getString(6);
        String hedefYagDiyet = goalCursor.getString(7);
        String hederEnerjiAktiviteDiyet = goalCursor.getString(8);
        String hedefProteinAktiviteDiyet = goalCursor.getString(9);
        String hedefKArbonAktivitDiyet = goalCursor.getString(10);
        String hedefYagdiyett = goalCursor.getString(11);
        String hedefEnerjiaktivitevediyet = goalCursor.getString(12);
        String hedefPRoteinaktivitevediyet = goalCursor.getString(13);
        String hedefKarbonAktiviteveDiyet = goalCursor.getString(14);
        String yedefyagAktivitevediyet = goalCursor.getString(15);



        TextView hedefEnerjiveDiyet = (TextView)getActivity().findViewById(R.id.tv_hedefEnerji);
        hedefEnerjiveDiyet.setText(hedefEnerjiDiyet);
        TextView hedefproteindiyet = (TextView)getActivity().findViewById(R.id.tv_hedefDiyetProtein);
        hedefproteindiyet.setText(hedefProteinDiyet);
        TextView hederkarbonhidratDiyet = (TextView)getActivity().findViewById(R.id.tv_hedefDiyetKArb);
        hederkarbonhidratDiyet.setText(hedefKArbonhidratDiyet);
        TextView hedefYagdiyet = (TextView)getActivity().findViewById(R.id.tv_hedefDiyetYag);
        hedefYagdiyet.setText(hedefYagDiyet);


        TextView hedefenerjidiyetveEgzersiz = (TextView)getActivity().findViewById(R.id.tv_hedefenerjidiyetAktivit);
        hedefenerjidiyetveEgzersiz.setText(hedefEnerjiaktivitevediyet);
        TextView hedefproteindiyetveEgzersiz = (TextView)getActivity().findViewById(R.id.tv_hedefProteinDiyetle);
        hedefproteindiyetveEgzersiz.setText(hedefPRoteinaktivitevediyet);
        TextView hedekarbonhidratdiyetveEgzersiz = (TextView)getActivity().findViewById(R.id.tv_karbonhidratDiyetle);
        hedekarbonhidratdiyetveEgzersiz.setText(hedefKarbonAktiviteveDiyet);
        TextView hedefdiyetveEgzersiz = (TextView)getActivity().findViewById(R.id.hedefEgzersizveDiyet);
        hedefdiyetveEgzersiz.setText(yedefyagAktivitevediyet);


        TextView hedeffEnerjiB = (TextView)getActivity().findViewById(R.id.hedefEnerjibrim);
        hedeffEnerjiB.setText(hedefEnerjiB);
        TextView hedeffProteinB = (TextView)getActivity().findViewById(R.id.tv_hedefProteinBRM);
        hedeffProteinB.setText(hedefProteinB);
        TextView hedeffKArbB = (TextView)getActivity().findViewById(R.id.tv_hedefKArbonBrm);
        hedeffKArbB.setText(hedefKarbonhidratB);
        TextView textViewGoalFatBMR = (TextView)getActivity().findViewById(R.id.tvYagBrm);
        textViewGoalFatBMR.setText(hedefYagB);


        TextView hedeflenenyagEgzersizle = (TextView)getActivity().findViewById(R.id.tv_hedefEnerjiaktivite);
        hedeflenenyagEgzersizle.setText(hederEnerjiAktiviteDiyet);
        TextView hedefleneproteinEgzersizle = (TextView)getActivity().findViewById(R.id.hedefproteinaktiviteyle);
        hedefleneproteinEgzersizle.setText(hedefProteinAktiviteDiyet);
        TextView hedeflenenKarbonhidratEgzersizle = (TextView)getActivity().findViewById(R.id.hedefkarbaktiviteyle);
        hedeflenenKarbonhidratEgzersizle.setText(hedefKArbonAktivitDiyet);
        TextView hedeflenennyagEgzersizle = (TextView)getActivity().findViewById(R.id.tv_hedefyagaktiviteyle);
        hedeflenennyagEgzersizle.setText(hedefYagdiyett);

        veriler.close();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " ");                                                                                   //**********************!
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
