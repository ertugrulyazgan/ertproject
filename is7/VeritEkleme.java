package com.example.deeptrancer.diyetuygulamam496;

/**
 * Created by deeptrancer on 2.03.2018.
 */

        import android.content.Context;
        import android.database.sqlite.SQLiteException;

public class VeritEkleme {


    private final Context icerik;


    public VeritEkleme(Context ctx){
        this.icerik = ctx;
    }




    public void kategoriOlustur(String values){
        try{
            Verit veriler = new Verit(icerik);
            veriler.open();
            veriler.insert("kategoriler",
                    "_id, kategori_isim, kategori_pid, kategori_ikn, kategori_aciklama",
                    values);
            veriler.close();
        }
        catch (SQLiteException e){ //def constructor
        }
    }
    public void tumKategorileriAktar(){
        kategoriOlustur("NULL, 'Bugday', '1', '', NULL");
        kategoriOlustur("NULL, 'Ekmek', '1', '', NULL");
        kategoriOlustur("NULL, 'Simit', '1', '', NULL");

        kategoriOlustur("NULL, 'Et', '0', '', NULL");
        kategoriOlustur("NULL, 'tavuk', '0', '', NULL");

        kategoriOlustur("NULL, 'Makarna', '0', '', NULL");
        kategoriOlustur("NULL, 'Peynir', '0', '', NULL");
        kategoriOlustur("NULL, 'Aburcubur', '0', '', NULL");

    }



    public void besinleriEkle(String values){

        try {
            Verit veriler = new Verit(icerik);
            veriler.open();
            veriler.insert("gidalar",
                    "_id, gida_isim, gida_Marka, gida_servisGram, gida_servisGramolcm, gida_servisAdet, gida_servisAdetolcm, gida_enerji, gida_protein, gida_karbonhidrat, gida_yag, gida_olculenEnerji, gida_olculenProtein, gida_olculen_karbonhidrat, gida_olculen_yag, gida_kullaniciID, gida_bar, gida_kategori, gida_tmb, gida_fa, gida_fb, gida_fc, gida_not",
                    values);
            veriler.close();
        }
        catch (SQLiteException e){

        }

    }
    // veritabanina ekliyoruz

    public void tumbesinleriAktar(){
        besinleriEkle("NULL, 'Geleneksel Tereyag', 'Torku', '26', 'gram', '1', 'NULL', '122', '3.5', '23.4', '1', '32', '1', '6', '0', NULL, NULL, '1', 'NULL', 'NULL', 'NULL', 'NULL', NULL");
        besinleriEkle("NULL, 'Helva ', 'Koska', '60', 'gram', '60', 'g', '389', '11.4', '63.1', '7.8', '233', '7', '38', '5', NULL, NULL, '1', 'NULL', 'NULL', 'NULL', 'NULL', NULL");
        besinleriEkle("NULL, 'Baldo princ', 'Reis', '80', 'gram', '80', 'g', '380', '13', '61', '7', '304', '10', '49', '6', NULL, NULL, '1', 'NULL', 'NULL', 'NULL', 'NULL', NULL");
        besinleriEkle("NULL, 'Gong', 'Eti', '40', 'gram', '1', 'NULL', '251', '8.1', '50', '1.5', '201', '6', '40', '1', NULL, NULL, '1', 'NULL', 'NULL', 'NULL', 'NULL', NULL");
        besinleriEkle("NULL, 'Makarna', 'Filiz', '200', 'gram', '1', 'gram', '250', '18', '25', '6', '175', '13', '18', '4', NULL, NULL, '1', 'NULL', 'NULL', 'NULL', 'NULL', NULL");
        besinleriEkle("NULL, 'Pizza', 'SuperFresh', '200', 'gram', '1', 'gram', '260', '10', '39', '0.5', '156', '6', '23', '0', NULL, NULL, '1', 'NULL', 'NULL', 'NULL', 'NULL', NULL");
        besinleriEkle("NULL, 'Hazir döner', 'Erpiliç', '126', 'gram', '1', 'gram', '330', '14', '43', '5.5', '33', '1', '4', '1', NULL, NULL, '1', 'NULL', 'NULL', 'NULL', 'NULL', NULL");
        besinleriEkle("NULL, 'Tavuk Kanat', 'Banvit', '100', 'gram', '1', 'gram', '338', '10.5', '55.5', '3.5', '54', '2', '9', '1', NULL, NULL, '1', 'NULL', 'NULL', 'NULL', 'NULL', NULL");
        besinleriEkle("NULL, 'Ucgen Peynir', 'Torku', '100', 'gram', '100', 'gram', '400', '0', '100', '0', '400', '0', '100', '0', NULL, NULL, '1', 'NULL', 'NULL', 'NULL', 'NULL', NULL");
        besinleriEkle("NULL, 'Salam', 'Namet', '100', 'gram', '100', 'gram', '341', '10.2', '69.6', '1.6', '341', '10', '70', '2', NULL, NULL, '1', 'NULL', 'NULL', 'NULL', 'NULL', NULL");
        besinleriEkle("NULL, 'Makarna', 'NuhunAnkara', '100', 'gram', '100', 'gram', '341', '10.2', '69.6', '1.6', '341', '10', '70', '2', NULL, NULL, '2', 'NULL', 'NULL', 'NULL', 'NULL', NULL");


    }


}
