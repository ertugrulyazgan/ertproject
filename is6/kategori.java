package com.example.deeptrancer.diyetuygulamam;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;


public class kategori extends Fragment {
    // TODO: Rename parameter arguments, choose names that match

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    Cursor kategoriGost;

    public kategori() {

    }

    // TODO: Rename and change types and number of parameters
    public static kategori newInstance(String param1, String param2) {
        kategori fragment = new kategori();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


        ((MainActivity)getActivity()).getSupportActionBar().setTitle("kategoriler");

    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        populateList("0", "");

    }


    public void populateList(String parentID, String parentName){


        verit db = new verit(getActivity());
        db.open();


        String fields[] = new String[] {

        };
        kategoriGost = db.select("kategoriler", fields, "kategoriu_id", parentID);


        ArrayList<String> values = new ArrayList<String>();


        int categoriesCount = kategoriGost.getCount();
        for(int x=0;x<categoriesCount;x++){
            values.add(kategoriGost.getString(kategoriGost.getColumnIndex("k_name")));

            kategoriGost.moveToNext();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, values);

        ListView lv = (ListView)getActivity().findViewById(R.id.listViewCategories);
        lv.setAdapter(adapter);


        if(parentID.equals("0")) {
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    listItemClicked(arg2);
                }
            });
        }

        db.close();
    }


    public void listItemClicked(int listItemIDClicked){


        kategoriGost.moveToPosition(listItemIDClicked);


        String id = kategoriGost.getString(0);
        String name = kategoriGost.getString(1);



        ((MainActivity)getActivity()).getSupportActionBar().setTitle(name);


        populateList(id, name);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        return inflater.inflate(R.layout.kategoriler, container, false);


    }

    // TODO: Rename method, update argument and hook method into UI event




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
