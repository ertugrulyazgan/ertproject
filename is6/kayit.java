﻿package com.example.deeptrancer.diyetuygulamam;

import android.content.Intent;
import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

public class kayit extends AppCompatActivity {

    public void  gecis(View view) {

        Intent gec = new Intent (this, durum.class);
        startActivity(gec);

    }

    private String[] gunler = new String[31];
    private String[] yilicin = new String[100];



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kayit);

        //gunleri tutan arraye doğum tarihine göre numaraları dolduruyorum.
        int kisicinSayac = 0;
        for(int x=0;x<31;x++){
            kisicinSayac=x+1;
            this.gunler[x] = "" + kisicinSayac;
        }
        Spinner gun = (Spinner) findViewById(R.id.gun);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, gunler);
        gun.setAdapter(adapter);

        Calendar calendar = Calendar.getInstance();
        int yili = calendar.get(Calendar.YEAR);
        int kadar = yili-100;
        int nerede = 0;
        for(int x=yili;x>kadar;x--){
            this.yilicin[nerede] = "" + x;


            nerede++;
        }

        Spinner yil = (Spinner) findViewById(R.id.yil);
        ArrayAdapter<String> adapterYear = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, yilicin);
        yil.setAdapter(adapterYear);

        ImageView i_hata = (ImageView)findViewById(R.id.i_hata);
        i_hata.setVisibility(View.GONE);

        TextView t_hata = (TextView)findViewById(R.id.t_hata);
        t_hata.setVisibility(View.GONE);


        EditText uzunlugunuz_inc = (EditText)findViewById(R.id.uzunlugunuz_inc);
        uzunlugunuz_inc.setVisibility(View.GONE);


        Spinner olcumbirimi = (Spinner)findViewById(R.id.olcumbirimi);
        olcumbirimi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {   // Listelerden bir eleman seçildiğinde olacaklar
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int yeri, long id) {
                olcudonusumu();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // olcudonusumu();
            }
        });





        /* Listener b_kayit */
        Button b_kayit = (Button)findViewById(R.id.b_kayit);
        b_kayit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                kayitdoldur();
            }
        });


    }

    public void olcudonusumu() {

        Spinner olcumbirimi = (Spinner)findViewById(R.id.olcumbirimi);
        String s_olcum = olcumbirimi.getSelectedItem().toString();


        EditText uzunlugunuz_cm = (EditText)findViewById(R.id.uzunlugunuz_cm);
        EditText uzunlugunuz_inc = (EditText)findViewById(R.id.uzunlugunuz_inc);
        String s_cm = uzunlugunuz_cm.getText().toString();
        String s_inc = uzunlugunuz_inc.getText().toString();

        double d_cm = 0;
        double d_feet = 0;
        double d_inc = 0;

        TextView santim = (TextView)findViewById(R.id.santim);
        TextView t_kilo = (TextView)findViewById(R.id.t_kilo);

        if(s_olcum.startsWith("I")){

            uzunlugunuz_inc.setVisibility(View.VISIBLE);
            santim.setText("");
            t_kilo.setText("kilo");

            try {
                d_cm = Double.parseDouble(s_cm);
            }
            catch(NumberFormatException nfe) {

            }
            if(d_cm != 0){

                d_feet = (d_cm * 0.3937008)/12;

                int f_uzunluk = (int) d_feet;

                uzunlugunuz_cm.setText("" + f_uzunluk);

            }

        } //
        else{
            // Metre
            uzunlugunuz_inc.setVisibility(View.GONE);
            santim.setText("cm");
            t_kilo.setText("kg");

            try {
                d_feet = Double.parseDouble(s_cm);
            }
            catch(NumberFormatException nfe) {

            }

                d_inc = Double.parseDouble(s_inc);
            }
            catch(NumberFormatException nfe) {

            }

            if(d_feet != 0 && d_inc != 0) {
                d_cm = ((d_feet * 12) + d_inc) * 2.54;
                d_cm = Math.round(d_cm);
                uzunlugunuz_cm.setText("" + d_cm);
            }
        }

        EditText edt_agirlik = (EditText)findViewById(R.id.edt_agirlik);
        String s_olcumb = edt_agirlik.getText().toString();
        double d_agirlik = 0;

        try {
            d_agirlik = Double.parseDouble(s_olcumb);
        }
        catch(NumberFormatException nfe) {
        }

        if(d_agirlik != 0) {

            if (s_olcum.startsWith("I")) {

                d_agirlik = Math.round(d_agirlik / 0.45359237);
            } else {

                d_agirlik = Math.round(d_agirlik * 0.45359237);
            }
            edt_agirlik.setText("" + d_agirlik);
        }

    }

    public void kayitdoldur() {

        ImageView i_hata = (ImageView)findViewById(R.id.i_hata);
        TextView t_hata = (TextView)findViewById(R.id.t_hata);
        String hataliGirdi = "";

        TextView t_isim = (TextView)findViewById(R.id.t_isim);
        EditText text_isim = (EditText)findViewById(R.id.text_isim);
        String s_email = text_isim.getText().toString();
        if(s_email.isEmpty() || s_email.startsWith(" ")){
            hataliGirdi = "Lütfen bir kullanici ismi girin";
        }

        Spinner gun = (Spinner)findViewById(R.id.gun);
        String s_gun = gun.getSelectedItem().toString();
        int gunicin = 0;
        try {
            gunicin = Integer.parseInt(s_gun);

            if(gunicin < 10){
                s_gun = "0" + s_gun;
            }

        }
        catch(NumberFormatException nfe) {
            System.out.println("hata " + nfe);
            hataliGirdi = "Doğum gününüz için bir gün seçin";
        }

        Spinner ay = (Spinner)findViewById(R.id.ay);
        String s_ay = ay.getSelectedItem().toString();
        int ayicin = ay.getSelectedItemPosition();
        int month = ayicin+1;
        if(month < 10){
            s_ay = "0" + month;
        }
        else{
            s_ay = "" + month;
        }


        Spinner yil = (Spinner)findViewById(R.id.yil);
        String s_yil = yil.getSelectedItem().toString();
        int yilicin = 0;
        try {
            yilicin = Integer.parseInt(s_yil);
        }
        catch(NumberFormatException nfe) {
            System.out.println("Could not parse " + nfe);
            hataliGirdi = "Lütfen dogum tarihinizi tekrar girin";
        }

        String s_dogumTarihi = yilicin + "-" + s_ay + "-" + s_gun;

        RadioGroup rg_cinsiyet = (RadioGroup)findViewById(R.id.rg_cinsiyet);
        int r_button_Id = rg_cinsiyet.getCheckedRadioButtonId();
        View radioButtonGender = rg_cinsiyet.findViewById(r_button_Id);
        int yeri = rg_cinsiyet.indexOfChild(radioButtonGender);

        String cinsiyetiNe = "";
        if(yeri == 0){
            cinsiyetiNe = "male";
        }
        else{
            cinsiyetiNe = "female";
        }


        EditText uzunlugunuz_cm = (EditText)findViewById(R.id.uzunlugunuz_cm);
        EditText uzunlugunuz_inc = (EditText)findViewById(R.id.uzunlugunuz_inc);
        String s_cm = uzunlugunuz_cm.getText().toString();
        String s_inc = uzunlugunuz_inc.getText().toString();

        double d_cm = 0;
        double d_feet = 0;
        double d_inc = 0;
        boolean metre = true;

        // Metre mi değil mi
        Spinner olcumbirimi = (Spinner)findViewById(R.id.olcumbirimi);
        String s_olcum = olcumbirimi.getSelectedItem().toString();

        int olcubirimiInt = olcumbirimi.getSelectedItemPosition();
        if(olcubirimiInt == 0){
            s_olcum = "metre";
        }
        else{
            s_olcum = "imperial";
            metre = false;
        }

        if(metre == true) {

            try {
                d_cm = Double.parseDouble(s_cm);
                d_cm = Math.round(d_cm);
            }
            catch(NumberFormatException nfe) {
                hataliGirdi = "uzunluk bir sayi girdisi olmali.";
            }
        }
        else {

            try {
                d_feet = Double.parseDouble(s_cm);
            }
            catch(NumberFormatException nfe) {
                hataliGirdi = "uzunluk bir sayi girdisi olmali";
            }

            try {
                d_inc = Double.parseDouble(s_inc);
            }
            catch(NumberFormatException nfe) {
                hataliGirdi = "uzunluk bir sayi girdisi olmali";
            }

            d_cm = ((d_feet * 12) + d_inc) * 2.54;
            d_cm = Math.round(d_cm);
        }

        EditText edt_agirlik = (EditText)findViewById(R.id.edt_agirlik);
        String s_olcumb = edt_agirlik.getText().toString();
        double d_agirlik = 0;
        try {
            d_agirlik = Double.parseDouble(s_olcumb);
        }
        catch(NumberFormatException nfe) {
            hataliGirdi = "agirlik bir sayi girdisi olmali";
        }
        if(metre == true) {
        }
        else{

            d_agirlik = Math.round(d_agirlik*0.45359237);
        }



        Spinner aktiviteyogunluk = (Spinner)findViewById(R.id.aktiviteyogunluk);

        int programyogunlugu = aktiviteyogunluk.getSelectedItemPosition();

        // hata
        if(hataliGirdi.isEmpty()){

            i_hata.setVisibility(View.GONE);
            t_hata.setVisibility(View.GONE);



            verit db = new verit(this);
            db.open();



            String s_emailSQL = db.quoteSmart(s_email);
            String s_dogumTarihiSQL = db.quoteSmart(s_dogumTarihi);
            String s_cinsiyetSQL = db.quoteSmart(cinsiyetiNe);
            double heightCmSQL = db.quoteSmart(d_cm);
            int programyogunluguSQL = db.quoteSmart(programyogunlugu);
            double agirlikSQL = db.quoteSmart(d_agirlik);
            String s_olcumbSQL = db.quoteSmart(s_olcum);


            String stringInput = "NULL, " + s_emailSQL + "," + s_dogumTarihiSQL + "," + s_cinsiyetSQL + "," + heightCmSQL + "," + programyogunluguSQL + "," + s_olcumbSQL;
            db.insert("kullanicilar",
                    "k_idNumarasi, k_email, k_d, k.cinsiyet, k_uzunluk_, k_aktivite, k_olcum",
                    stringInput);



            DateFormat df1=new SimpleDateFormat("yyyy-MM-dd");
            String goalDate=df1.format(Calendar.getInstance().getTime());

            String s_hedefSQL = db.quoteSmart(goalDate);

            stringInput = "NULL, " + agirlikSQL + "," + s_hedefSQL;
            db.insert("hedef",
                    "hedefID, hedef_kilo, hedef_zaman",
                    stringInput);



            db.close();

            Intent i = new Intent(kayit.this, kayithedef.class);
            startActivity(i);

        }
        else {

            t_hata.setText(hataliGirdi);
            i_hata.setVisibility(View.VISIBLE);
            t_hata.setVisibility(View.VISIBLE);
        }
    }




}
