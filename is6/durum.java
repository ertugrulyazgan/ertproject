﻿package com.example.deeptrancer.diyetuygulamam;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

public class durum extends AppCompatActivity {
    private RadioGroup radioGroup;
    private EditText editText;
    private TextView boyt, kilogoster_sk, idealk_tv, durum_tv, aciklama;
    private RadioButton radioBayan, radioBay;



    private boolean kadinmi = true;
    private double boy = 0.0;
    private int kilo = 50 ;
    private SeekBar kilo_sk ;
    private double vucutendex = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_durum);

        editText = (EditText) findViewById(R.id.editText);
        kilogoster_sk = (TextView) findViewById(R.id.kilogoster_sk);
        idealk_tv = (TextView) findViewById(R.id.idealk_tv);
        boyt = (TextView) findViewById(R.id.boyt);
        durum_tv = (TextView) findViewById(R.id.durum_tv);
        radioGroup  = (RadioGroup) findViewById(R.id.radiog);
        kilo_sk = (SeekBar) findViewById(R.id.kilo_sk);
        aciklama = (TextView) findViewById(R.id.aciklama);


        editText.addTextChangedListener(editTextOlaylar); // bir yerdeki değerler değişince diğer yerlerdeki değerlerinde deişmesi için listener ekliyoruz. Onclick listener yönteminin alternatifi.
        // bu yüzden editTextOlaylar adında bir değişken buraya ekledik. Bu değişken üzerinden yapılacak değişikleri güncelle.
        kilo_sk.setOnSeekBarChangeListener(editSeekOlaylar);

        radioGroup.setOnCheckedChangeListener(radioGroupOlaylar);

    }

    private TextWatcher editTextOlaylar = new TextWatcher() {  // aşağıda kullanmak üzere TextWatcher türünde bir nesne oluşturdum.
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            try{

                boy= Double.parseDouble(s.toString())/100.0 ;  //değerler char olarak geliyor onları double a çevirmek istedim. önce stringe sonra double a.
                // mesela değer 175 geliyorsa 1.75 olarak ele alması içinde 100.0 a böldüm.


            }catch(NumberFormatException e)  {  // parse işleminde hatalar olmasın diye bunu ekledim.
                boy = 0.0;    // try kısmında bir hata oluşursa başlangıç değerini tekrar burada koydum.
            }

            guncelle();

        }
        @Override
        public void afterTextChanged(Editable s) {

        }
    } ;  // buradaki ";" ı unutma !


    private void guncelle() {



        kilogoster_sk.setText(String.valueOf(kilo) + "kg");  // default olarak 50 den başlattık. seek bar hareket ettikçe bu değişsin
        boyt.setText(String.valueOf(boy)+ "cm");

        double idealKiloBay =0.0;
        idealKiloBay = (50+2.3*(boy*100*0.4-60));

        double idealKiloBayan = 0.0 ;
        idealKiloBayan =(45.5+2.3*(boy*100*0.4-60));

        double vucutEndex =0.0;

        vucutendex=vucutEndex;

        vucutEndex = kilo / (boy*boy);

        if(kadinmi){

            idealk_tv.setText(String.valueOf(idealKiloBayan));


            if (vucutEndex<20.7) {
                durum_tv.setBackgroundResource(R.color.zayif);
                durum_tv.setText(R.string.zayif);
                aciklama.setText("Zayıfsınız. 4 veya 5 kilo almanız sağlığınız açısından daha iyi olacaktır.");
                vucutendex=vucutEndex;
            }
            else if (20.7<vucutEndex && vucutEndex<26.4) {
                durum_tv.setText(R.string.ideal);
                durum_tv.setBackgroundResource(R.color.ideal);
                aciklama.setText("İdeal bir kiloya sahipsiniz. Beslenmenize aynı şekilde devam edebilirsiniz");
                vucutendex=vucutEndex;
            }

            else if (26.4<vucutEndex && vucutEndex<27.8) {
                durum_tv.setText(R.string.birazfazla);
                durum_tv.setBackgroundResource(R.color.birazfazla);
                aciklama.setText("Biraz kilo fazlalığınız var. 3 yada 4 kilo vermeniz sağlığınız açısından iyi olacaktır");
                vucutendex=vucutEndex;
            }

            else if (27.8<vucutEndex && vucutEndex<31.1) {
                durum_tv.setText(R.string.kilolu);
                durum_tv.setBackgroundResource(R.color.kilolu);
                aciklama.setText("Kilolu durumdasınız. Az kalorili yiyecekler tüketerek kilo vermeye başlayın");
                vucutendex=vucutEndex;
            }

            else if (31.1<vucutEndex && vucutEndex<34.9) {
                durum_tv.setText(R.string.obez);
                durum_tv.setBackgroundResource(R.color.obez);
                aciklama.setText("Ciddi derecede kilo fazlanız var. Sıkı bir diyet yapmanız gerekiyor");
                vucutendex=vucutEndex;
            }

            else{
                durum_tv.setText(R.string.aciltedavi);
                durum_tv.setBackgroundResource(R.color.aciltedavi);
                aciklama.setText("Kilo vermek için en kısa sürede doktorunuza görünün.");
                vucutendex=vucutEndex;
            }


        }
        else
        {
            idealk_tv.setText(String.valueOf(idealKiloBay));

            if (vucutEndex<20.7) {
                durum_tv.setBackgroundResource(R.color.zayif);
                durum_tv.setText(R.string.zayif);
                aciklama.setText("Zayıfsınız. 4 veya 5 kilo almanız sağlığınız açısından daha iyi olacaktır.");
                vucutendex=vucutEndex;
            }
            else if (20.7<vucutEndex && vucutEndex<26.4) {
                durum_tv.setText(R.string.ideal);
                durum_tv.setBackgroundResource(R.color.ideal);
                aciklama.setText("İdeal bir kiloya sahipsiniz. Beslenmenize aynı şekilde devam edebilirsiniz");
                vucutendex=vucutEndex;
            }

            else if (26.4<vucutEndex && vucutEndex<27.8) {
                durum_tv.setText(R.string.birazfazla);
                durum_tv.setBackgroundResource(R.color.birazfazla);
                aciklama.setText("Biraz kilo fazlalığınız var. 3 yada 4 kilo vermeniz sağlığınız açısından iyi olacaktır");
                vucutendex=vucutEndex;
            }

            else if (27.8<vucutEndex && vucutEndex<31.1) {
                durum_tv.setText(R.string.kilolu);
                durum_tv.setBackgroundResource(R.color.kilolu);
                aciklama.setText("Kilolu durumdasınız. Az kalorili yiyecekler tüketerek kilo vermeye başlayın");
                vucutendex=vucutEndex;
            }

            else if (31.1<vucutEndex && vucutEndex<34.9) {
                durum_tv.setText(R.string.obez);
                durum_tv.setBackgroundResource(R.color.obez);
                aciklama.setText("Ciddi derecede kilo fazlanız var. Sıkı bir diyet yapmanız gerekiyor");
                vucutendex=vucutEndex;
            }

            else{
                durum_tv.setText(R.string.aciltedavi);
                durum_tv.setBackgroundResource(R.color.aciltedavi);
                aciklama.setText("Kilo vermek için en kısa sürede doktorunuza görünün.");
                vucutendex=vucutEndex;
            }

        }
    }


    private SeekBar.OnSeekBarChangeListener editSeekOlaylar = new SeekBar.OnSeekBarChangeListener() {
        // seekbarı hareket ettirince değerin göretrilmesini sağlamak istediğim için burda listener oluşturup güncellemeye gittim.
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            kilo = 30+progress ;
            guncelle();
        }
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    } ; // unutma!

    private RadioGroup.OnCheckedChangeListener radioGroupOlaylar = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId == R.id.bayan)
                kadinmi =true;
            else if (checkedId == R.id.bay)
                kadinmi = false;
            guncelle();
        }
    };

    public void  gec(View view) {


        if (vucutendex<20.7) {

            Intent gec = new Intent (this, dprogram1.class);
            startActivity(gec);

        }
        else if (20.7<vucutendex && vucutendex<26.4) {
            Intent gec = new Intent (this, dprogram2.class);
            startActivity(gec);

        }

        else if (26.4<vucutendex && vucutendex<27.8) {
            Intent gec = new Intent (this, dprogram3.class);
            startActivity(gec);

        }

        else if (27.8<vucutendex && vucutendex<31.1) {
            Intent gec = new Intent (this, dprogram4.class);
            startActivity(gec);

        }

        else if (31.1<vucutendex && vucutendex<34.9) {
            Intent gec = new Intent (this, dprogram5.class);
            startActivity(gec);

        }

        else{
            Intent gec = new Intent (this, dprogram6.class);
            startActivity(gec);

        }

    }









}
