﻿package com.example.deeptrancer.diyetuygulamam;
import android.content.Intent;
import android.database.Cursor;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class kayithedef extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kayithedef);

        Button devametbutonu = (Button)findViewById(R.id.devametbutonu);
        devametbutonu.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                hedefiekle();
            }
        });
        olcum();
    }

    public void hedefiekle(){

        verit db = new verit(this);
        db.open();

        ImageView hataicindeneme = (ImageView)findViewById(R.id.hataicindeneme);
        TextView text_hata = (TextView)findViewById(R.id.text_hata);
        String errorMessage = "";

        EditText hedefkilogir = (EditText)findViewById(R.id.hedefkilogir);
        String stringTargetWeight = hedefkilogir.getText().toString();
        double doubleTargetWeight = 0;
        try{
            doubleTargetWeight = Double.parseDouble(stringTargetWeight);
        }
        catch(NumberFormatException nfe) {
            errorMessage = "Target weight has to be a number.";
        }

        Spinner s_almakveyavermek = (Spinner)findViewById(R.id.s_almakveyavermek);
        int intIWantTo = s_almakveyavermek.getSelectedItemPosition();

        Spinner s_haftalıkdegisim = (Spinner)findViewById(R.id.s_haftalıkdegisim);
        String stringWeeklyhedef = s_haftalıkdegisim.getSelectedItem().toString();


        if(errorMessage.isEmpty()){

            long hedefID = 1;

            double doubleTargetWeightSQL = db.quoteSmart(doubleTargetWeight);
            db.update("hedef", "hedef_ID", hedefID, "hedef_kilo", doubleTargetWeightSQL);

            int intIWantToSQL = db.quoteSmart(intIWantTo);
            db.update("hedef", "hedef_ID", hedefID, "hedef_tempo", intIWantToSQL);

            String stringWeeklyhedefSQL = db.quoteSmart(stringWeeklyhedef);
            db.update("hedef", "hedef_ID", hedefID, "hedef_haftalik_tempo", stringWeeklyhedefSQL);

        }
        if(errorMessage.isEmpty()){

            long rowID = 1;
            String fields[] = new String[] {
                    "hedef_ID",
                    "user_dob",
                    "user_gender",
                    "user_height",
                    "user_activity_level"
            };
            Cursor c = db.select("users", fields, "hedef_ID", rowID);
            String stringUserDob = c.getString(1);
            String stringUserGender  = c.getString(2);
            String stringUserHeight = c.getString(3);
            String stringUserActivityLevel = c.getString(4);


            String[] items1 = stringUserDob.split("-");

            double doubleUserHeight = 0;
            try {
                doubleUserHeight = Double.parseDouble(stringUserHeight);
            }
            catch(NumberFormatException nfe) {
                System.out.println("Could not parse " + nfe);
            }

            double bmr = 0;
            if(stringUserGender.startsWith("m")){

                bmr = 66.5+(13.75*doubleTargetWeight)+(5.003*doubleUserHeight)-(6.755);
            }
            else{

                bmr = 655+(9.563*doubleTargetWeight)+(1.850*doubleUserHeight)-(4.676);

            }
            bmr = Math.round(bmr);
            long hedefID = 1;
            double energyBmrSQL = db.quoteSmart(bmr);
            db.update("hedef", "hedef_ID", hedefID, "hedef_ enerji", energyBmrSQL);

            double proteinsBmr = Math.round(bmr*25/100);
            double carbsBmr = Math.round(bmr*50/100);
            double fatBmr = Math.round(bmr*25/100);

            double proteinsBmrSQL = db.quoteSmart(proteinsBmr);
            double carbsBmrSQL = db.quoteSmart(carbsBmr);
            double fatBmrQL = db.quoteSmart(fatBmr);
            db.update("hedef", "hedef_cesit", hedefID, "hedef_protein", proteinsBmrSQL);
            db.update("hedef", "hedef_cesit", hedefID, "hedef_karbonhidrat", carbsBmrSQL);
            db.update("hedef", "hedef_cesit", hedefID, "hedef_yag", fatBmrQL);

            double doubleWeeklyhedef = 0;
            try {
                doubleWeeklyhedef = Double.parseDouble(stringWeeklyhedef);
            }
            catch(NumberFormatException nfe) {
                System.out.println("Could not parse " + nfe);
            }

            double kcal = 0;
            double energyDiet = 0;
            kcal = 7700*doubleWeeklyhedef;
            if(intIWantTo == 0){
                energyDiet = Math.round(bmr - (kcal/7));
            }
            else{

                energyDiet = Math.round(bmr + (kcal/7));
            }

            double energyDietSQL = db.quoteSmart(energyDiet);
            db.update("hedef", "hedef_ID", hedefID, "hedef_energy_diet", energyDietSQL);

            double proteinsDiet = Math.round(energyDiet*25/100);
            double carbsDiet = Math.round(energyDiet*50/100);
            double fatDiet = Math.round(energyDiet*25/100);

            double proteinsDietSQL = db.quoteSmart(proteinsDiet);
            double carbsDietSQL = db.quoteSmart(carbsDiet);
            double fatDietQL = db.quoteSmart(fatDiet);
            db.update("hedef", "hedef_ID", hedefID, "diyet_protein", proteinsDietSQL);
            db.update("hedef", "hedef_ID", hedefID, "diyet_kalori", carbsDietSQL);
            db.update("hedef", "hedef_ID", hedefID, "diyet_yag", fatDietQL);

            double energyWithActivity = 0;
            if(stringUserActivityLevel.equals("0")) {
                energyWithActivity = bmr * 1.2;
            }
            else if(stringUserActivityLevel.equals("1")) {
                energyWithActivity = bmr * 1.375;
            }
            else if(stringUserActivityLevel.equals("2")) {
                energyWithActivity = bmr*1.55;
            }
            else if(stringUserActivityLevel.equals("3")) {
                energyWithActivity = bmr*1.725;
            }
            else if(stringUserActivityLevel.equals("3")) {
                energyWithActivity = bmr * 1.9;
            }
            energyWithActivity = Math.round(energyWithActivity);
            double energyWithActivitySQL = db.quoteSmart(energyWithActivity);
            db.update("hedef", "hedef_ID", hedefID, "aktivite_yag", energyWithActivitySQL);

            double aktiviteProteinSQL = db.quoteSmart(proteinsDiet);
            double karbonhidratProteinSQL = db.quoteSmart(carbsDiet);
            double yagProetinSQL = db.quoteSmart(fatDiet);
            db.update("hedef", "hedef_ID", hedefID, "aktivite_protein", aktiviteProteinSQL);
            db.update("hedef", "hedef_ID", hedefID, "aktivite_karbonhidrat", karbonhidratProteinSQL);
            db.update("hedef", "hedef_ID", hedefID, "aktivite_yag", yagProetinSQL);

            kcal = 0;
            double diyetEnerji = 0;
            kcal = 7700*doubleWeeklyhedef;
            if(intIWantTo == 0){

                diyetEnerji = Math.round(bmr - (kcal/7));

            }
            else{

                diyetEnerji = Math.round(bmr + (kcal/7));
            }

            double diyetEnerjiSQL = db.quoteSmart(diyetEnerji);
            db.update("hedef", "hedef_ID", hedefID, "diyetAktivite_enerji", diyetEnerjiSQL);

            double protein = Math.round(diyetEnerji*25/100);
            double karbonhidrat = Math.round(diyetEnerji*50/100);
            double yag = Math.round(diyetEnerji*25/100);

            double proteinsSQL = db.quoteSmart(protein);
            double karbonhidratSQL = db.quoteSmart(karbonhidrat);
            double yagSQL = db.quoteSmart(yag);
            db.update("hedef", "hedef_ID", hedefID, "diyetAktivite_protein", proteinsSQL);
            db.update("hedef", "hedef_ID", hedefID, "diyetAktivite_karbonhidrat", karbonhidratSQL);
            db.update("hedef", "hedef_ID", hedefID, "diyetAktivite_yag", yagSQL);

        }


        if(!(errorMessage.isEmpty())){
            text_hata.setText(errorMessage);
            hataicindeneme.setVisibility(View.VISIBLE);
            text_hata.setVisibility(View.VISIBLE);

        }

        db.close();

        if(errorMessage.isEmpty()){
            Intent i = new Intent(kayithedef.this, MainActivity.class);
            startActivity(i);
        }
    }

    public void olcum(){

        verit db = new verit(this);
        db.open();

        long rowID = 1;
        String fields[] = new String[] {
                "hedef_ID",
                "kullanici_olcum"
        };
        Cursor c = db.select("users", fields, "hedef_ID", rowID);
        String mesurment;
        mesurment = c.getString(1);

        if(mesurment.startsWith("m")){

        }
        else{
        }
        db.close();
    }


}
