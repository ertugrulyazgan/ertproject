package com.example.deeptrancer.diyetuygulamam;

import android.content.Context;
import android.database.sqlite.SQLiteException;

public class veritekleme {

    private final Context context;


    public veritekleme(Context ctx){
        this.context = ctx;
    }


    public void kategorileriEkle(String values){
        try{
            verit db = new verit(context);
            db.open();
            db.insert("kategoriler",
                    "kategori_id,kategori_isim, kategori_bID, kategori_ikon, kategori_aciklama",
                    values);
            db.close();
        }
        catch (SQLiteException e){

        }
    }
    public void tumkategorileriekle(){
        kategorileriEkle("NULL, 'Bread', '0', '', NULL");
        ///// devam edecek

    }

    public void gidalariEkle(String values){

        try {
            verit db = new verit(context);
            db.open();
            db.insert("besin",
                    "g_idNumarasi, g_isim, g_ureticiIsim, g_miktar, g_miktarBirimi, null, null, g_enerjiMiktari, g_proteinMiktari, g_karbonhidratMiktari, g_yag, g_alinanenerji, g_alinanProtein, g_alinanKarbonhidrat, g_alinanYag, g_kullaniciID, null, g_kategoriID, NULL, NULL, NULL, NULL, g_aciklama",
                    values);
            db.close();
        }
        catch (SQLiteException e){

        }

    }

    public void gidalariEkle(){
        gidalariEkle("NULL, 'Ulker', 'gofret', '26', 'gram', '1', 'stk', '122', '3.5', '23.4', '1', '32', '1', '6', '0', NULL, NULL, '2', NULL, NULL, NULL, NULL, NULL");
    }


}
