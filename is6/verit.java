﻿package com.example.deeptrancer.diyetuygulamam;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class verit {

    private static final String databaseName = "DiyetProgramım";
    private static final int databaseVersion = 7;


    private final Context context;
    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;



    public verit(Context ctx){
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }


    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context){
            super(context, databaseName, null, databaseVersion);
        }

        @Override
        public void onCreate(SQLiteDatabase db){
            try{

                db.execSQL("CREATE TABLE IF NOT EXISTS hedef (" +
                        " _id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        " hedef_cesit INT, "+
                        " hedef_guncel_kilo INT, "+
                        " hedef_kilo INT, "+
                        " hedef_tempo VARCHAR, "+
                        " hedef_haftalik_tempo VARCHAR, "+
                        " hedef_tarih DATE, "+
                        " hedef_ enerji INT, "+
                        " hedef_protein INT, "+
                        " hedef_karbonhidrat INT, "+
                        " hedef_yag INT, "+
                        " diyet_enerji INT, "+
                        " diyet_protein INT, "+
                        " diyet_kalori INT, "+
                        " diyet_yag INT, "+
                        " aktivite_enerji INT, "+
                        " aktivite_protein INT, "+
                        " aktivite_karbonhidrat INT, "+
                        " aktivite_yağ INT, "+
                        " diyetAktivite_enerji INT, "+
                        " diyetAktivite_protein INT, "+
                        " diyetAktivite_karbonhidrat INT, "+
                        " diyetAktivite_yag INT, "+
                        " aciklama VARCHAR);");
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
            try{

                db.execSQL("CREATE TABLE IF NOT EXISTS kullancilar (" +
                        " _id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        " kullanici_id INTEGER, " +
                        " kullanici_email VARCHAR," +
                        " kullanici_sifre VARCHAR, " +
                        " kullanici_tuz VARCHAR, " +
                        " kullanici_rumuz VARCHAR," +
                        " kullanici_d DATE, " +
                        " kullanici_cinsiyet INT, " +
                        " kullanici_yeri VARHCAR, " +
                        " kullanici_boy INT, " +
                        " kullanici_durum INT, " +
                        " kullanici_birim VARHCAR, " +
                        " kullanici_sonakt TIME," +
                        " kullanici_aciklama VARCHAR);");

            }
            catch (SQLException e) {
                e.printStackTrace();
            }

            try{
                db.execSQL("CREATE TABLE IF NOT EXISTS besinlerdenAlinanKalori (" +
                        " _id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        " alinan INTEGER, " +
                        " alinan_tarih DATE, " +
                        " alinan_besin_no INT, " +
                        " alinan_enerji INT, " +
                        " alinan_protein INT, " +
                        " alinan_karbonhidrat INT, " +
                        " alinan_yag INT);");
            }
            catch (SQLException e) {
                e.printStackTrace();
            }

            try{
                db.execSQL("CREATE TABLE IF NOT EXISTS gunlukBesin (" +
                        " _id INTEGER PRIMARY KEY AUTOINCREMENT," +
                        " gbesin_ID INTEGER," +
                        " gbesin_tarih DATE," +
                        " gbesin_ogun INT," +
                        " gbesin_besinID INT," +
                        " gbesin_servis_porsiyon DOUBLE," +
                        " gbesin_birim VARCHAR," +
                        " gbesin_enerji DOUBLE," +
                        " gbesin_protein DOUBLE," +
                        " gbesin_karbonhidrat_calculated DOUBLE," +
                        " gbesin_yag DOUBLE" +
                        " gbesin_ogun_id INT);");

            }
            catch (SQLException e) {
                e.printStackTrace();
            }
            try{
                db.execSQL("CREATE TABLE IF NOT EXISTS kategoriler (" +
                        " _id INTEGER PRIMARY KEY AUTOINCREMENT," +
                        " kategori_id INTEGER," +
                        " kategori_isim VARCHAR," +
                        " kategori_pid INT," +
                        " kategori_ikn VARCHAR," +
                        " kategori_aciklama VARCHAR);");

            }
            catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                db.execSQL("CREATE TABLE IF NOT EXISTS besin (" +
                        " _id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        " besin_id INTEGER, " +
                        " besin_isim VARCHAR," +
                        " besin_ureticiIsmi VARCHAR," +
                        " besin_aciklama VARCHAR," +
                        " besin_miktar DOUBLE," +
                        " besin_olcumb VARCHAR," +
                        " besin_servsno DOUBLE," +
                        " besin_degeri VARCHAR," +
                        " besin_enerjiDOUBLE," +
                        " besin_protein DOUBLE," +
                        " besin_karbonhidrat DOUBLE," +
                        " besin_yag DOUBLE," +
                        " besin_enerjisi DOUBLE," +
                        " besin_protein DOUBLE," +
                        " besin_toplamKarbonhidrat DOUBLE," +
                        " besin_toplamyag DOUBLE," +
                        " besin_kullanici INT," +
                        " besin_kodu VARCHAR," +
                        " besin_kategori INT," +
                        " besin_t VARCHAR," +
                        " besin_f1 VARCHAR," +
                        " besin_f2 VARCHAR," +
                        " besin_f3 VARCHAR," +
                        " besin_aciklama VARCHAR);");



            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            db.execSQL("DROP TABLE IF EXISTS goal");
            db.execSQL("DROP TABLE IF EXISTS users");
            db.execSQL("DROP TABLE IF EXISTS food_diary_cal_eaten");
            db.execSQL("DROP TABLE IF EXISTS food_diary");
            db.execSQL("DROP TABLE IF EXISTS categories");
            db.execSQL("DROP TABLE IF EXISTS food");
            onCreate(db);

            String TAG = "Tag";
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");

        }
    }



    public verit open() throws SQLException {
        db = DBHelper.getWritableDatabase();
        return this;
    }


    public void close() {
        DBHelper.close();
    }

   
    public String quoteSmart(String value){

        boolean isNumeric = false;
        try {
            double myDouble = Double.parseDouble(value);
            isNumeric = true;
        }
        catch(NumberFormatException nfe) {
            System.out.println("Could not parse " + nfe);
        }
        if(isNumeric == false){
            if (value != null && value.length() > 0) {
                value = value.replace("\\", "\\\\");
                value = value.replace("'", "\\'");
                value = value.replace("\0", "\\0");
                value = value.replace("\n", "\\n");
                value = value.replace("\r", "\\r");
                value = value.replace("\"", "\\\"");
                value = value.replace("\\x1a", "\\Z");
            }
        }

        value = "'" + value + "'";

        return value;
    }
    public double quoteSmart(double value) {
        return value;
    }
    public int quoteSmart(int value) {
        return value;
    }


    public void insert(String table, String fields, String values){

        try {
            db.execSQL("INSERT INTO " + table +  "(" + fields + ") VALUES (" + values + ")");
        }
        catch(SQLiteException e){
            System.out.println("Insert error: " + e.toString());
        }
    }


    public int count(String table)
    {
        try {
            Cursor mCount = db.rawQuery("SELECT COUNT(*) FROM " + table + "", null);
            mCount.moveToFirst();
            int count = mCount.getInt(0);
            mCount.close();
            return count;
        }
        catch(SQLiteException e){
            return -1;
        }

    }

    public Cursor select(String table, String[] fields) throws SQLException
    {

        Cursor mCursor = db.query(table, fields, null, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }


    public Cursor select(String table, String[] fields, String whereClause, String whereCondition) throws SQLException
    {

        Cursor mCursor = db.query(table, fields, whereClause + "=" + whereCondition, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }


    public Cursor select(String table, String[] fields, String whereClause, long whereCondition) throws SQLException {


        Cursor mCursor = db.query(table, fields, whereClause + "=" + whereCondition, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public boolean update(String table, String primaryKey, long rowId, String field, String value) {



        value = value.substring(1, value.length()-1);

        ContentValues args = new ContentValues();
        args.put(field, value);
        return db.update(table, args, primaryKey + "=" + rowId, null) > 0;
    }
    public boolean update(String table, String primaryKey, long rowId, String field, double value) {
        ContentValues args = new ContentValues();
        args.put(field, value);
        return db.update(table, args, primaryKey + "=" + rowId, null) > 0;
    }
    public boolean update(String table, String primaryKey, long rowId, String field, int value) {
        ContentValues args = new ContentValues();
        args.put(field, value);
        return db.update(table, args, primaryKey + "=" + rowId, null) > 0;
    }

}
