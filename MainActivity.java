﻿package tobbetu.bil496project;

import android.app.SearchManager;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;


public class MainActivity extends AppCompatActivity implements View.OnClickListener{ // click edince bir event oluşuyor onclick listener da bunu dinliyor.

    Button b  ;   // aşağıda birçok metodta kullanacağımızdan dolayı bunları class değişkeni olarak atadım.
    TextView t ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        b =  findViewById(R.id.giris);                 // oluşturduğum giriş butonunun id sini tutmak için Button türünden bir değişken atadım. b. giris butonunun id sini tutuyor.
        t =  findViewById(R.id.hataligiris);

        b.setOnClickListener(this);   // b tıklandığında BU(this) class içindeki onClick metodu çalışssın ve dinlesin.
                                         // bu metod içinden oluşturulan obje, bu b butonunu dinleyecek. onu belirttim.
    }

    @Override
    public void onClick(View v) { 

        t.setText(R.string.uyari);    // butona tıklanınca resource dosyasında kayıtlı olan text i al, bu text i view in metni olarak ata.

    }
}

